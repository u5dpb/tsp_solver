#ifndef INITIAL_ROUTE_OPTION_H_
#define INITIAL_ROUTE_OPTION_H_

#include "initial_route.h"
#include "nearest_neighbour_initial_route.h"
#include "random_initial_route.h"
#include "successive_inclusion_initial_route.h"

namespace initial_route_methods {

enum class InitialRouteOption {
  none = 0,
  nearest_neighbour,
  random,
  successive_inclusion,
  end
};

static std::array<InitialRouteOption, 4> InitialRouteOptionAll = {
    InitialRouteOption::none, InitialRouteOption::nearest_neighbour,
    InitialRouteOption::random, InitialRouteOption::successive_inclusion};

std::string to_string(InitialRouteOption option);
bool uses_seed(InitialRouteOption option);
bool uses_starting_city(InitialRouteOption option);
std::unique_ptr<InitialRoute> initialRouteFactory(
    const initial_route_methods::InitialRouteOption initial_route);
} // namespace initial_route_methods

#endif // INITIAL_ROUTE_OPTION_H_
