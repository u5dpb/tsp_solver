#ifndef RANDOM_INITIAL_ORDER_H_
#define RANDOM_INITIAL_ORDER_H_

#include "initial_route.h"

class RandomInitialRoute : public InitialRoute {

public:
  RandomInitialRoute() = default;
  RandomInitialRoute(const RandomInitialRoute &) = delete;

  void generateInitialSolution(const MapData &map_data) override;
  const std::string name() const override { return "Random"; }
  const std::string title() const override {
    return name() + " seed " + std::to_string(seed);
  }

  void generateInitialSolution(const MapData &map_data, RandomNumber &rnd_dist);

private:
  const std::string parameters() const override;
  void shuffle_cities(std::vector<unsigned int> &cities,
                      RandomNumber &rnd_dist);
};

#endif // RANDOM_INITIAL_ORDER_H_