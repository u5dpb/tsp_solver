#include "initial_route_option.h"

namespace initial_route_methods {
std::string to_string(InitialRouteOption option) {
  static char const *initial_route_option_str[] = {
      "None",
      "Nearest Neighbour",
      "Random",
      "Successive Inclusion",
  };
  return initial_route_option_str[int(option)];
}

bool uses_starting_city(InitialRouteOption option) {
  static bool const initial_route_option_starting_city[] = {
      false,
      true,
      false,
      true,
  };
  return initial_route_option_starting_city[int(option)];
}

bool uses_seed(InitialRouteOption option) {
  static bool const initial_route_option_seed[] = {
      false,
      false,
      true,
      false,
  };
  return initial_route_option_seed[int(option)];
}

std::unique_ptr<InitialRoute> initialRouteFactory(
    const initial_route_methods::InitialRouteOption initial_route) {
  switch (initial_route) {
  case initial_route_methods::InitialRouteOption::nearest_neighbour:
    return std::make_unique<NearestNeighbourInitialRoute>();
  case initial_route_methods::InitialRouteOption::random:
    return std::make_unique<RandomInitialRoute>();
  case initial_route_methods::InitialRouteOption::successive_inclusion:
    return std::make_unique<SuccessiveInclusionInitialRoute>();
  default:
    return nullptr;
  }
}
} // namespace initial_route_methods
