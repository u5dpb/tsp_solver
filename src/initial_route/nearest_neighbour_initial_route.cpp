#include "nearest_neighbour_initial_route.h"

void NearestNeighbourInitialRoute::generateInitialSolution(
    const MapData &map_data) {
  Route temp_route;
  std::vector<unsigned int> local_route(0);
  local_route.push_back(starting_city);
  set_current_route(local_route);
  std::set<unsigned int> remaining;
  // currently_searching = true;
  std::vector<unsigned int> city_insertion_order;
  sort_by_nearest_neighbour(city_insertion_order, map_data);
  for (auto c_city_insertion_order = 0u;
       c_city_insertion_order < city_insertion_order.size() && searching();
       ++c_city_insertion_order) {
    unsigned int current_city = city_insertion_order[c_city_insertion_order];
    local_route.push_back(current_city);
    set_current_route(local_route);
    print_route_status(local_route, map_data);
    delay_search();
  }
  solution_summary("Nearest Neighbour", map_data);
}

void NearestNeighbourInitialRoute::sort_by_nearest_neighbour(
    std::vector<unsigned int> &ordered_cities, const MapData &map_data) {
  std::set<unsigned int> remaining_cities;
  setup_remaining_cities(remaining_cities, map_data);
  unsigned int last_city = starting_city;
  while (remaining_cities.size() > 0u) {
    unsigned int nearest_city =
        find_nearest_city(remaining_cities, last_city, map_data);
    remaining_cities.erase(nearest_city);
    ordered_cities.push_back(nearest_city);
    last_city = nearest_city;
  }
}

void NearestNeighbourInitialRoute::setup_remaining_cities(
    std::set<unsigned int> &remaining_cities, const MapData &map_data) {
  for (auto i = 1u; i <= map_data.no_locations(); ++i)
    if (i != starting_city)
      remaining_cities.insert(i);
}

unsigned int NearestNeighbourInitialRoute::find_nearest_city(
    std::set<unsigned int> remaining_cities, unsigned int last_city,
    const MapData &map_data) {
  unsigned int nearest_city = *remaining_cities.begin();
  for (auto city : remaining_cities) {
    if (map_data.distance(last_city, city) <
        map_data.distance(last_city, nearest_city)) {
      nearest_city = city;
    }
  }
  return nearest_city;
}

const std::string NearestNeighbourInitialRoute::parameters() const {
  return "Starting City = " + std::to_string(starting_city);
}
