#include "random_initial_route.h"

void RandomInitialRoute::generateInitialSolution(const MapData &map_data) {
  UniformDoubleRandomNumber rnd_dist(seed);
  generateInitialSolution(map_data, rnd_dist);
}

void RandomInitialRoute::generateInitialSolution(const MapData &map_data,
                                                 RandomNumber &rnd_dist) {
  Route temp_route;
  std::vector<unsigned int> local_route(0);

  set_current_route(local_route);
  print_route_status(local_route, map_data);
  std::vector<unsigned int> city_insertion_order;
  setup_city_insertion_order(city_insertion_order, map_data, true);
  shuffle_cities(city_insertion_order, rnd_dist);
  for (auto c_city_insertion_order = 0u;
       c_city_insertion_order < city_insertion_order.size() && searching();
       ++c_city_insertion_order) {
    unsigned int current_city = city_insertion_order[c_city_insertion_order];
    local_route.push_back(current_city);
    set_current_route(local_route);
    print_route_status(local_route, map_data);
    delay_search();
  }
  solution_summary("Random", map_data);
}

void RandomInitialRoute::shuffle_cities(std::vector<unsigned int> &cities,
                                        RandomNumber &rnd_dist) {
  for (auto i = 0u; i < cities.size() - 1; ++i) {
    unsigned int j = rnd_dist.generate((unsigned int)cities.size() - i) + i;
    std::swap(cities[i], cities[j]);
  }
}

const std::string RandomInitialRoute::parameters() const {
  return "Seed = " + std::to_string(seed);
}
