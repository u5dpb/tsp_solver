#include "successive_inclusion_initial_route.h"

void SuccessiveInclusionInitialRoute::generateInitialSolution(
    const MapData &map_data) {
  Route temp_route;
  Route local_route;
  local_route = std::vector<unsigned int>({starting_city});
  set_current_route(local_route.order());
  std::vector<unsigned int> city_order;
  for (auto current_city = starting_city % map_data.no_locations() + 1;
       current_city != starting_city && searching();
       current_city = current_city % map_data.no_locations() + 1) {
    unsigned int best_insert_position =
        find_best_insert_position(local_route, current_city, map_data);
    local_route.insert_after(best_insert_position, current_city);
    set_current_route(local_route.order());
    print_route_status(local_route, map_data);
    delay_search();
  }
  solution_summary("Successive Inclusion", map_data);
}

unsigned int SuccessiveInclusionInitialRoute::find_best_insert_position(
    const Route &route, const unsigned int current_city,
    const MapData &map_data) {

  unsigned int best_insert_position = 0;
  double best_insert_cost =
      get_insert_cost(route, best_insert_position, map_data, current_city);

  for (auto insert_position = 1u; insert_position < route.size();
       ++insert_position) {
    double new_insert_cost =
        get_insert_cost(route, insert_position, map_data, current_city);
    if (new_insert_cost < best_insert_cost) {
      best_insert_position = insert_position;
      best_insert_cost = new_insert_cost;
    }
  }
  return best_insert_position;
}
double SuccessiveInclusionInitialRoute::get_insert_cost(
    const Route &route, const unsigned int insert_position,
    const MapData &map_data, const unsigned int current_city) {
  Route new_route = route;
  new_route.insert_after(insert_position, current_city);
  return route.evaluate(new_route, map_data);
}

const std::string SuccessiveInclusionInitialRoute::parameters() const {
  return "Starting City = " + std::to_string(starting_city);
}
