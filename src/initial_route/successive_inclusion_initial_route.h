#ifndef SUCCESSIVE_INCLUSION_INITIAL_ROUTE_H_
#define SUCCESSIVE_INCLUSION_INITIAL_ROUTE_H_

#include "initial_route.h"

class SuccessiveInclusionInitialRoute : public InitialRoute {
public:
  SuccessiveInclusionInitialRoute() = default;
  SuccessiveInclusionInitialRoute(const SuccessiveInclusionInitialRoute &) =
      delete;
  void generateInitialSolution(const MapData &map_data) override;
  const std::string name() const override { return "Successive Inclusion"; }
  const std::string title() const override {
    return name() + " start " + std::to_string(starting_city);
  }
  // std::unique_ptr<InitialRoute> clone_unique_ptr() const override {
  //   return std::make_unique<SuccessiveInclusionInitialRoute>(*this);
  // }

private:
  const std::string parameters() const override;

  unsigned int find_best_insert_position(const Route &route,
                                         const unsigned int current_city,
                                         const MapData &map_data);
  double get_insert_cost(const Route &route, const unsigned int insert_position,
                         const MapData &map_data,
                         const unsigned int current_city);
};

#endif // SUCCESSIVE_INCLUSION_INITIAL_ROUTE_H_