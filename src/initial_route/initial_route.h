// class to create an initial order or tour in the TSP Solver program

#ifndef INITIAL_ORDER_H_
#define INITIAL_ORDER_H_
#include <algorithm>
#include <chrono>
#include <definitions.h>
#include <map>
#include <map_data.h>
#include <memory>
#include <mutex>
#include <random>
#include <random_number.h>
#include <route.h>
#include <search_signals_base.h>
#include <search_signals_empty.h>
#include <set>
#include <solution.h>
#include <sstream>
#include <thread>
#include <uniform_double_random_number.h>
#include <unistd.h>
#include <vector>

class InitialRoute {
public:
  InitialRoute();
  InitialRoute(const InitialRoute &) = delete;
  virtual ~InitialRoute() {}
  void set_signal_functions(SearchSignalsBase *search_signal_functions) {
    this->search_signal_functions = search_signal_functions;
  }
  void setStartingCity(unsigned int starting_city);
  void setSeed(unsigned int seed);
  void setDelay(double delay);

  bool searching();
  void search(const MapData &map_data);
  void end_search() { searching_ = false; }

  virtual void generateInitialSolution(const MapData &map_data) {}
  virtual const std::string name() const { return ""; }
  virtual const std::string title() const { return ""; }

  Route current_route() const;
  std::string summary();
  std::vector<std::string> search_text_strings() const;

protected:
  unsigned int starting_city;
  unsigned int seed;

  virtual const std::string parameters() const { return ""; }

  void set_current_route(const Route &new_route);

  double delay;

  void update_route(Route route);
  void solution_summary(const std::string &initial_solution,
                        const MapData &map_data);

  void setup_city_insertion_order(std::vector<unsigned int> &cities,
                                  const MapData &map_data,
                                  bool include_starting_city = false);

  void print_route_status(const Route &route, const MapData &map_data);
  void delay_search();

  bool searching() const { return searching_; }

private:
  bool searching_;
  mutable std::shared_mutex searching_mutex;
  Route current_route_;
  mutable std::shared_mutex current_route_mutex;
  SearchSignalsBase *search_signal_functions;
  std::string summary_;
  mutable std::shared_mutex summary_mutex;
  std::vector<std::string> search_text_string_;
  mutable std::shared_mutex search_text_string_mutex;

  SearchSignalsEmpty search_signals_empty;
};

#endif // INITIAL_ORDER_H_
