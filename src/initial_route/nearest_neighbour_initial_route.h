#ifndef NEAREST_NEIGHBOUR_INITIAL_ROUTE_H_
#define NEAREST_NEIGHBOUR_INITIAL_ROUTE_H_

#include "initial_route.h"

class NearestNeighbourInitialRoute : public InitialRoute {
public:
  NearestNeighbourInitialRoute() = default;
  NearestNeighbourInitialRoute(const NearestNeighbourInitialRoute &) = delete;
  void generateInitialSolution(const MapData &map_data) override;
  const std::string name() const override { return "Nearest Neighbour"; }
  const std::string title() const override {
    return name() + " start " + std::to_string(starting_city);
  }
  // std::unique_ptr<InitialRoute> clone_unique_ptr() const override {
  //   return std::make_unique<NearestNeighbourInitialRoute>(*this);
  // }

private:
  const std::string parameters() const override;

  void sort_by_nearest_neighbour(std::vector<unsigned int> &cities,
                                 const MapData &map_data);
  unsigned int find_nearest_city(std::set<unsigned int> remaining_cities,
                                 unsigned int last_city,
                                 const MapData &map_data);
  void setup_remaining_cities(std::set<unsigned int> &remaining_cities,
                              const MapData &map_data);
};

#endif // NEAREST_NEIGHBOUR_INITIAL_ROUTE_H_