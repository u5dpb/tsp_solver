// functions for the InitialRoute class

#include "initial_route.h"
#include <iostream>

InitialRoute::InitialRoute() {
  std::unique_lock searching_lock(searching_mutex);
  searching_ = false;
  searching_lock.unlock();
  delay = 0u;
  searching_ = true;
  search_signal_functions = &search_signals_empty;
  starting_city = 1;
  seed = 1;
}

void InitialRoute::solution_summary(const std::string &initial_solution,
                                    const MapData &map_data) {
  std::ostringstream output_string_stream;
  output_string_stream << name() << " ";
  output_string_stream << "Initial order: ";
  output_string_stream << initial_solution;
  output_string_stream << "\n";
  output_string_stream << parameters() << "\n";
  std::shared_lock route_lock(current_route_mutex);
  output_string_stream << "Solution cost = "
                       << Route::evaluate(current_route_, map_data) << "\n";
  route_lock.unlock();
  std::unique_lock summary_lock(summary_mutex);
  summary_ += output_string_stream.str();
  summary_lock.unlock();
  search_signal_functions->signal_function_summary_update();
}

std::string InitialRoute::summary() {
  std::unique_lock summary_lock(summary_mutex);
  std::string return_string = summary_;
  summary_ = "";
  summary_lock.unlock();
  return return_string;
}

void InitialRoute::setStartingCity(unsigned int starting_city) {
  this->starting_city = starting_city;
}

void InitialRoute::setSeed(unsigned int seed) { this->seed = seed; }

void InitialRoute::setup_city_insertion_order(std::vector<unsigned int> &cities,
                                              const MapData &map_data,
                                              bool include_starting_city) {
  cities.clear();
  for (auto i = 1u; i <= map_data.no_locations(); ++i)
    if (i != starting_city || include_starting_city)
      cities.push_back(i);
}

void InitialRoute::print_route_status(const Route &route,
                                      const MapData &map_data) {
  std::ostringstream output_string_stream;
  output_string_stream << "Current cost: ";
  std::shared_lock route_lock(current_route_mutex);
  output_string_stream << Route::evaluate(route, map_data);
  route_lock.unlock();
  std::unique_lock search_text_string_lock(search_text_string_mutex);
  search_text_string_.resize(0);
  search_text_string_.push_back(name());
  search_text_string_.push_back(output_string_stream.str());
  search_text_string_lock.unlock();
  search_signal_functions->signal_function_search_status_update();
}

std::vector<std::string> InitialRoute::search_text_strings() const {
  std::shared_lock search_text_string_lock(search_text_string_mutex);
  return search_text_string_;
}

Route InitialRoute::current_route() const {
  std::shared_lock lock(current_route_mutex);
  return current_route_;
}

bool InitialRoute::searching() {
  std::shared_lock lock(searching_mutex);
  return searching_;
}

void InitialRoute::search(const MapData &map_data) {
  std::unique_lock lock(searching_mutex);
  searching_ = true;
  lock.unlock();
  generateInitialSolution(map_data);
  lock.lock();
  searching_ = false;
  lock.unlock();
}

void InitialRoute::setDelay(double delay) { this->delay = delay; }

void InitialRoute::set_current_route(const Route &route) {
  std::unique_lock lock(current_route_mutex);
  this->current_route_ = route;
  lock.unlock();
  search_signal_functions->signal_function_initial_route_changed();
}

void InitialRoute::delay_search() {
  std::this_thread::sleep_for(std::chrono::milliseconds(int(delay * 1000.0)));
}