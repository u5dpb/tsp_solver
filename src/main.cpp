// the main loop for the TSP Solver program

#include <glibmm.h>
#include <gtkmm.h>
#include <gtkmm/main.h>

#include "main_window.h"

int main(int argc, char *argv[]) {
  try {
    if (!Glib::thread_supported())
      Glib::thread_init(); // set up threads in not already done so

    Gtk::Main kit(argc, argv);
    MainWindow main_window;

    Gtk::Main::run(main_window);
  } catch (char *error_string) {
    std::cout << error_string << "\n";
  }
  exit(0);
}
