// the menu functions as used in the TSP Solver program
#ifndef MENU_FUNCTIONS_H
#define MENU_FUNCTIONS_H

#include "menu_functions.h"

void openfile(GtkWidget *widget) {
  // std::string filename="";
  // GtkWidget *open_dialog=gtk_file_chooser_dialog_new(
  //                         "Open File", NULL,
  //                         GTK_FILE_CHOOSER_ACTION_OPEN,
  //                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
  //                         GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
  //                         NULL);
  // if (gtk_dialog_run(GTK_DIALOG(open_dialog)) == GTK_RESPONSE_ACCEPT)
  // {
  //   filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(open_dialog));

  //   std::ifstream in;
  //   in.open(filename.c_str(), std::ios::in);
  //   if (!in)
  //   {
  //     std::cerr << "Error in void openfile()\nCannot open file: " << filename
  //     << "\n"; exit(-1);
  //   }
  //   int number_of_cities;
  //   in >> number_of_cities;
  //   map_data.locations.clear();
  //   map_data.order.clear();
  //   for (int i=1; i<=number_of_cities; ++i)
  //   {
  //     Coord c;
  //     in >> c.x >> c.y;
  //     c.x*=scale::scale;
  //     c.y*=scale::scale;
  //     c.x+=scale::x_plus;
  //     c.y+=scale::y_plus;
  //     map_data.locations.push_back(c);
  //     map_data.order.push_back(i-1);
  //   }
  //   in.close();
  //   in.clear();
  // }
  // gtk_widget_destroy(open_dialog);

  // // write the current filename in the parameters tab
  // GtkWidget *label_filename=glade_xml_get_widget(xml, "label_filename");
  // gtk_label_set_text(GTK_LABEL(label_filename),filename.c_str());

  // // alter the starting city spin button to the range for these cities
  // GtkWidget *spinbutton_startingcity=glade_xml_get_widget(xml,
  // "spinbutton_startingcity");
  // gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinbutton_startingcity),
  //                           1,map_data.locations.size());
}

#endif