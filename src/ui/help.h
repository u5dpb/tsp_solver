// the help window

#ifndef HELP_WINDOW_H
#define HELP_WINDOW_H

#include <gtkmm.h>

class HelpWindow : public Gtk::Window
{
  public:
    HelpWindow();
    virtual ~HelpWindow();
  private:
    Gtk::Fixed fixed_area;
    Gtk::Label label_layout1;
    Gtk::Label label_layout2;
    Gtk::Label label_layout3;
    Gtk::Label label_layout4;
    Gtk::Button w_button;
    virtual void on_button_clicked();
};

#endif
