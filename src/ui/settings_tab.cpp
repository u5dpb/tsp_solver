// functions for the SettingsTab class

#include "settings_tab.h"
#include <iostream>

SettingsTab::SettingsTab(const MapData &map_data)
    : map_data(map_data), label_filename("Filename:"),
      label_delay_factor("Delay Factor"),
      label_initial_solution("Initial Solution"),
      //          label_starting_city("Starting City"),
      label_improvement_option("Improvement Option"),
      label_text_period("Text Period"), w_filename("No file loaded"),
      adj_delay_factor(Gtk::Adjustment::create(0, 0, 100, 0.1, 10, 0)),
      w_delay_factor(adj_delay_factor, 1.0, 2),
      //          adj_starting_city(Gtk::Adjustment::create(0, 0, 0, 1, 10, 0)),
      //          w_starting_city(adj_starting_city),
      adj_text_period(Gtk::Adjustment::create(100, 0, 100000, 1, 10, 0)),
      w_text_period(adj_text_period),
      label_layout6("<b>Neighbourhood Option</b>"),
      search_button("   Start Search   "),
      stop_search_button("   Stop Search   ") {

  set_border_width(10);

  setup_filename_widget();
  setup_delay_factor_widget();
  setup_initial_solution_widget();
  //    setup_starting_city_widget();
  setup_improvement_option_widget();
  setup_text_period_widget();
  display = true;

  put(initial_order_settings_frame, 360, 145);

  setup_neighbourhood_widget();
  setup_search_button();
  setup_stop_search_button();
  setup_search_method_frames();
  toggleSearchOff();
}

void SettingsTab::setup_filename_widget() {
  put(label_filename, 20, 15);
  put(w_filename, 103, 15);
}

void SettingsTab::setup_delay_factor_widget() {
  put(label_delay_factor, 20, 65);
  put(w_delay_factor, 140, 60);
}

void SettingsTab::setup_initial_solution_widget() {
  put(label_initial_solution, 20, 105);
  for (const auto initial_route_option :
       initial_route_methods::InitialRouteOptionAll) {
    w_initial_solution.append(
        initial_route_methods::to_string(initial_route_option));
  }
  w_initial_solution.set_active_text(initial_route_methods::to_string(
      initial_route_methods::InitialRouteOptionAll[0]));
  w_initial_solution.signal_changed().connect(
      sigc::mem_fun(*this, &SettingsTab::on_initial_solution_change));
  put(w_initial_solution, 140, 100);
}

// void SettingsTab::setup_starting_city_widget() {
//     put(label_starting_city, 360, 145);
//     put(w_starting_city, 480, 140);
// }

void SettingsTab::setup_improvement_option_widget() {
  put(label_improvement_option, 20, 185);
  for (const auto search_option : search_methods::SearchOptionAll) {
    w_search.append(search_methods::search_option_str[search_option]);
  }
  w_search.set_active_text(
      search_methods::search_option_str[search_methods::SearchOptionAll[0]]);
  put(w_search, 140, 180);
}

void SettingsTab::setup_text_period_widget() {
  put(label_text_period, 20, 220);
  put(w_text_period, 140, 220);
}

void SettingsTab::setup_neighbourhood_widget() {
  label_layout6.set_use_markup();
  neighbourhood_vbox.pack_start(label_layout6, Gtk::PACK_SHRINK);
  for (int i = 0; i < int(neighbourhood::END); ++i) {
    Gtk::CheckButton *check =
        new Gtk::CheckButton(neighbourhood::neighbourhood_str(i));
    check->signal_clicked().connect(sigc::bind<neighbourhood::NEIGHBOURHOOD>(
        sigc::mem_fun(*this, &SettingsTab::on_neighbourhood_check_changed),
        neighbourhood::NEIGHBOURHOOD(i)));
    w_check_neighbour.push_back(check);
  }
  for (int i = 0; i < w_check_neighbour.size(); ++i)
    neighbourhood_vbox.pack_start(*w_check_neighbour[i], Gtk::PACK_SHRINK);
  put(neighbourhood_vbox, 580, 60);
}

void SettingsTab::setup_search_button() {
  put(search_button, 100, 475);
  search_button.signal_clicked().connect(
      sigc::mem_fun(*this, &SettingsTab::on_search_clicked));
}

void SettingsTab::setup_stop_search_button() {
  put(stop_search_button, 100, 515);
  stop_search_button.signal_clicked().connect(
      sigc::mem_fun(*this, &SettingsTab::on_stop_search_clicked));
}

void SettingsTab::setup_search_method_frames() {
  for (const auto search_method : search_methods::SearchOptionAll) {
    if (search_methods::validSearch(search_method)) {
      search_settings[search_method] =
          search_methods::searchSettingsFrameFactory(search_method);
    }
  }
  unsigned int x_pos = 20;
  for (const auto search_method : search_methods::SearchOptionAll) {
    if (search_methods::validSearch(search_method)) {
      put(*search_settings[search_method], x_pos, 279);
      x_pos += 320;
    }
  }
}

SettingsTab::~SettingsTab() {
  for (int i = 0; i < w_check_neighbour.size(); ++i) {
    delete w_check_neighbour[i];
  }
}

void SettingsTab::mapDataUpdate() {
  setFilename(map_data.filename());
  setCityLimits(1, map_data.no_locations());
  //    initial_order_settings_frame.set_starting_city_range(1,
  //    map_data.no_locations()); no_locations = map_data.no_locations();
}

void SettingsTab::on_neighbourhood_check_changed(
    neighbourhood::NEIGHBOURHOOD n) {
  if (w_check_neighbour[int(n)]->get_active()) { // it's newly active
    search_neighbourhood.insert(n);
  } else { // if it's no longer active
    search_neighbourhood.erase(search_neighbourhood.find(n));
  }
  updateSettingsHelp();
}

void SettingsTab::on_initial_solution_change() {
  if (initial_route_methods::uses_seed(getInitialRouteOption()))
    initial_order_settings_frame.enable_initial_seed();
  else
    initial_order_settings_frame.disable_initial_seed();
  if (initial_route_methods::uses_starting_city(getInitialRouteOption()))
    initial_order_settings_frame.enable_starting_city();
  else
    initial_order_settings_frame.disable_starting_city();
}

void SettingsTab::on_check_display() {
  if (w_display.get_active())
    display = true;
  else
    display = false;
}

std::set<neighbourhood::NEIGHBOURHOOD> SettingsTab::getSearchNeighbourhood() {
  return search_neighbourhood;
}

bool SettingsTab::searchNeighbourhood(
    neighbourhood::NEIGHBOURHOOD neighbourhood) {
  return search_neighbourhood.find(neighbourhood) != search_neighbourhood.end();
}

double SettingsTab::getDelayFactor() { return w_delay_factor.get_value(); }

unsigned int SettingsTab::getStartingCity() const {
  return initial_order_settings_frame.getStartingCity();
}

unsigned int SettingsTab::getSeed() const {
  return initial_order_settings_frame.getInitialSeed();
}

initial_route_methods::InitialRouteOption SettingsTab::getInitialRouteOption() {
  std::string text = w_initial_solution.get_active_text();
  auto initial_route_option = initial_route_methods::InitialRouteOption::none;
  for (int i = 0; i < int(initial_route_methods::InitialRouteOption::end);
       ++i) {
    if (initial_route_methods::to_string(
            initial_route_methods::InitialRouteOption(i)) == text)
      initial_route_option = initial_route_methods::InitialRouteOption(i);
  }
  return initial_route_option;
}

search_methods::SearchOption SettingsTab::getSearchOption() {
  std::string text = w_search.get_active_text();
  //    search_option = search_methods::search_option_from_str[text];
  return search_methods::search_option_from_str[text];
}

void SettingsTab::setFilename(std::string filename) {
  this->filename = filename;
  w_filename.set_text(filename);
}

void SettingsTab::setCityLimits(int lower, int upper) {
  initial_order_settings_frame.set_starting_city_range(lower, upper);
  no_locations = upper;
  updateSettingsHelp();
}

bool SettingsTab::getDisplay() { return display; }

int SettingsTab::getTextPeriod() { return int(w_text_period.get_value()); }

void SettingsTab::updateSettingsHelp() {
  // if (map_data.filename().size() > 0) {
  //   sa_settings.updateSettingsHelp(no_locations);
  // }
  // if (map_data.filename().size() > 0) {
  //   std::vector<unsigned int> order;
  //   for (unsigned int i = 1; i <= no_locations; ++i)
  //     order.push_back(i);
  //   // Route test_route(order, getSearchNeighbourhood());
  //   Route test_route(order);

  //   tabu_settings.updateSettingsHelp(no_locations, 0);
  // }
}

void SettingsTab::on_search_clicked() { signal_search(); }

void SettingsTab::on_stop_search_clicked() { signal_stop_search(); }

void SettingsTab::setUpSearchInstance(
    std::unique_ptr<Search> &search_instance) {
  search_instance->setDelay(getDelayFactor());
  search_instance->setTextPeriod(getTextPeriod());
  std::string text = w_search.get_active_text();
  search_settings[search_methods::search_option_from_str[text]]
      ->setUpSearchInstance(search_instance);
}

void SettingsTab::searchActive(const bool search_active) {
  search_button.set_sensitive(!search_active);
}

void SettingsTab::toggleSearchOn() {
  search_button.set_sensitive(false);
  stop_search_button.set_sensitive(true);
}

void SettingsTab::toggleSearchOff() {
  search_button.set_sensitive(true);
  stop_search_button.set_sensitive(false);
}