//
// Created by danb on 15/12/2021.
//

#ifndef TSP_SOLVER_INITIAL_ORDER_SETTINGS_FRAME_H
#define TSP_SOLVER_INITIAL_ORDER_SETTINGS_FRAME_H

#include <gtkmm.h>

class InitialOrderSettingsFrame : public Gtk::Frame {
public:
    InitialOrderSettingsFrame();

    int getStartingCity() const;
    void set_starting_city_range(int lower, int upper);

    int getInitialSeed() const;

    void enable_starting_city();
    void disable_starting_city();

    void enable_initial_seed();
    void disable_initial_seed();

protected:
    static const int frame_margin = 5;
    Gtk::Table table;
    Glib::RefPtr<Gtk::Adjustment>   adj_starting_city,adj_initial_seed;
    Gtk::SpinButton w_starting_city,w_initial_seed;
    Gtk::Label label_starting_city,label_initial_seed;

};


#endif //TSP_SOLVER_INITIAL_ORDER_SETTINGS_FRAME_H
