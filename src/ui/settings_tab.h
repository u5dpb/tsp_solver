// a class to display and record the settings for the TSP Solver program

#ifndef SETTINGS_TAB_H
#define SETTINGS_TAB_H

#include <gtkmm.h>
#include <map>
#include <memory>
#include <set>
#include <vector>

#include "definitions.h"
#include "first_found_descent_settings_frame.h"
#include "initial_order_settings_frame.h"
#include "initial_route.h"
#include "initial_route_option.h"
#include "map_data.h"
#include "move.h"
#include "route.h"
#include "search.h"
#include "search_option.h"
#include "simulated_annealing_settings_frame.h"
#include "solution.h"
#include "steepest_descent_settings_frame.h"
#include "tabu_settings_frame.h"

class SettingsTab : public Gtk::Fixed {
public:
  SettingsTab(const MapData &map_data);
  virtual ~SettingsTab();

  // void update_initial_route_methods();
  // void update_search_methods();

  std::set<neighbourhood::NEIGHBOURHOOD> getSearchNeighbourhood();
  bool searchNeighbourhood(neighbourhood::NEIGHBOURHOOD neighbourhood);

  double getDelayFactor();
  unsigned int getStartingCity() const;
  unsigned int getSeed() const;
  search_methods::SearchOption getSearchOption();
  initial_route_methods::InitialRouteOption getInitialRouteOption();

  bool getDisplay();
  int getTextPeriod();
  void setUpSearchInstance(std::unique_ptr<Search> &search_instance);
  // int getSearchMethodIndex() const;

  // std::string getFilename();

  void setFilename(std::string filename);
  void setCityLimits(int lower, int upper);

  std::map<search_methods::SearchOption, std::shared_ptr<SearchSettingsFrame>>
      search_settings;
  // TabuSettingsFrame tabu_settings;
  // SimulatedAnnealingSettingsFrame sa_settings;
  // SteepestDescentSettingsFrame steepest_descent_settings;
  // FirstFoundDescentSettingsFrame first_found_descent_settings;

  Glib::Dispatcher signal_search;
  Glib::Dispatcher signal_stop_search;

  void mapDataUpdate();

  void searchActive(const bool search_active);
  void toggleSearchOn();
  void toggleSearchOff();

protected:
  //    Gtk::Fixed main_fixed;
  // data
  Gtk::Label label_filename;
  Gtk::Label label_delay_factor;
  Gtk::Label label_initial_solution;
  //  Gtk::Label label_starting_city;
  Gtk::Label label_improvement_option;
  Gtk::Label label_layout6;
  Gtk::Label label_text_period;

  InitialOrderSettingsFrame initial_order_settings_frame;

  // the data Widgets - w in the variable name stands for widget
  Gtk::Label w_filename;
  Glib::RefPtr<Gtk::Adjustment> adj_delay_factor,
      // adj_starting_city,
      adj_text_period;
  Gtk::SpinButton w_delay_factor;
  Gtk::ComboBoxText w_initial_solution;
  // Gtk::SpinButton w_starting_city;
  Gtk::ComboBoxText w_search;
  Gtk::CheckButton w_display;
  Gtk::SpinButton w_text_period;
  // neighbourhood options
  Gtk::VBox neighbourhood_vbox;
  std::vector<Gtk::CheckButton *> w_check_neighbour;

  Gtk::Button search_button;
  Gtk::Button stop_search_button;

  std::set<neighbourhood::NEIGHBOURHOOD> search_neighbourhood;
  std::string filename;
  int no_locations;
  // double delay_factor;
  //  int starting_city;
  //  search_methods::SearchOption search_option;
  //  initial_route_methods::InitialRouteOption initial_route_option;
  // int search_method_index;
  bool display;
  //  int text_period;

  // signal functions
  virtual void on_neighbourhood_check_changed(neighbourhood::NEIGHBOURHOOD n);
  //   virtual void on_delay_factor_change();
  virtual void on_initial_solution_change();
  //  virtual void on_starting_city_change();
  //  virtual void on_search_change();
  virtual void on_check_display();
  //  virtual void on_text_period_change();
  virtual void on_search_clicked();
  virtual void on_stop_search_clicked();

  void updateSettingsHelp();

  const MapData &map_data;
  // const std::vector<InitialRoute *> &initial_route_methods;
  // const std::vector<Search<Solution> *> &search_methods;

  void setup_filename_widget();
  void setup_delay_factor_widget();
  void setup_initial_solution_widget();
  void setup_starting_city_widget();
  void setup_improvement_option_widget();
  void setup_text_period_widget();
  void setup_neighbourhood_widget();
  void setup_search_button();
  void setup_stop_search_button();
  void setup_search_method_frames();
};

#endif
