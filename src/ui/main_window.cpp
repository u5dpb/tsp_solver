// the functions for the MainWindow class of the TSP Solver application

#include "main_window.h"

MainWindow::MainWindow() : map_drawing_area(map_data), settings_tab(map_data) {

  set_title("TSP Solver");
  set_default_size(500, 350);
  maximize();

  end_search = false;

  add(main_v_box);

  // create the menu and tool bar
  action_group = Gtk::ActionGroup::create();
  setup_action_group();

  ui_manager = Gtk::UIManager::create();
  ui_manager->insert_action_group(action_group);
  add_accel_group(ui_manager->get_accel_group());
  setup_ui_manager();

  main_v_box.pack_start(*ui_manager->get_widget("/MenuBar"), Gtk::PACK_SHRINK);
  main_v_box.pack_start(*ui_manager->get_widget("/ToolBar"), Gtk::PACK_SHRINK);
  main_v_box.pack_start(notebook, Gtk::PACK_EXPAND_WIDGET);

  notebook.append_page(scrolled_settings, "Settings");
  notebook.append_page(scrolled_window, "Output");
  notebook.append_page(map_drawing_area, "Map");

  map_drawing_area.show();

  scrolled_window.add(text_output);
  scrolled_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  scrolled_settings.add(settings_tab);
  scrolled_settings.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  setup_ui_signals();
  show_all_children();
}

MainWindow::~MainWindow() {}

void MainWindow::setup_ui_signals() {
  settings_tab.signal_search.connect(
      sigc::mem_fun(*this, &MainWindow::on_menu_file_search));
  settings_tab.signal_stop_search.connect(
      sigc::mem_fun(*this, &MainWindow::end_premature));
  signal_map_data_changed.connect(
      sigc::mem_fun(*this, &MainWindow::update_map_data));
  map_data.set_call_signal_map_data_changed(
      [&]() { signal_map_data_changed(); });
  // map_data.signal_map_data_changed.connect(
  //     sigc::mem_fun(*this, &MainWindow::update_map_data));
  signal_search_status_on.connect(
      sigc::mem_fun(settings_tab, &SettingsTab::toggleSearchOn));
  signal_search_status_off.connect(
      sigc::mem_fun(settings_tab, &SettingsTab::toggleSearchOff));
};

void MainWindow::setup_action_group() {
  action_group->add(Gtk::Action::create("FileMenu", "File"));
  action_group->add(Gtk::Action::create("FileOpen", Gtk::Stock::OPEN, "_Open",
                                        "Open a map file"),
                    sigc::mem_fun(*this, &MainWindow::on_menu_file_open));
  action_group->add(Gtk::Action::create("FileSave", Gtk::Stock::SAVE, "_Save",
                                        "Save output file"),
                    sigc::mem_fun(*this, &MainWindow::on_menu_file_save));
  action_group->add(Gtk::Action::create("FileEnd", Gtk::Stock::NO,
                                        "_End Search", "end the search"),
                    sigc::mem_fun(*this, &MainWindow::end_premature));
  action_group->add(
      Gtk::Action::create("FileQuit", Gtk::Stock::QUIT, "_Quit", "Quit"),
      sigc::mem_fun(*this, &MainWindow::on_menu_file_quit));
  // the edit menu
  action_group->add(Gtk::Action::create("EditMenu", "Edit"));
  action_group->add(Gtk::Action::create("EditClear", Gtk::Stock::CLEAR,
                                        "_Clear", "Clear output"),
                    sigc::mem_fun(*this, &MainWindow::on_menu_edit_clear));
  // the help menu
  action_group->add(Gtk::Action::create("HelpMenu", "Help"));
  action_group->add(Gtk::Action::create("HelpAbout", Gtk::Stock::COPY, "_About",
                                        "About the program"),
                    sigc::mem_fun(*this, &MainWindow::on_menu_help_about));
}

void MainWindow::setup_ui_manager() {
  Glib::ustring ui_info = "<ui>"
                          "  <menubar name='MenuBar'>"
                          "    <menu action='FileMenu'>"
                          "      <menuitem action='FileOpen'/>"
                          "      <menuitem action='FileSave'/>"
                          "      <menuitem action='FileEnd'/>"
                          "      <menuitem action='FileQuit'/>"
                          "    </menu>"
                          "    <menu action='EditMenu'>"
                          "      <menuitem action='EditClear'/>"
                          "    </menu>"
                          "    <menu action='HelpMenu'>"
                          "      <menuitem action='HelpAbout'/>"
                          "    </menu>"
                          "  </menubar>"
                          "  <toolbar  name='ToolBar'>"
                          "    <toolitem action='FileOpen'/>"
                          "    <toolitem action='FileSave' />"
                          "    <toolitem action='FileEnd' />"
                          "    <toolitem action='EditClear' />"
                          "    <toolitem action='FileQuit'/>"
                          "  </toolbar>"
                          "</ui>";

  ui_manager->add_ui_from_string(ui_info);
}

void MainWindow::on_menu_file_open() {
  Gtk::FileChooserDialog dialog("Please choose a file",
                                Gtk::FILE_CHOOSER_ACTION_OPEN);
  dialog.set_transient_for(*this);

  dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

  auto filter_text = Gtk::FileFilter::create();
  filter_text->set_name("Text files");
  filter_text->add_mime_type("text/plain");

  int result = dialog.run();
  if (result == Gtk::RESPONSE_OK) {
    map_drawing_area.clearOrder();
    file_io.load_file(dialog.get_filename(),
                      std::bind(&MapData::load_map_data, &map_data,
                                std::placeholders::_1, std::placeholders::_2));
  }
  update_text("Filename: " + dialog.get_filename() + "\n");
}

void MainWindow::on_menu_file_save() {
  Gtk::FileChooserDialog dialog("Save file", Gtk::FILE_CHOOSER_ACTION_SAVE);
  dialog.set_transient_for(*this);

  dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  dialog.add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_ACCEPT);
  dialog.set_current_name("Untitled file");

  int result = dialog.run();
  if (result == Gtk::RESPONSE_ACCEPT) {
    file_io.save_file(dialog.get_filename());
  }
}

void MainWindow::setup_search() {
  initial_route_algorithm = initial_route_methods::initialRouteFactory(
      settings_tab.getInitialRouteOption());
  setup_initial_route_signals();
  initial_route_algorithm->setStartingCity(settings_tab.getStartingCity());
  initial_route_algorithm->setSeed(settings_tab.getSeed());
  initial_route_algorithm->setDelay(settings_tab.getDelayFactor());
  std::vector<std::string> search_title = {initial_route_algorithm->title()};

  if (search_methods::validSearch(settings_tab.getSearchOption())) {
    std::unique_ptr<BaseSolution> null_solution =
        std::make_unique<Solution>(Route());
    Neighbourhood neighbourhood(
        settings_tab.searchNeighbourhood(neighbourhood::SWAP_2_ADJ),
        settings_tab.searchNeighbourhood(neighbourhood::SWAP_2_ANY),
        settings_tab.searchNeighbourhood(neighbourhood::REINSERT),
        settings_tab.searchNeighbourhood(neighbourhood::REVERSE));
    search_algorithm = search_methods::searchFactory(
        settings_tab.getSearchOption(), null_solution,
        std::make_unique<Neighbourhood>(neighbourhood));
    setup_search_signals();
    settings_tab.setUpSearchInstance(search_algorithm);
    for (const auto subtitle : search_algorithm->title())
      search_title.push_back(subtitle);
  }
  map_drawing_area.setSearchTitle(search_title);
}

void MainWindow::setup_initial_route_signals() {
  initial_signals.signal_initial_route_changed.connect(
      sigc::mem_fun(*this, &MainWindow::update_route_initial));
  initial_signals.signal_summary_update.connect(
      sigc::mem_fun(*this, &MainWindow::update_initial_text));
  initial_signals.signal_search_status_update.connect(
      sigc::mem_fun(*this, &MainWindow::update_initial_status_text));
}

void MainWindow::setup_search_signals() {
  search_algorithm->set_signal_functions(&search_signals);
  search_signals.signal_current_route_changed.connect(
      sigc::mem_fun(*this, &MainWindow::update_route_search));
  search_signals.signal_search_status_update.connect(
      sigc::mem_fun(*this, &MainWindow::update_search_status_text));
  search_signals.signal_summary_update.connect(
      sigc::mem_fun(*this, &MainWindow::update_search_text));
}

void MainWindow::run_search() {
  signal_search_status_on();
  if (!end_search) {
    initial_route_algorithm->set_signal_functions(&initial_signals);
    initial_route_algorithm->search(map_data);
  }
  if (search_methods::validSearch(settings_tab.getSearchOption())) {

    if (!end_search) {
      Solution best_solution(initial_route_algorithm->current_route());
      search_algorithm->set_initial_solution(
          std::make_unique<Solution>(best_solution));
      search_algorithm->set_signal_functions(&search_signals);
    }
    if (!end_search) {
      search_algorithm->run_search(map_data);
    }
  }
  end_search = false;
  signal_search_status_off();
}

// void MainWindow::update_search_status() { settings_tab.toggleSearchActive();
// }

void MainWindow::on_menu_file_search() {
  if (int(settings_tab.getInitialRouteOption()) > 0 &
      map_data.no_locations() > 0) {
    setup_search();
    search_thread = std::thread(&MainWindow::run_search, this);
    search_thread.detach();
  }
}

void MainWindow::on_menu_file_quit() { hide(); }

void MainWindow::on_menu_edit_copy() {}

void MainWindow::on_menu_edit_clear() {
  text_output.clear();
  // map_drawing_area.clearOrder();
  update_map();
  update_text("");
}

void MainWindow::on_menu_help_about() {
  // help_window.show();
  Gtk::MessageDialog dialog(*this, "About TSP Solver");
  dialog.set_secondary_text(
      "TSP Solver\nVersion 1.0\nWritten by Dan Black\nPlease report errors to "
      "dan.black@lancaster.ac.uk");

  dialog.run();
}

void MainWindow::update_search_status_text() {
  std::unique_lock lock(gui_mutex);
  map_drawing_area.setSearchText(search_algorithm->search_status());
}

void MainWindow::update_initial_status_text() {
  std::vector<std::string> update_strings;
  update_strings = initial_route_algorithm->search_text_strings();
  std::unique_lock lock(gui_mutex);
  map_drawing_area.setSearchText(update_strings);
}

void MainWindow::update_map() {}

void MainWindow::update_map_initial() {}

void MainWindow::update_text(std::string text) {
  std::unique_lock lock(gui_mutex);
  text_output.updateBuffer(text);
}

void MainWindow::update_initial_text() {
  std::string summary = initial_route_algorithm->summary();
  std::unique_lock lock(gui_mutex);
  text_output.updateBuffer(summary);
}

void MainWindow::update_search_text() {
  std::unique_lock lock(gui_mutex);
  for (const auto text : search_algorithm->summary()) {

    text_output.updateBuffer(text);
  }
}

void MainWindow::end_premature() {
  end_search = true;
  if (initial_route_algorithm != nullptr) {
    initial_route_algorithm->end_search();
  }
  if (search_algorithm != nullptr) {
    search_algorithm->end_search();
  }
}

void MainWindow::update_map_data() {
  std::unique_lock lock(gui_mutex);
  settings_tab.mapDataUpdate();
  map_drawing_area.show();
}

void MainWindow::update_route_search() {
  std::unique_lock lock(gui_mutex);
  map_drawing_area.setOrder(search_algorithm->current_route());
  map_drawing_area.queue_draw();
}

void MainWindow::update_route_initial() {
  std::unique_lock lock(gui_mutex);
  map_drawing_area.setOrder(initial_route_algorithm->current_route());
  map_drawing_area.queue_draw();
}
