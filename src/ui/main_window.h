// the main window of the TSP Solver program

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <gtkmm.h>
#include <gtkmm/stock.h>
#include <iostream>
#include <memory>
#include <mutex>

//#include "first_found.h"
#include <initial_route.h>
#include <map_data.h>
#include <nearest_neighbour_initial_route.h>
#include <random_initial_route.h>
#include <successive_inclusion_initial_route.h>
// #include "ui/help.h"
#include "map_drawing_area.h"
#include "settings_tab.h"
#include <file_io.h>
//#include "simulated_annealing.h"
//#include "steepest_descent.h"
#include "text_output.h"
#include <initial_route_option.h>
#include <neighbourhood.h>
#include <random_number.h>
#include <route.h>
#include <search.h>
#include <search_signals_glib.h>
#include <solution.h>
#include <tabu_search.h>
#include <uniform_double_random_number.h>

class MainWindow : public Gtk::Window {
public:
  MainWindow();
  virtual ~MainWindow();
  // Glib::Dispatcher sig_done;
  // related functions
  // void load_file(std::string filename);
  // void save_file(std::string filename);

protected:
  void setup_ui_signals();
  void setup_action_group();
  void setup_ui_manager();
  // signal handlers
  virtual void on_menu_file_open();
  virtual void on_menu_file_save();
  // virtual void on_menu_file_initial();
  virtual void on_menu_file_search();
  virtual void on_menu_file_quit();
  virtual void on_menu_edit_copy();
  virtual void on_menu_edit_clear();
  virtual void on_menu_help_about();

  virtual void update_map();
  virtual void update_map_initial();
  virtual void end_premature();

  // child widgets
  Gtk::VBox main_v_box;

  Glib::RefPtr<Gtk::UIManager> ui_manager;
  Glib::RefPtr<Gtk::ActionGroup> action_group;

  Gtk::Notebook notebook;
  SettingsTab settings_tab;
  MapDrawingArea map_drawing_area;
  Gtk::ScrolledWindow scrolled_window;
  Gtk::ScrolledWindow scrolled_settings;
  TextOutput text_output;

  std::unique_ptr<InitialRoute> initial_route_algorithm;
  std::unique_ptr<Search> search_algorithm;

  MapData map_data;
  file_io::FileIO file_io;

  void update_map_data();
  void update_route_initial();
  void update_route_search();
  void update_initial_text();
  void update_text(std::string text);
  void update_search_text();
  void update_search_status_text();
  void update_initial_status_text();
  std::mutex gui_mutex;
  // sigc::signal<void(bool)> signal_update_search_status;
  SearchSignalsGlib search_signals;
  SearchSignalsGlib initial_signals;
  Glib::Dispatcher signal_search_status_on;
  Glib::Dispatcher signal_search_status_off;
  Glib::Dispatcher signal_map_data_changed;

  // void update_search_status(bool search_status);
  // void update_search_status();

  void setup_search();
  void setup_search_signals();
  void setup_initial_route_signals();
  void run_search();

  std::thread search_thread;
  bool end_search;

  // HelpWindow help_window;
};

#endif
