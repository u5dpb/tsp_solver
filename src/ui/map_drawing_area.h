// location to store the data for the map of the TSP Solver program

#ifndef MAP_DRAWING_AREA_H_
#define MAP_DRAWING_AREA_H_

#include <cairomm/context.h>
#include <fstream>
#include <glibmm/thread.h>
#include <gtkmm.h>
#include <sstream>
#include <vector>

#include "definitions.h"
#include "map_data.h"
#include "route.h"

class MapDrawingArea : public Gtk::DrawingArea {
public:
  MapDrawingArea(const MapData &map_data);
  virtual ~MapDrawingArea();
  void setOrder(Route order);
  void clearOrder();
  void setSearchText(std::vector<std::string> search_text);
  void setSearchComplete(bool search_complete);
  void setSearchTitle(const std::vector<std::string> search_title);
  Route getOrder();
  // double getDistance(int location_1, int location_2);
  // std::vector<std::vector<double>> getDistances();
  virtual bool on_timeout();

protected:
  const MapData &map_data;

  static constexpr double scale = 2.5;
  static constexpr int x_plus = 200;
  static constexpr int y_plus = 10;
  // namespace scale
  // std::vector<Coord> locations;
  // std::vector<Coord> outline;
  Route order;
  // std::vector<std::vector<double>> distance;

  Glib::Mutex mutex;

  std::vector<std::string> search_text;
  std::vector<std::string> search_title;

  bool on_draw(const Cairo::RefPtr<Cairo::Context> &context) override;

  virtual void load_file_error(std::string file);
};

#endif // MAP_DARWING_AREA_H_
