// functions for the TextOutput class

#include "text_output.h"

TextOutput::TextOutput() {
  // buffer=Gtk::TextBuffer::create();
}

TextOutput::~TextOutput() {}

void TextOutput::printMapData(MapDrawingArea *map_data) {
  // char line[10];
  // Glib::RefPtr<Gtk::TextBuffer> buffer = get_buffer();
  // std::vector<Coord> outline = map_data->getOutline();
  // for (int i = 0; i < outline.size(); ++i) {
  //   sprintf(line, "%d %d\n", outline[i].x, outline[i].y);
  //   buffer->insert(buffer->end(), line);
  // }
}

void TextOutput::updateBuffer(std::string new_string) {
  std::unique_lock lock(buffer_mutex);
  Glib::RefPtr<Gtk::TextBuffer> buffer = get_buffer();
  buffer->insert(buffer->end(), new_string);
  // buffer->set_text(new_string);
  const Gtk::TextIter end = buffer->end();
  Glib::RefPtr<Gtk::TextMark> end_mark = buffer->create_mark(end);
  lock.unlock();
  scroll_to(end_mark);
  show();
}

void TextOutput::saveBuffer(std::string filename) {
  std::shared_lock lock(buffer_mutex);

  std::ofstream out;
  out.open(filename.c_str(), std::ios::out);
  if (!out) {
    save_file_error(filename);
    return;
  }
  Glib::RefPtr<Gtk::TextBuffer> buffer = get_buffer();
  const Gtk::TextIter start = buffer->begin();
  const Gtk::TextIter end = buffer->end();
  out << buffer->get_text(start, end);
  out.close();
  out.clear();
}

void TextOutput::clear() {
  std::unique_lock lock(buffer_mutex);

  Glib::RefPtr<Gtk::TextBuffer> buffer = get_buffer();
  buffer->set_text("");
  show();
}

void TextOutput::save_file_error(std::string file) {
  Gtk::MessageDialog dialog("Don't Panic!", false, Gtk::MESSAGE_ERROR,
                            Gtk::BUTTONS_CLOSE);
  dialog.set_secondary_text("Error: Cannot save to the file: " + file);
  dialog.run();
}
