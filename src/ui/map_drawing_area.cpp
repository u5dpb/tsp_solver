// functions for the MapData class

#include "map_drawing_area.h"
#include <iostream>

MapDrawingArea::MapDrawingArea(const MapData &map_data) : map_data(map_data) {
  search_text.clear();
}

MapDrawingArea::~MapDrawingArea() {}

bool MapDrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context> &context) {

  if (get_window()) {
    context->set_source_rgb(1.0, 0.0, 0.0);
    context->set_line_width(2.0);

    Gtk::Allocation allocation = get_allocation();
    double width_unit = allocation.get_width() / 1000.0;
    double height_unit = allocation.get_height() / 500.0;

    // draw the outline
    for (auto i = 0u; i < map_data.outline().size(); ++i) {
      Coord from = map_data.outline()[i];
      Coord to;
      if (i == map_data.outline().size() - 1)
        to = map_data.outline()[0];
      else
        to = map_data.outline()[i + 1];
      context->move_to(width_unit * (x_plus + (scale * from.x)),
                       height_unit * (y_plus + (scale * from.y)));
      context->line_to(width_unit * (x_plus + (scale * to.x)),
                       height_unit * (y_plus + (scale * to.y)));
    }
    context->stroke();
    context->set_source_rgb(0, 0, 0);
    // draw the locations
    for (int i = 0; i < map_data.no_locations(); ++i) {
      std::ostringstream output_string_stream;
      output_string_stream << i + 1;
      std::string label = output_string_stream.str();
      Coord c = map_data.coordinate(i + 1);
      context->move_to(width_unit * (x_plus + (scale * c.x)),
                       height_unit * (y_plus + (scale * c.y)));
      context->show_text(label.c_str());
    }
    context->stroke();
    context->set_source_rgb(0, 0, 1);
    // draw the order
    if (order.size() > 0) {
      for (auto i = 0u; i < order.size(); ++i) {
        Coord from = map_data.coordinate(order[i]);
        Coord to = map_data.coordinate(order[i + 1]);
        if (from.x != to.x || from.y != to.y) {
          context->move_to(width_unit * (x_plus + (scale * from.x)),
                           height_unit * (y_plus + (scale * from.y)));
          context->line_to(width_unit * (x_plus + (scale * to.x)),
                           height_unit * (y_plus + (scale * to.y)));
        }
      }
      context->stroke();
    }
    if (search_text.size() > 0) { // display the iteration number
      context->set_font_size(20);
      context->set_source_rgb(0, 0, 0);
      for (int i = 0; i < search_text.size(); ++i) {
        context->move_to(10, 450 + 21 * (i));
        context->show_text(search_text[i]);
      }
      context->stroke();
    }
    double title_y = 20;
    context->set_font_size(20);
    context->set_source_rgb(0, 0, 0);
    for (const auto title : search_title) {
      context->move_to(10, title_y);
      context->show_text(title);
      context->stroke();
      title_y += 30;
    }
  }
  return true;
}

void MapDrawingArea::load_file_error(std::string file) {
  Gtk::MessageDialog dialog("Don't Panic!", false, Gtk::MESSAGE_ERROR,
                            Gtk::BUTTONS_CLOSE);
  dialog.set_secondary_text("Error: Cannot load the file: " + file);
  dialog.run();
}

void MapDrawingArea::setSearchTitle(
    const std::vector<std::string> search_title) {
  this->search_title = search_title;
}

// Coord MapDrawingArea::getLocation(int location_no) {
//   return locations[location_no];
// }

// std::vector<Coord> MapDrawingArea::getOutline() { return outline; }

// int MapDrawingArea::getNoLocations() { return map_data.no_locations(); }

void MapDrawingArea::setOrder(Route order) { this->order = order; }

void MapDrawingArea::clearOrder() { order.setOrder({}); }

Route MapDrawingArea::getOrder() { return order; }

void MapDrawingArea::setSearchText(std::vector<std::string> search_text) {
  this->search_text = search_text;
}

// double MapDrawingArea::getDistance(int location_1, int location_2) {
//   return map_data.distance(location_1, location_2);
// }

// std::vector<std::vector<double>> MapDrawingArea::getDistances() {
//   return distance;
// }

bool MapDrawingArea::on_timeout() {
  // force our program to redraw the entire clock.
  Glib::RefPtr<Gdk::Window> win = get_window();
  if (win) {
    Gdk::Rectangle r(0, 0, get_allocation().get_width(),
                     get_allocation().get_height());
    win->invalidate_rect(r, false);
  }
  return true;
}
