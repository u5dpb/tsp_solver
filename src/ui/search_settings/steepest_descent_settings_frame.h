// class to record, display and store the SA Search setttings

#ifndef STEEPEST_DESCENT_SETTINGS_H
#define STEEPEST_DESCENT_SETTINGS_H

#include "map_data.h"
#include "search_settings_frame.h"
#include "solution.h"
#include "steepest_descent.h"
#include <gtkmm.h>
#include <sstream>

class SteepestDescentSettingsFrame : public SearchSettingsFrame {
public:
  SteepestDescentSettingsFrame();
  virtual ~SteepestDescentSettingsFrame();

  void updateSettingsHelp(int no_cities);
  void setUpSearchInstance(std::unique_ptr<Search> &steepest_descent);

protected:
  Gtk::Table table;

  Gtk::Label w_steepest_descent_help;
};

#endif
