// class to record, display and store the Tabu Search setttings

#ifndef TABU_SETTINGS_H
#define TABU_SETTINGS_H

#include "search_settings_frame.h"
#include "solution.h"
#include "tabu_search.h"
#include "tabu_settings.h"
#include <gtkmm.h>
#include <sstream>

class TabuSettingsFrame : public SearchSettingsFrame {
public:
  TabuSettingsFrame();
  virtual ~TabuSettingsFrame();
  int getListLength() const;
  int getNoMoves() const;
  TabuSettings getSettings() const;
  void updateSettingsHelp(int no_cities, int neighbourhood_size);
  void setUpSearchInstance(std::unique_ptr<Search> &tabu_search);

protected:
  virtual void on_no_moves_changed();
  virtual void on_list_length_changed();

  Gtk::Table table;

  Gtk::Label no_moves_label;
  Gtk::Label list_length_label;

  Gtk::Label w_tabu_help;
  Glib::RefPtr<Gtk::Adjustment> no_moves_adj, list_length_adj;
  Gtk::SpinButton no_moves_spin_button;
  Gtk::SpinButton list_length_spin_button;

  int no_moves;
  int list_length;
};

#endif
