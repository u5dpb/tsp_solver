#ifndef SEARCH_SETTINGS_FRAME_H_
#define SEARCH_SETTINGS_FRAME_H_

#include "search.h"
#include "search_settings_frame.h"
#include "solution.h"
#include <gtkmm.h>

class SearchSettingsFrame : public Gtk::Frame {
public:
  SearchSettingsFrame() = default;
  virtual ~SearchSettingsFrame(){};
  virtual void setUpSearchInstance(std::unique_ptr<Search> &search) = 0;

protected:
  static const int frame_margin = 5;
};
#endif // SEARCH_SETTINGS_FRAME_H_