// functions for the FirstFoundDescentSettingsFrame class

#include "first_found_descent_settings_frame.h"

FirstFoundDescentSettingsFrame::FirstFoundDescentSettingsFrame()
    : seed_label("Seed"),
      seed_adj(Gtk::Adjustment::create(1, 0, 1000000, 1, 100, 0)),
      seed_spin_button(seed_adj) {

  seed = 1u;
  set_label("First Found Descent Search Settings");
  table.resize(2, 6);

  seed_adj->signal_value_changed().connect(
      sigc::mem_fun(*this, &FirstFoundDescentSettingsFrame::on_seed_changed));
  seed_label.set_margin_left(frame_margin);
  seed_spin_button.set_margin_right(frame_margin);
  seed_spin_button.set_margin_top(frame_margin);
  seed_spin_button.set_margin_bottom(frame_margin);
  table.attach(seed_label, 0, 1, 0, 1);
  table.attach(seed_spin_button, 1, 2, 0, 1);

  // std::ostringstream output_string_stream;
  // output_string_stream << "If you are unsure, and delay factor is zero,\n";
  // output_string_stream << "try something like:\n";
  // output_string_stream << "Initial Temperature ?\n";
  // output_string_stream << "Final Temperature ?\n";
  // output_string_stream << "No. Cost Comparisons ?\n";
  // output_string_stream << "However, if delay factor > 0 you may want\n";
  // output_string_stream << "to decrease the no. of moves.";
  // std::string output_string = output_string_stream.str();
  // w_sa_help.set_text(output_string);

  // table.attach(w_sa_help, 0, 2, 1, 2);

  add(table);

  show_all_children();
}

FirstFoundDescentSettingsFrame::~FirstFoundDescentSettingsFrame() {}

void FirstFoundDescentSettingsFrame::on_seed_changed() {
  seed = int(seed_spin_button.get_value());
}

uint FirstFoundDescentSettingsFrame::getSeed() const { return seed; }

void FirstFoundDescentSettingsFrame::setUpSearchInstance(
    std::unique_ptr<Search> &first_found_descent) {

  static_cast<FirstFoundDescent *>(first_found_descent.get())->set_seed(seed);
}
