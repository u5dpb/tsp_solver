#include "tabu_settings_frame.h"

TabuSettingsFrame::TabuSettingsFrame()
    : no_moves_label("No. Moves"),
      no_moves_adj(Gtk::Adjustment::create(100, 0, 10000, 1, 100, 0)),
      no_moves_spin_button(no_moves_adj),
      list_length_adj(Gtk::Adjustment::create(100, 0, 10000, 1, 100, 0)),
      list_length_spin_button(list_length_adj),
      list_length_label("List Length") {
  no_moves = 100;
  list_length = 100;
  set_label("Tabu Search Settings");
  table.resize(2, 3);

  no_moves_adj->signal_value_changed().connect(
      sigc::mem_fun(*this, &TabuSettingsFrame::on_no_moves_changed));
  no_moves_spin_button.set_margin_top(frame_margin);
  no_moves_spin_button.set_margin_bottom(frame_margin);
  no_moves_spin_button.set_margin_right(frame_margin);
  no_moves_label.set_margin_left(frame_margin);
  table.attach(no_moves_label, 0, 1, 0, 1);
  table.attach(no_moves_spin_button, 1, 2, 0, 1);

  list_length_adj->signal_value_changed().connect(
      sigc::mem_fun(*this, &TabuSettingsFrame::on_list_length_changed));
  list_length_spin_button.set_margin_top(frame_margin);
  list_length_spin_button.set_margin_bottom(frame_margin);
  list_length_spin_button.set_margin_right(frame_margin);
  list_length_label.set_margin_left(frame_margin);
  table.attach(list_length_label, 0, 1, 1, 2);
  table.attach(list_length_spin_button, 1, 2, 1, 2);

  // std::ostringstream output_string_stream;
  // output_string_stream << "If you are unsure, and delay factor is zero,\n";
  // output_string_stream << "try something like:\n";
  // output_string_stream << "List Length ?\n";
  // output_string_stream << "No. Moves ?\n";
  // output_string_stream << "However, if delay factor > 0 you may want\n";
  // output_string_stream << "to decrease the no. of moves.";
  // std::string output_string = output_string_stream.str();
  // w_tabu_help.set_text(output_string);
  // table.attach(w_tabu_help, 0, 2, 2, 3);

  add(table);

  show_all_children();
}

TabuSettingsFrame::~TabuSettingsFrame() {}

TabuSettings TabuSettingsFrame::getSettings() const {
  TabuSettings tabu_settings;
  tabu_settings.list_length = list_length;
  tabu_settings.no_moves = no_moves;
  return tabu_settings;
}

void TabuSettingsFrame::setUpSearchInstance(
    std::unique_ptr<Search> &tabu_search) {
  static_cast<TabuSearch *>(tabu_search.get())
      ->set_tabu_list_length(list_length);
  static_cast<TabuSearch *>(tabu_search.get())->set_max_no_moves(no_moves);
}

void TabuSettingsFrame::updateSettingsHelp(int no_cities,
                                           int neighbourhood_size) {
  std::ostringstream output_string_stream;
  output_string_stream << "If you are unsure, and delay factor is zero,\n";
  output_string_stream << "try something like:\n";
  output_string_stream << "List Length " << neighbourhood_size / 20.0 << "\n";
  output_string_stream << "No. Moves ";
  if (no_cities * 50 > 2000)
    output_string_stream << 2000;
  else
    output_string_stream << no_cities * 50;
  output_string_stream << "\n";
  output_string_stream << "However, if delay factor > 0 you may want\n";
  output_string_stream << "to decrease the no. of moves.";
  std::string output_string = output_string_stream.str();
  w_tabu_help.set_text(output_string);
}

void TabuSettingsFrame::on_no_moves_changed() {
  no_moves = int(no_moves_spin_button.get_value());
}

void TabuSettingsFrame::on_list_length_changed() {
  list_length = int(list_length_spin_button.get_value());
}

int TabuSettingsFrame::getListLength() const { return list_length; }

int TabuSettingsFrame::getNoMoves() const { return no_moves; }
