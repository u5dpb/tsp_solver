// class to record, display and store the SA Search setttings

#ifndef FIRST_FOUND_DESCENT_SETTINGS_H
#define FIRST_FOUND_DESCENT_SETTINGS_H

#include "first_found_descent.h"
#include "map_data.h"
#include "search_settings_frame.h"
#include "solution.h"
#include <gtkmm.h>
#include <sstream>

class FirstFoundDescentSettingsFrame : public SearchSettingsFrame {
public:
  FirstFoundDescentSettingsFrame();
  virtual ~FirstFoundDescentSettingsFrame();
  uint getSeed() const;
  void updateSettingsHelp(int no_cities);
  void setUpSearchInstance(std::unique_ptr<Search> &simulated_annealing);

protected:
  virtual void on_seed_changed();

  Gtk::Table table;

  Gtk::Label seed_label;

  Gtk::Label w_sa_help;
  Glib::RefPtr<Gtk::Adjustment> seed_adj;
  Gtk::SpinButton seed_spin_button;

  uint seed;
};

#endif
