
#include "steepest_descent_settings_frame.h"

SteepestDescentSettingsFrame::SteepestDescentSettingsFrame() {

  set_label("Steepest Descent Settings");
  table.resize(2, 6);

  std::ostringstream output_string_stream;
  output_string_stream << "Steepest Descent does not ";
  output_string_stream << "use any\nparameters other ";
  output_string_stream << "than the neighbourhood\n";
  output_string_stream << "choices above.\n";
  std::string output_string = output_string_stream.str();
  w_steepest_descent_help.set_text(output_string);
  w_steepest_descent_help.set_margin_left(frame_margin);
  w_steepest_descent_help.set_margin_right(frame_margin);
  w_steepest_descent_help.set_margin_top(frame_margin);
  w_steepest_descent_help.set_margin_bottom(frame_margin);

  table.attach(w_steepest_descent_help, 0, 2, 0, 1);

  add(table);

  show_all_children();
}

SteepestDescentSettingsFrame::~SteepestDescentSettingsFrame() {}

void SteepestDescentSettingsFrame::setUpSearchInstance(
    std::unique_ptr<Search> &steepest_descent) {}
