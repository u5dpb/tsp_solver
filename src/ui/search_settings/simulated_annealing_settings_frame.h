// class to record, display and store the SA Search setttings

#ifndef SA_SETTINGS_H
#define SA_SETTINGS_H

#include "map_data.h"
#include "search_settings_frame.h"
#include "simulated_annealing.h"
#include "solution.h"
#include <gtkmm.h>
#include <sstream>

class SimulatedAnnealingSettingsFrame : public SearchSettingsFrame {
public:
  SimulatedAnnealingSettingsFrame();
  virtual ~SimulatedAnnealingSettingsFrame();
  double getInitialTemp() const;
  double getFinalTemp() const;
  double getTempMultiplier() const;
  uint getNoCostComparisons() const;
  uint getSeed() const;
  void updateSettingsHelp(int no_cities);
  void setUpSearchInstance(std::unique_ptr<Search> &simulated_annealing);

protected:
  virtual void on_initial_temp_changed();
  virtual void on_final_temp_changed();
  virtual void on_temp_multiplier_changed();
  virtual void on_no_cost_comparisions_changed();
  virtual void on_seed_changed();

  Gtk::Table table;

  Gtk::Label seed_label;
  Gtk::Label initial_temp_label;
  Gtk::Label final_temp_label;
  Gtk::Label no_cost_comparisons_label;
  Gtk::Label temp_multiplier_label;

  Gtk::Label w_sa_help;
  Glib::RefPtr<Gtk::Adjustment> seed_adj, initial_temp_adj, final_temp_adj,
      no_cost_comparisons_adj, temp_multiplier_adj;
  Gtk::SpinButton seed_spin_button;
  Gtk::SpinButton initial_temp_spin_button;
  Gtk::SpinButton final_temp_spin_button;
  Gtk::SpinButton temp_multiplier_spin_button;
  Gtk::SpinButton no_cost_comparisons_spin_button;

  double initial_temp;
  double final_temp;
  double temp_multiplier;
  uint no_cost_comparisons;
  uint seed;
};

#endif
