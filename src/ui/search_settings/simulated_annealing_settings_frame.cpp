// functions for the SimulatedAnnealingSettingsFrame class

#include "simulated_annealing_settings_frame.h"

SimulatedAnnealingSettingsFrame::SimulatedAnnealingSettingsFrame()
    : initial_temp_label("Initial Temperature"),
      initial_temp_adj(
          Gtk::Adjustment::create(5.0, 0.0, 100.0, 0.01, 1.0, 0.0)),
      initial_temp_spin_button(initial_temp_adj, 1.0, 2),

      final_temp_label("Final Temperature"),
      final_temp_adj(Gtk::Adjustment::create(0.25, 0.0, 100.0, 0.01, 1.0, 0.0)),
      final_temp_spin_button(final_temp_adj, 1.0, 2),

      temp_multiplier_label("Temp. Multiplier"),
      temp_multiplier_adj(
          Gtk::Adjustment::create(0.95, 0.0, 1.0, 0.01, 1.0, 0.0)),
      temp_multiplier_spin_button(temp_multiplier_adj, 1.0, 2),

      no_cost_comparisons_label("No. Cost Comparisons"),
      no_cost_comparisons_adj(
          Gtk::Adjustment::create(1000, 0, 1000000, 1, 100, 0)),
      no_cost_comparisons_spin_button(no_cost_comparisons_adj),
      seed_label("Seed"),
      seed_adj(Gtk::Adjustment::create(1, 0, 1000000, 1, 100, 0)),
      seed_spin_button(seed_adj) {
  initial_temp = 5;
  final_temp = 0.25;
  temp_multiplier = 0.95;
  no_cost_comparisons = 1000u;
  seed = 1u;
  set_label("Simulated Annealing Settings");
  table.resize(2, 6);

  seed_adj->signal_value_changed().connect(
      sigc::mem_fun(*this, &SimulatedAnnealingSettingsFrame::on_seed_changed));
  seed_label.set_margin_left(frame_margin);
  seed_spin_button.set_margin_right(frame_margin);
  seed_spin_button.set_margin_top(frame_margin);
  seed_spin_button.set_margin_bottom(frame_margin);
  table.attach(seed_label, 0, 1, 0, 1);
  table.attach(seed_spin_button, 1, 2, 0, 1);

  initial_temp_adj->signal_value_changed().connect(sigc::mem_fun(
      *this, &SimulatedAnnealingSettingsFrame::on_initial_temp_changed));
  initial_temp_label.set_margin_left(frame_margin);
  initial_temp_spin_button.set_margin_right(frame_margin);
  initial_temp_spin_button.set_margin_top(frame_margin);
  initial_temp_spin_button.set_margin_bottom(frame_margin);
  table.attach(initial_temp_label, 0, 1, 1, 2);
  table.attach(initial_temp_spin_button, 1, 2, 1, 2);

  final_temp_adj->signal_value_changed().connect(sigc::mem_fun(
      *this, &SimulatedAnnealingSettingsFrame::on_final_temp_changed));
  final_temp_label.set_margin_left(frame_margin);
  final_temp_spin_button.set_margin_right(frame_margin);
  final_temp_spin_button.set_margin_top(frame_margin);
  final_temp_spin_button.set_margin_bottom(frame_margin);
  table.attach(final_temp_label, 0, 1, 2, 3);
  table.attach(final_temp_spin_button, 1, 2, 2, 3);

  temp_multiplier_adj->signal_value_changed().connect(sigc::mem_fun(
      *this, &SimulatedAnnealingSettingsFrame::on_temp_multiplier_changed));
  temp_multiplier_label.set_margin_left(frame_margin);
  temp_multiplier_spin_button.set_margin_right(frame_margin);
  temp_multiplier_spin_button.set_margin_top(frame_margin);
  temp_multiplier_spin_button.set_margin_bottom(frame_margin);
  table.attach(temp_multiplier_label, 0, 1, 3, 4);
  table.attach(temp_multiplier_spin_button, 1, 2, 3, 4);

  no_cost_comparisons_adj->signal_value_changed().connect(sigc::mem_fun(
      *this,
      &SimulatedAnnealingSettingsFrame::on_no_cost_comparisions_changed));
  no_cost_comparisons_label.set_margin_left(frame_margin);
  no_cost_comparisons_spin_button.set_margin_right(frame_margin);
  no_cost_comparisons_spin_button.set_margin_top(frame_margin);
  no_cost_comparisons_spin_button.set_margin_bottom(frame_margin);
  table.attach(no_cost_comparisons_label, 0, 1, 4, 5);
  table.attach(no_cost_comparisons_spin_button, 1, 2, 4, 5);

  // std::ostringstream output_string_stream;
  // output_string_stream << "If you are unsure, and delay factor is zero,\n";
  // output_string_stream << "try something like:\n";
  // output_string_stream << "Initial Temperature ?\n";
  // output_string_stream << "Final Temperature ?\n";
  // output_string_stream << "No. Cost Comparisons ?\n";
  // output_string_stream << "However, if delay factor > 0 you may want\n";
  // output_string_stream << "to decrease the no. of moves.";
  // std::string output_string = output_string_stream.str();
  // w_sa_help.set_text(output_string);
  // table.attach(w_sa_help, 0, 2, 5, 6);

  add(table);

  show_all_children();
}

SimulatedAnnealingSettingsFrame::~SimulatedAnnealingSettingsFrame() {}

void SimulatedAnnealingSettingsFrame::updateSettingsHelp(int no_cities) {
  std::ostringstream output_string_stream;
  output_string_stream << "If you are unsure, and delay factor is zero,\n";
  output_string_stream << "try something like:\n";
  output_string_stream << "Initial Temperature 5\n";
  output_string_stream << "Final Temperature 0.25\n";
  output_string_stream << "No. Cost Comparisons " << 50 * no_cities << "\n";
  output_string_stream << "However, if delay factor > 0 you may want\n";
  output_string_stream << "to decrease the no. of moves.";
  std::string output_string = output_string_stream.str();
  w_sa_help.set_text(output_string);
}

void SimulatedAnnealingSettingsFrame::on_initial_temp_changed() {
  initial_temp = initial_temp_spin_button.get_value();
}

void SimulatedAnnealingSettingsFrame::on_final_temp_changed() {
  final_temp = final_temp_spin_button.get_value();
}

void SimulatedAnnealingSettingsFrame::on_temp_multiplier_changed() {
  temp_multiplier = temp_multiplier_spin_button.get_value();
}

void SimulatedAnnealingSettingsFrame::on_no_cost_comparisions_changed() {
  no_cost_comparisons = int(no_cost_comparisons_spin_button.get_value());
}

void SimulatedAnnealingSettingsFrame::on_seed_changed() {
  seed = int(seed_spin_button.get_value());
}

double SimulatedAnnealingSettingsFrame::getInitialTemp() const {
  return initial_temp;
}

double SimulatedAnnealingSettingsFrame::getFinalTemp() const {
  return final_temp;
}

double SimulatedAnnealingSettingsFrame::getTempMultiplier() const {
  return temp_multiplier;
}

uint SimulatedAnnealingSettingsFrame::getNoCostComparisons() const {
  return no_cost_comparisons;
}

uint SimulatedAnnealingSettingsFrame::getSeed() const { return seed; }

void SimulatedAnnealingSettingsFrame::setUpSearchInstance(
    std::unique_ptr<Search> &simulated_annealing) {

  static_cast<SimulatedAnnealing *>(simulated_annealing.get())
      ->set_initial_temp(initial_temp);
  static_cast<SimulatedAnnealing *>(simulated_annealing.get())
      ->set_final_temp(final_temp);
  static_cast<SimulatedAnnealing *>(simulated_annealing.get())
      ->set_multiplier(temp_multiplier);
  static_cast<SimulatedAnnealing *>(simulated_annealing.get())
      ->set_no_cost_comparisons(no_cost_comparisons);
  static_cast<SimulatedAnnealing *>(simulated_annealing.get())->set_seed(seed);
}
