// class to store the search text produced by TSP Solver

#ifndef TEXT_OUTPUT_H
#define TEXT_OUTPUT_H

#include <fstream>
//#include <glibmm/thread.h>
#include <gtkmm.h>
#include <mutex>
#include <vector>

#include "definitions.h"
#include "map_drawing_area.h"

class TextOutput : public Gtk::TextView {
public:
  TextOutput();
  virtual ~TextOutput();
  void printMapData(MapDrawingArea *map_data);
  void updateBuffer(std::string new_string);
  void saveBuffer(std::string filename);
  void clear();

private:
  virtual void save_file_error(std::string file);
  // Gtk::TextBuffer buffer;
  mutable std::shared_mutex buffer_mutex;
};

#endif
