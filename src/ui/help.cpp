// functions for the Help class

#include "help.h"

HelpWindow::HelpWindow()
  :
  label_layout1("<b>TSP Solver</b>"),
  label_layout2("Version 1.0"),
  label_layout3("Written by Dan Black"),
  label_layout4("Report errors to dan.black@lancaster.ac.uk"),
  w_button("Close")
{
  set_title("About TSP Solver");
  add(fixed_area);
  label_layout1.set_use_markup();
  fixed_area.put(label_layout1,10,10);
  fixed_area.put(label_layout2,10,20);
  fixed_area.put(label_layout3,10,30);
  fixed_area.put(label_layout4,10,40);
  w_button.signal_clicked().connect( sigc::mem_fun(*this,
                  &HelpWindow::on_button_clicked) );
  fixed_area.put(w_button,10,50);
  
}

HelpWindow::~HelpWindow()
{
}

void HelpWindow::on_button_clicked()
{
  hide();
}
