//
// Created on 15/12/2021.
//

#include "initial_order_settings_frame.h"

InitialOrderSettingsFrame::InitialOrderSettingsFrame() :
label_starting_city("Starting City"),
adj_starting_city(Gtk::Adjustment::create(1, 1, 1, 1, 10, 0)),
w_starting_city(adj_starting_city),
label_initial_seed("Seed"),
adj_initial_seed(Gtk::Adjustment::create(1, 1, 1000, 1, 10, 0)),
w_initial_seed(adj_initial_seed)
{
    set_label("Initial Order Settings");

    label_starting_city.set_margin_left(frame_margin);
    label_starting_city.set_margin_right(frame_margin);
    w_starting_city.set_margin_top(frame_margin);
    w_starting_city.set_margin_bottom(frame_margin);
    w_starting_city.set_margin_right(frame_margin);
    label_initial_seed.set_margin_left(frame_margin);
    label_initial_seed.set_margin_right(frame_margin);
    w_initial_seed.set_margin_top(frame_margin);
    w_initial_seed.set_margin_bottom(frame_margin);
    w_initial_seed.set_margin_right(frame_margin);


    table.resize(2, 2);
    table.attach(label_starting_city, 0, 1, 0, 1);
    table.attach(w_starting_city, 1, 2, 0, 1);
    table.attach(label_initial_seed, 0, 1, 1, 2);
    table.attach(w_initial_seed, 1, 2, 1, 2);
    add(table);

    show_all_children();
}

int InitialOrderSettingsFrame::getStartingCity() const {
    return int(w_starting_city.get_value());
}

int InitialOrderSettingsFrame::getInitialSeed() const
{
    return int(w_initial_seed.get_value());
}

void InitialOrderSettingsFrame::enable_starting_city(){
    w_starting_city.set_sensitive(true);
}
void InitialOrderSettingsFrame::disable_starting_city(){
    w_starting_city.set_sensitive(false);
}

void InitialOrderSettingsFrame::enable_initial_seed(){
    w_initial_seed.set_sensitive(true);
}
void InitialOrderSettingsFrame::disable_initial_seed(){
    w_initial_seed.set_sensitive(false);
}

#include <iostream>
void InitialOrderSettingsFrame::set_starting_city_range(int lower, int upper)
{
    w_starting_city.set_range(lower, upper);
}
