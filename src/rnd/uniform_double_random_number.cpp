#include "uniform_double_random_number.h"

UniformDoubleRandomNumber::UniformDoubleRandomNumber() {
  rnd_dist = std::uniform_real_distribution<double>(0.0, 1.0);
  this->seed = 0;
}
UniformDoubleRandomNumber::UniformDoubleRandomNumber(const unsigned int seed) {
  // rnd_dist = std::uniform_real_distribution<double>(0.0, 1.0);
  set_seed(seed);
}

// UniformDoubleRandomNumber::~UniformDoubleRandomNumber() {}

void UniformDoubleRandomNumber::set_seed(const unsigned int seed) {
  this->seed = seed;
  rnd_gen.seed((unsigned long)seed);
}

double UniformDoubleRandomNumber::generate() { return rnd_dist(rnd_gen); }

unsigned int UniformDoubleRandomNumber::generate(unsigned int value) {
  return (unsigned int)(std::floor(double(value) * rnd_dist(rnd_gen)));
}