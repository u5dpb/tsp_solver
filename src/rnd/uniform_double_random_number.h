#ifndef UNIFORM_DOUBLE_RANDOM_NUMBER_H_
#define UNIFORM_DOUBLE_RANDOM_NUMBER_H_

#include "random_number.h"
#include <random>

class UniformDoubleRandomNumber : public RandomNumber {
public:
  UniformDoubleRandomNumber();
  UniformDoubleRandomNumber(const unsigned int seed);
  //~UniformDoubleRandomNumber();
  void set_seed(const unsigned int seed);
  double generate();
  unsigned int generate(unsigned int value);

private:
  unsigned int seed;
  std::mt19937 rnd_gen;
  std::uniform_real_distribution<double> rnd_dist;
};

#endif // UNIFORM_DOUBLE_RANDOM_NUMBER_H_