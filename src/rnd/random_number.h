#ifndef RANDOM_NUMBER_H_
#define RANDOM_NUMBER_H_

#include <random>

class RandomNumber {
public:
  virtual void set_seed(const unsigned int seed) = 0;
  virtual double generate() = 0;
  virtual unsigned int generate(unsigned int value) = 0;

protected:
};

#endif // RANDOM_NUMBER_H_