#include "steepest_descent.h"

void SteepestDescent::run_search(const MapData &map_data) {
  set_summary(name() + "\n");

  bool improvement_found = true;
  this->best_solution_ = std::move(this->initial_solution_);
  this->best_solution_->evaluate(map_data);
  unsigned int iteration = 0;
  while (improvement_found && this->searching()) {
    iteration++;
    this->start_iteration();
    std::unique_ptr<BaseSolution> current_solution =
        this->best_solution_->clone();
    neighbourhood->generate_move_set(current_solution->route());
    improvement_found = false;
    while (neighbourhood->move_set_size() > 0 && this->searching()) {
      std::unique_ptr<BaseSolution> new_solution =
          neighbourhood->move_set_pop(current_solution.get());
      new_solution->evaluate(map_data);

      if (new_solution->cost() < this->best_solution_->cost()) {
        improvement_found = true;
        this->best_solution_ = std::move(new_solution);
      }
    }
    this->set_current_route(this->best_solution_->route());
    this->print_map_window_search_status(this->best_solution_->cost(),
                                         "Iteration: " +
                                             std::to_string(iteration));
    this->print_search_status(this->best_solution_->cost(), iteration, "",
                              improvement_found);
    this->end_iteration();
  }
  this->solution_summary();
}