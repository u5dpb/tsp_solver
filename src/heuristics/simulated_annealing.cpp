
#include "simulated_annealing.h"

SimulatedAnnealing::SimulatedAnnealing(
    double initial_temp, double final_temp, double multiplier,
    int no_cost_comparisons,
    const std::unique_ptr<BaseSolution> &initial_solution,
    std::unique_ptr<Neighbourhood> &&neighbourhood)
    : Search(initial_solution, std::move(neighbourhood)),
      initial_temp(initial_temp), final_temp(final_temp),
      multiplier(multiplier), no_cost_comparisons(no_cost_comparisons) {}

// template <class SolutionClass>
// void SimulatedAnnealing<SolutionClass>::set_initial_solution(
//     SolutionClass const &initial_solution) {
//   initial_solution_ = initial_solution;
// }

void SimulatedAnnealing::run_search(const MapData &map_data) {
  UniformDoubleRandomNumber rnd_dist(this->seed);
  run_search(map_data, rnd_dist);
}

void SimulatedAnnealing::run_search(const MapData &map_data,
                                    RandomNumber &rnd) {
  set_summary(name() + "\n");

  int iteration = 0;
  this->initial_solution_->evaluate(map_data);
  std::unique_ptr<BaseSolution> current_solution =
      std::move(this->initial_solution_);
  this->best_solution_ = current_solution->clone();
  double temperature = initial_temp;

  while (temperature >= final_temp && this->searching()) {
    for (auto attempt_at_temperature = 1;
         attempt_at_temperature <= no_cost_comparisons && this->searching();
         ++attempt_at_temperature) {

      iteration++;
      this->start_iteration();
      neighbourhood->generate_move_set(current_solution->route());
      if (neighbourhood->move_set_size() > 0) {
        std::unique_ptr<BaseSolution> new_solution =
            neighbourhood->move_set_pop_random(current_solution.get(), rnd);
        new_solution->evaluate(map_data);

        bool improvement = false;
        // while (!(improvement = (new_solution.cost() - current_solution.cost()
        // <
        //                         -temperature * log(rnd.generate()))) &&
        //        current_solution.move_set_size() > 0 && this->searching()) {
        //   new_solution = current_solution.move_set_pop_random(rnd);
        //   new_solution.evaluate(map_data);
        // }
        improvement = (new_solution->cost() - current_solution->cost() <
                       -temperature * log(rnd.generate()));
        if (improvement && this->searching()) {
          current_solution = std::move(new_solution);
          current_solution->evaluate(map_data);
          if (current_solution->cost() < this->best_solution_->cost()) {
            this->best_solution_ = current_solution->clone();
          }
          this->set_current_route(current_solution->route());
          this->print_map_window_search_status(
              current_solution->cost(),
              "Temp " + std::to_string(temperature) + " Attempt " +
                  std::to_string(attempt_at_temperature));
          // this->delay_search();
        }
      }
      this->end_iteration();
    }
    temperature *= multiplier;
  }
  this->solution_summary();
}