// a tabu search class for the TSP Solver program

#ifndef TABU_H
#define TABU_H

// #include <glibmm/thread.h>
// #include <gtkmm.h>
#include <math.h>
#include <sstream>

// #include "definitions.h"
// #include "map_drawing_area.h"
// #include "move.h"
// #include "route.h"
#include "search.h"
// #include "text_output.h"

#include "map_data.h"
#include "random_number.h"
#include "tabu_list.h"

class TabuSearch : public Search {

private:
  unsigned int tabu_list_length;
  unsigned int max_no_moves;
  // std::vector<std::set<Move>::iterator> tabu_list;
  // std::set<Move> tabu_set;
  // int tabu_list_position;

public:
  TabuSearch() = delete;
  TabuSearch(const unsigned int list_length, const unsigned int max_no_moves,
             const std::unique_ptr<BaseSolution> &initial_solution,
             std::unique_ptr<Neighbourhood> &&neighbourhood);
  // void set_initial_solution(SolutionClass const &initial_solution);
  void run_search(const MapData &map_data) override;
  // SolutionClass best_solution() const { return best_solution_; }
  const std::string name() const override { return "Tabu Search"; }
  const std::vector<std::string> title() const override {
    return {
        "Tabu Search :", "  List Length " + std::to_string(tabu_list_length),
        "  Max No Moves " + std::to_string(max_no_moves)};
  }

  void set_tabu_list_length(unsigned int tabu_list_length) {
    this->tabu_list_length = tabu_list_length;
  }
  void set_max_no_moves(unsigned int max_no_moves) {
    this->max_no_moves = max_no_moves;
  }
};

// template <class SolutionClass>
// void TabuSearch<SolutionClass>::set_initial_solution(
//     SolutionClass const &initial_solution) {
//   initial_solution_ = initial_solution;
// }

#endif
