// steepest descent search

#ifndef STEEPEST_DESCENT_H
#define STEEPEST_DESCENT_H

// #include <glibmm/thread.h>
// #include <gtkmm.h>
#include <math.h>
#include <memory>
#include <sstream>

// #include "definitions.h"
// #include "map_drawing_area.h"
// #include "route.h"
// #include "text_output.h"
#include "search.h"

#include "map_data.h"
#include "route.h"

class SteepestDescent : public Search {
public:
  SteepestDescent(const std::unique_ptr<BaseSolution> &initial_solution,
                  std::unique_ptr<Neighbourhood> &&neighbourhood)
      : Search(initial_solution, std::move(neighbourhood)) {}

  void run_search(const MapData &map_data);

  const std::string name() const { return "Steepest Descent"; }
  const std::vector<std::string> title() const { return {"Steepest Descent"}; }

private:
};

#endif
