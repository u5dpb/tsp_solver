#include "search_option.h"

namespace search_methods {
std::unique_ptr<Search>
searchFactory(const search_methods::SearchOption search,
              std::unique_ptr<BaseSolution> &initial_solution,
              std::unique_ptr<Neighbourhood> &&neighbourhood) {
  switch (search) {
  case search_methods::SearchOption::tabu_search:
    return std::make_unique<TabuSearch>(0, 0, initial_solution,
                                        std::move(neighbourhood));
  case search_methods::SearchOption::simulated_annealing:
    return std::make_unique<SimulatedAnnealing>(
        0.0, 0.0, 0.0, 0, initial_solution, std::move(neighbourhood));
  case search_methods::SearchOption::first_found_descent:
    return std::make_unique<FirstFoundDescent>(initial_solution,
                                               std::move(neighbourhood));
  case search_methods::SearchOption::steepest_descent:
    return std::make_unique<SteepestDescent>(initial_solution,
                                             std::move(neighbourhood));
  default:
    return nullptr;
  }
}
std::shared_ptr<SearchSettingsFrame>
searchSettingsFrameFactory(const search_methods::SearchOption search) {
  switch (search) {
  case search_methods::SearchOption::tabu_search:
    return std::make_shared<TabuSettingsFrame>();
  case search_methods::SearchOption::steepest_descent:
    return std::make_shared<SteepestDescentSettingsFrame>();
  case search_methods::SearchOption::simulated_annealing:
    return std::make_shared<SimulatedAnnealingSettingsFrame>();
  case search_methods::SearchOption::first_found_descent:
    return std::make_shared<FirstFoundDescentSettingsFrame>();
  default:
    return nullptr;
  }
}

bool validSearch(const search_methods::SearchOption &search) {
  return search != SearchOption::none;
}

} // namespace search_methods