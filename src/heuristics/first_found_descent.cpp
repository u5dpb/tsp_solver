#include "first_found_descent.h"

void FirstFoundDescent::run_search(const MapData &map_data) {
  UniformDoubleRandomNumber rnd_dist(this->seed);
  run_search(map_data, rnd_dist);
}

void FirstFoundDescent::run_search(const MapData &map_data, RandomNumber &rnd) {
  set_summary(name() + "\n");
  bool improvement_found = true;
  this->best_solution_ = this->initial_solution_->clone();
  this->best_solution_->evaluate(map_data);
  unsigned int iteration = 0;
  while (improvement_found && this->searching()) {
    iteration++;
    this->start_iteration();
    neighbourhood->generate_move_set(best_solution_->route());

    improvement_found = false;
    while (!improvement_found && neighbourhood->move_set_size() > 0 &&
           this->searching()) {
      std::unique_ptr<BaseSolution> new_solution =
          neighbourhood->move_set_pop_random(this->best_solution_.get(), rnd);

      new_solution->evaluate(map_data);

      if (new_solution->cost() < this->best_solution_->cost()) {
        improvement_found = true;
        this->best_solution_ = std::move(new_solution);
      }
    }
    this->set_current_route(this->best_solution_->route());
    this->print_map_window_search_status(this->best_solution_->cost(),
                                         "Iteration: " +
                                             std::to_string(iteration));
    this->print_search_status(this->best_solution_->cost(), iteration, "",
                              improvement_found);
    this->end_iteration();
  }
  this->solution_summary();

  // close search
}
