// a tabu search class for the TSP Solver program

#ifndef SEARCH_H
#define SEARCH_H

// #include <math.h>
// #include <queue>
#include <chrono>
#include <memory>
#include <sstream>
#include <thread>

// #include "move.h"
#include "search_signals_base.h"
#include "search_signals_empty.h"
#include <base_solution.h>
#include <map_data.h>
#include <neighbourhood.h>
#include <route.h>

class Search {

public:
  Search() = delete;
  Search(const std::unique_ptr<BaseSolution> &initial_solution,
         std::unique_ptr<Neighbourhood> &&neighbourhood_in)
      : initial_solution_(initial_solution->clone()),
        best_solution_(initial_solution->clone()),
        neighbourhood(std::move(neighbourhood_in)) {
    delay = 0;
    seed = 0;
    text_period = 0;
    searching_ = true;
    search_signal_functions = &search_signals_empty;
  }
  virtual ~Search() = default;
  void set_signal_functions(SearchSignalsBase *search_signal_functions) {
    this->search_signal_functions = search_signal_functions;
  }

  void end_search() {
    searching_ = false;
    set_current_route(best_solution_->route());
  }
  void set_seed(unsigned int seed) { this->seed = seed; }
  void
  set_initial_solution(const std::unique_ptr<BaseSolution> &initial_solution);
  virtual void run_search(const MapData &map_data) = 0;
  std::unique_ptr<BaseSolution> best_solution() const {
    return best_solution_->clone();
  }
  virtual const std::string name() const = 0;
  virtual const std::vector<std::string> title() const = 0;

  void setDelay(double delay) { this->delay = delay; }
  void setTextPeriod(unsigned int text_period) {
    this->text_period = text_period;
  }

  Route current_route() const;
  std::vector<std::string> search_status() const;
  std::vector<std::string> summary();

protected:
  std::unique_ptr<BaseSolution> initial_solution_;
  std::unique_ptr<BaseSolution> best_solution_;
  std::unique_ptr<Neighbourhood> neighbourhood;

  void solution_summary();
  void print_map_window_search_status(const double &current_cost,
                                      std::string algorithm_details = "");
  void print_search_status(const double &current_cost,
                           const unsigned int &iteration,
                           const std::string &search_information_string,
                           bool force = false);
  void print_search_intro(const std::string &search_information_string);

  double delay;
  unsigned int seed;
  unsigned int text_period;
  bool searching() const { return searching_; }

  void start_iteration();
  void end_iteration();

  void set_current_route(const Route &new_route);
  void add_search_status(const std::string &text);
  void add_search_status(const std::vector<std::string> &text_vector);
  void set_summary(const std::string &text);
  void set_summary(const std::vector<std::string> &text_vector);

private:
  SearchSignalsBase *search_signal_functions;

  std::thread iteration_thread;

  void delay_search();
  bool searching_;

  Route current_route_;
  mutable std::shared_mutex current_route_mutex;

  std::vector<std::string> search_status_;
  mutable std::shared_mutex search_status_mutex;
  std::vector<std::string> summary_;
  mutable std::shared_mutex summary_mutex;

  SearchSignalsEmpty search_signals_empty;
};

#endif
