#ifndef SEARCH_SIGNALS_EMPTY_H_
#define SEARCH_SIGNALS_EMPTY_H_

#include "search_signals_base.h"

class SearchSignalsEmpty : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() override {}
  void signal_function_initial_route_changed() override {}
  void signal_function_search_status_update() override {}
  void signal_function_summary_update() override {}
};

#endif // SEARCH_SIGNALS_EMPTY_H_