#include "tabu_search.h"

TabuSearch::TabuSearch(const unsigned int list_length,
                       const unsigned int max_no_moves,
                       const std::unique_ptr<BaseSolution> &initial_solution,
                       std::unique_ptr<Neighbourhood> &&neighbourhood)
    : Search(initial_solution, std::move(neighbourhood)),
      // initial_solution_(initial_solution), best_solution_(initial_solution),
      tabu_list_length(list_length), max_no_moves(max_no_moves) {
  // this->list_length = list_length;
  // this->max_no_moves = max_no_moves;
}

void TabuSearch::run_search(const MapData &map_data) {
  set_summary(name() + "\n");

  this->print_search_intro("");
  TabuList tabu_list(tabu_list_length);
  unsigned int iteration = 0;
  this->best_solution_ = this->initial_solution_->clone();
  this->best_solution_->evaluate(map_data);
  std::unique_ptr<BaseSolution> current_solution =
      std::move(this->initial_solution_);
  while (iteration < max_no_moves && this->searching()) {
    iteration++;
    this->start_iteration();
    neighbourhood->generate_move_set(current_solution->route(), tabu_list);
    if (neighbourhood->move_set_size() > 0) {
      bool improvement_on_best = false;
      std::unique_ptr<BaseSolution> next_current_solution =
          neighbourhood->move_set_pop(current_solution.get());
      next_current_solution->evaluate(map_data);
      std::unique_ptr<BaseMove> p_best_move = neighbourhood->last_move_popped();
      if (next_current_solution->cost() < this->best_solution_->cost()) {
        this->best_solution_ = next_current_solution->clone();
      }
      while (neighbourhood->move_set_size() > 0 && this->searching()) {
        std::unique_ptr<BaseSolution> new_solution =
            neighbourhood->move_set_pop(current_solution.get());
        new_solution->evaluate(map_data);
        if (new_solution->cost() < this->best_solution_->cost()) {
          this->best_solution_ = new_solution->clone();
          improvement_on_best = true;
        }
        if (new_solution->cost() < next_current_solution->cost()) {
          next_current_solution = std::move(new_solution);
          p_best_move = neighbourhood->last_move_popped();
        }
      }
      current_solution = std::move(next_current_solution);
      this->set_current_route(current_solution->route());
      this->print_map_window_search_status(
          current_solution->cost(), "Iteration: " + std::to_string(iteration));
      this->print_search_status(current_solution->cost(), iteration, "",
                                improvement_on_best);
      tabu_list.add(std::move(p_best_move));
    }
    this->end_iteration();
  }
  this->solution_summary();
  // close search
}