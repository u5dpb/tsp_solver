#ifndef SEARCH_SIGNALS_BASE_H_
#define SEARCH_SIGNALS_BASE_H_

class SearchSignalsBase {
public:
  virtual void signal_function_current_route_changed() = 0;
  virtual void signal_function_initial_route_changed() = 0;
  virtual void signal_function_search_status_update() = 0;
  virtual void signal_function_summary_update() = 0;
};

#endif // SEARCH_SIGNALS_BASE_H_