#ifndef SearchOption_H_
#define SearchOption_H_

#include "first_found_descent.h"
#include "search.h"
#include "simulated_annealing.h"
#include "steepest_descent.h"
#include "tabu_search.h"

#include <first_found_descent_settings_frame.h>
#include <search_settings_frame.h>
#include <simulated_annealing_settings_frame.h>
#include <steepest_descent_settings_frame.h>
#include <tabu_settings_frame.h>

#include <array>
#include <map>
#include <neighbourhood.h>
#include <solution.h>

namespace search_methods {

enum class SearchOption {
  none = 0,
  first_found_descent,
  steepest_descent,
  simulated_annealing,
  tabu_search,
  end
};

static std::array<SearchOption, 5> SearchOptionAll = {
    SearchOption::none, SearchOption::first_found_descent,
    SearchOption::steepest_descent, SearchOption::simulated_annealing,
    SearchOption::tabu_search};

bool validSearch(const search_methods::SearchOption &search);

static std::map<SearchOption, std::string> search_option_str = {
    {SearchOption::none, "No Improvement"},
    {SearchOption::first_found_descent, "First-Found Descent"},
    {SearchOption::steepest_descent, "Steepest Descent"},
    {SearchOption::simulated_annealing, "Simulated Annealing"},
    {SearchOption::tabu_search, "Tabu Search"}};

static std::map<std::string, SearchOption> search_option_from_str = {
    {"No Improvement", SearchOption::none},
    {"First-Found Descent", SearchOption::first_found_descent},
    {"Steepest Descent", SearchOption::steepest_descent},
    {"Simulated Annealing", SearchOption::simulated_annealing},
    {"Tabu Search", SearchOption::tabu_search}};

std::unique_ptr<Search>
searchFactory(const search_methods::SearchOption search,
              std::unique_ptr<BaseSolution> &initial_solution,
              std::unique_ptr<Neighbourhood> &&neighbourhood);
std::shared_ptr<SearchSettingsFrame>
searchSettingsFrameFactory(const search_methods::SearchOption search);

} // namespace search_methods
#endif // SearchOption_H_