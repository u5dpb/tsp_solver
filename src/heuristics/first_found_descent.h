// first found descent search

#ifndef FIRST_FOUND_H
#define FIRST_FOUND_H

// #include <glibmm/thread.h>
// #include <gtkmm.h>
#include <math.h>
#include <sstream>

//#include "problem_structures/base_solution.h"
#include "map_data.h"
#include "random_number.h"
#include "route.h"
#include "uniform_double_random_number.h"

// #include "definitions.h"
// #include "map_drawing_area.h"
// #include "route.h"
// #include "text_output.h"
#include "search.h"

class FirstFoundDescent : public Search {
public:
  FirstFoundDescent(const std::unique_ptr<BaseSolution> &initial_solution,
                    std::unique_ptr<Neighbourhood> &&neighbourhood)
      : Search(initial_solution, std::move(neighbourhood)) {}

  void run_search(const MapData &map_data, RandomNumber &rnd);
  void run_search(const MapData &map_data);

  const std::string name() const { return "First Found Descent"; }
  const std::vector<std::string> title() const {
    return {"First Found Descent :", "   seed " + std::to_string(this->seed)};
  }

private:
};

#endif
