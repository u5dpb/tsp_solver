// a tabu search class for the TSP Solver program

#ifndef SIMULATED_ANNEALING_H
#define SIMULATED_ANNEALING_H

// #include <glibmm/thread.h>
// #include <gtkmm.h>
#include <iostream>
#include <math.h>
#include <random>
#include <sstream>

#include "definitions.h"
// #include "map_drawing_area.h"
// #include "route.h"
#include "search.h"
// #include "text_output.h"
#include "map_data.h"
#include "random_number.h"
#include "uniform_double_random_number.h"

class SimulatedAnnealing : public Search {

private:
  double initial_temp;
  double final_temp;
  double multiplier;
  int no_cost_comparisons;

  // SolutionClass initial_solution_;
  // SolutionClass best_solution_;

public:
  SimulatedAnnealing(double initial_temp, double final_temp, double multiplier,
                     int no_cost_comparisons,
                     const std::unique_ptr<BaseSolution> &initial_solution,
                     std::unique_ptr<Neighbourhood> &&neighbourhood);

  // void set_initial_solution(SolutionClass const &initial_solution);
  void run_search(const MapData &map_data, RandomNumber &rnd);
  void run_search(const MapData &map_data);

  // SolutionClass best_solution() const { return best_solution_; }
  const std::string name() const { return "Simulated Annealing"; }
  const std::vector<std::string> title() const {
    return {"Simulated Annealing:",
            "  seed " + std::to_string(this->seed),
            "  initial temp " + std::to_string(initial_temp),
            "  final temp " + std::to_string(final_temp),
            "  multiplier " + std::to_string(multiplier),
            "  no cost comparisons " + std::to_string(no_cost_comparisons)};
  }

  void set_initial_temp(const double initial_temp) {
    this->initial_temp = initial_temp;
  }
  void set_final_temp(const double final_temp) {
    this->final_temp = final_temp;
  }
  void set_multiplier(const double multiplier) {
    this->multiplier = multiplier;
  }
  void set_no_cost_comparisons(const int no_cost_comparisons) {
    this->no_cost_comparisons = no_cost_comparisons;
  }
};

#endif
