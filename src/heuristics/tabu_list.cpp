#include "tabu_list.h"

TabuList::TabuList(const unsigned int max_size) { this->max_size_ = max_size; }

TabuList::~TabuList() { tabu_list_.clear(); }

void TabuList::add(std::unique_ptr<BaseMove> p_move) {

  tabu_list_.push_back(std::move(p_move));
  if (tabu_list_.size() > max_size_) {
    tabu_list_.pop_front();
  }
}

bool TabuList::contains(const BaseMove *p_move) const {

  for (auto it = tabu_list_.begin(); it != tabu_list_.end(); ++it) {
    if (*(*it) == *p_move)
      return true;
  }
  return false;
}
