#ifndef TABU_LIST_H_
#define TABU_LIST_H_

#include "move.h"
#include <algorithm>
#include <deque>

class BaseMove;

class TabuList {
public:
  TabuList(const unsigned int max_size);
  ~TabuList();
  void add(std::unique_ptr<BaseMove> p_move);
  bool contains(const BaseMove *p_move) const;

private:
  unsigned int max_size_;
  std::deque<std::unique_ptr<BaseMove>> tabu_list_;
};

#endif // TABU_LIST_H_