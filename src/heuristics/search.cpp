
#include "search.h"

Route Search::current_route() const {
  std::shared_lock lock(current_route_mutex);
  return current_route_;
}

void Search::set_current_route(const Route &new_route) {
  std::unique_lock lock(current_route_mutex);
  current_route_ = new_route;
  lock.unlock();
  search_signal_functions->signal_function_current_route_changed();
}

std::vector<std::string> Search::search_status() const {
  std::shared_lock lock(search_status_mutex);
  return search_status_;
}

void Search::set_summary(const std::string &text) {
  std::unique_lock lock(summary_mutex);
  summary_.push_back(text);
  lock.unlock();
  search_signal_functions->signal_function_summary_update();
}

void Search::set_summary(const std::vector<std::string> &text_vector) {
  std::unique_lock lock(summary_mutex);
  for (auto text : text_vector)
    summary_.push_back(text);
  lock.unlock();
  search_signal_functions->signal_function_summary_update();
}
std::vector<std::string> Search::summary() {
  std::unique_lock lock(summary_mutex);
  std::vector<std::string> return_vector = summary_;
  summary_.resize(0);
  return return_vector;
}

void Search::add_search_status(const std::string &text) {
  std::unique_lock lock(search_status_mutex);
  search_status_ = {text};
  lock.unlock();
  search_signal_functions->signal_function_search_status_update();
}

void Search::add_search_status(const std::vector<std::string> &text_vector) {
  std::unique_lock lock(search_status_mutex);
  // for (const auto text : text_vector) {
  //   search_status_.push_back(text);
  // }
  search_status_ = text_vector;
  lock.unlock();
  search_signal_functions->signal_function_search_status_update();
}

void Search::set_initial_solution(
    const std::unique_ptr<BaseSolution> &initial_solution) {
  initial_solution_ = initial_solution->clone();
}

void Search::solution_summary() {
  std::ostringstream output_string_stream;
  output_string_stream << "Search Finished\n";
  output_string_stream << "Initial order: ";
  output_string_stream << this->best_solution_->route();
  output_string_stream << "\n";
  // std::shared_lock lock(route_mutex);
  output_string_stream << "Solution cost = " << this->best_solution_->cost()
                       << "\n";
  // lock.unlock();
  set_summary(output_string_stream.str());
  print_map_window_search_status(best_solution_->cost(), "Search finished");
}

void Search::print_map_window_search_status(const double &current_cost,
                                            std::string algorithm_details) {
  std::vector<std::string> return_strings;
  std::ostringstream output_string_stream;
  output_string_stream << "Current cost: ";
  output_string_stream << current_cost;
  return_strings.push_back(output_string_stream.str());
  output_string_stream.str("");
  output_string_stream << "Best cost: ";
  output_string_stream << best_solution_->cost();
  return_strings.push_back(output_string_stream.str());
  output_string_stream.str(algorithm_details);
  return_strings.push_back(output_string_stream.str());
  add_search_status(return_strings);
}

void Search::print_search_status(const double &current_cost,
                                 const unsigned int &iteration,
                                 const std::string &search_information_string,
                                 bool force) {
  if (force || (text_period != 0 && iteration % text_period == 0)) {
    std::ostringstream output_string_stream;
    output_string_stream << "Iteration ";
    output_string_stream << iteration;
    output_string_stream << " Best cost: ";
    output_string_stream << best_solution_->cost();
    output_string_stream << " Current cost: ";
    output_string_stream << current_cost;
    output_string_stream << " " << search_information_string;
    output_string_stream << "\n";
    set_summary(output_string_stream.str());
  }
}

void Search::print_search_intro(const std::string &search_information_string) {
  std::ostringstream output_string_stream;
  output_string_stream << "Search: " << name() << "\n";
  output_string_stream << "Neighbourhood: " << neighbourhood->neighbourhoodStr()
                       << "\n";
  output_string_stream << search_information_string << "\n";
  set_summary(output_string_stream.str());
}

void Search::delay_search() {
  std::this_thread::sleep_for(std::chrono::milliseconds(int(delay * 1000.0)));
}

void Search::start_iteration() {
  if (delay > 0) {
    iteration_thread = std::thread(&Search::delay_search, this);
  }
}
void Search::end_iteration() {
  if (delay > 0) {
    iteration_thread.join();
  }
}