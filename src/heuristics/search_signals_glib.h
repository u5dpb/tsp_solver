#ifndef SEARCH_SIGNALS_GLIB_H_
#define SEARCH_SIGNALS_GLIB_H_

#include "search_signals_base.h"
#include <gtkmm.h>

class SearchSignalsGlib : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() override {
    signal_current_route_changed();
  };
  void signal_function_initial_route_changed() override {
    signal_initial_route_changed();
  };
  void signal_function_search_status_update() override {
    signal_search_status_update();
  };
  void signal_function_summary_update() override { signal_summary_update(); };
  Glib::Dispatcher signal_current_route_changed;
  Glib::Dispatcher signal_initial_route_changed;
  Glib::Dispatcher signal_search_status_update;
  Glib::Dispatcher signal_summary_update;
};

#endif // SEARCH_SIGNALS_GLIB_H_