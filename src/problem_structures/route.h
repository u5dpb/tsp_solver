// class to perform route operations within the TSP Solver program

#ifndef ROUTE_H
#define ROUTE_H

//#include "definitions.h"
#include "map_data.h"
#include <cassert>
#include <iostream>
#include <list>
#include <math.h>
#include <random>
#include <set>
#include <vector>

class Route {

  friend std::ostream &operator<<(std::ostream &outstream, const Route &route);

private:
  std::vector<unsigned int> order_;

public:
  Route();
  Route(std::vector<unsigned int> order);
  unsigned int &operator[](int index);
  const unsigned int &operator[](int index) const;
  std::size_t size() const;
  void insert_after(const unsigned int location, const unsigned int value);
  std::vector<unsigned int> order() const;
  std::vector<unsigned int>::const_iterator begin() const {
    return order_.begin();
  }
  std::vector<unsigned int>::const_iterator end() const { return order_.end(); }
  void setOrder(const std::vector<unsigned int> order);

  static double evaluate(Route route, const MapData &map_data);
};

#endif
