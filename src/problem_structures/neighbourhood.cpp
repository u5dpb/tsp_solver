#include "neighbourhood.h"

void Neighbourhood::generate_move_set(const Route &route) {
  TabuList empty_tabu_list(1);
  generate_move_set(route, empty_tabu_list);
}

void Neighbourhood::generate_move_set(const Route &route, TabuList &tabu_list) {
  route_ = route;
  move_list.clear();
  if (use_swap2adj)
    generate_move_set_swap2adj(route, tabu_list);
  if (use_swap2any)
    generate_move_set_swap2any(route, tabu_list);
  if (use_reinsert)
    generate_move_set_reinsert(route, tabu_list);
  if (use_reverse)
    generate_move_set_reverse(route, tabu_list);
}

void Neighbourhood::generate_move_set_swap2adj(const Route &route,
                                               const TabuList &tabu_list) {
  for (auto i = 0u; i < route.size(); ++i) {
    MoveSwap2Adj new_move(route, i, i + 1);
    if (!tabu_list.contains(&new_move)) {
      move_list.addMove(std::make_unique<MoveSwap2Adj>(new_move));
    }
  }
}
void Neighbourhood::generate_move_set_swap2any(const Route &route,
                                               const TabuList &tabu_list) {
  for (auto i = 0u; i < route.size() - 1; ++i) {
    for (auto j = i + 1u; j < route.size(); ++j) {
      MoveSwap2Any new_move(route, i, j);
      if (!tabu_list.contains(&new_move)) {
        move_list.addMove(std::make_unique<MoveSwap2Any>(new_move));
      }
    }
  }
}

void Neighbourhood::generate_move_set_reinsert(const Route &route,
                                               const TabuList &tabu_list) {
  if (reinsert_city_pairs.size() == 0) {
    for (auto i = 0u; i < route.size(); ++i) {
      for (auto j = 0u; j < route.size(); ++j) {
        auto k =
            (j + 1u) % route.size(); //(j - 1 + route.size()) % route.size();
        if (j != i && k != i) {
          reinsert_city_pairs.push_back({i, j});
        }
      }
    }
  }
  for (const auto &city_pair : reinsert_city_pairs) {
    MoveReinsert new_move(route, city_pair.first, city_pair.second);
    if (!tabu_list.contains(&new_move)) {
      move_list.addMove(std::make_unique<MoveReinsert>(new_move));
    }
  }
  // for (auto i = 0u; i < route.size(); ++i) {
  //   for (auto j = 0u; j < route.size(); ++j) {
  //     auto k = (j + 1u) % route.size(); //(j - 1 + route.size()) %
  //     route.size();
  //     if (j != i && k != i) {
  //       MoveReinsert new_move(route, i, j);
  //       if (!tabu_list.contains(&new_move)) {
  //         move_list.addMove(std::make_unique<MoveReinsert>(new_move));
  //       }
  //     }
  //   }
  // }
}

void Neighbourhood::generate_move_set_reverse(const Route &route,
                                              const TabuList &tabu_list) {
  if (reverse_city_pairs.size() == 0) {
    for (auto i = 0u; i < route.size(); ++i) {
      for (auto j = i + 1u; j < route.size() && j - i < route.size() - 2u;
           ++j) {
        reverse_city_pairs.push_back({i, j});
      }
    }
  }
  for (const auto &city_pair : reverse_city_pairs) {
    std::unique_ptr<MoveReverse> new_move =
        std::make_unique<MoveReverse>(route, city_pair.first, city_pair.second);
    if (!tabu_list.contains(new_move.get())) {
      move_list.addMove(std::move(new_move));
    }
  }

  // for (auto i = 0u; i < route.size(); ++i) {
  //   for (auto j = i + 1u; j < route.size() && j - i < route.size() - 2u; ++j)
  //   {
  //     std::unique_ptr<MoveReverse> new_move =
  //         std::make_unique<MoveReverse>(route, i, j);
  //     if (!tabu_list.contains(new_move.get())) {
  //       move_list.addMove(std::move(new_move));
  //     }
  //   }
  // }
}

std::unique_ptr<BaseSolution>
Neighbourhood::move_set_pop(BaseSolution *solution) {
  last_move_popped_ = move_list.pop_front();
  std::unique_ptr<BaseSolution> return_solution = solution->clone();
  return_solution->set_route(last_move_popped_->chooseMove(route_));
  return return_solution;
}

std::unique_ptr<BaseSolution>
Neighbourhood::move_set_pop_random(BaseSolution *solution, RandomNumber &rnd) {
  last_move_popped_ =
      move_list.pop_item(rnd.generate((unsigned int)move_list.size()));
  std::unique_ptr<BaseSolution> return_solution = solution->clone();
  return_solution->set_route(last_move_popped_->chooseMove(route_));
  return return_solution;
}

std::unique_ptr<BaseMove> Neighbourhood::last_move_popped() {
  return std::move(last_move_popped_);
}

std::string Neighbourhood::neighbourhoodStr() const {
  std::string return_string = "";
  if (use_swap2any)
    return_string += "Swap2Any ";
  if (use_swap2adj)
    return_string += "Swap2Adj ";
  if (use_reinsert)
    return_string += "Reinsert ";
  if (use_reverse)
    return_string += "Reverse ";
  return return_string;
}