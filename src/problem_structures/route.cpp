// class functions for the Route class

#include "route.h"

Route::Route() { order_.resize(0); }

Route::Route(std::vector<unsigned int> order) { this->order_ = order; }

std::vector<unsigned int> Route::order() const { return order_; }

void Route::setOrder(const std::vector<unsigned int> order) {
  this->order_ = order;
}

double Route::evaluate(Route route, const MapData &map_data) {
  if (route.size() < 2)
    return 0.0;
  double route_length = 0.0;
  // for (auto i = 0; i < (int)route.size(); ++i) {
  //   route_length += map_data.distance(route[i], route[i + 1]);
  // }
  auto it = route.begin() + 1;
  for (; it != route.end(); ++it) {
    // std::cout << *(it - 1) << " to " << *it << "\n";
    route_length += map_data.distance(*(it - 1), *it);
  }
  route_length += map_data.distance(route[(int)route.size() - 1], route[0]);
  return route_length;
}

void Route::insert_after(const unsigned int location,
                         const unsigned int value) {
  if (order_.size() == 0 && location == 0) {
    order_ = {value};
    return;
  }
  auto it = order_.begin();
  std::advance(it, location + 1);
  assert(location < order_.size());
  order_.insert(it, value);
}

unsigned int &Route::operator[](int index) {
  if (index < 0)
    index += int(order_.size());
  if (index >= int(order_.size()))
    index -= int(order_.size());
  assert(index >= 0);
  assert(index < (int)order_.size());
  return order_[size_t(index)];
}

const unsigned int &Route::operator[](int index) const {
  if (index < 0)
    index += int(order_.size());
  if (index >= int(order_.size()))
    index -= int(order_.size());
  assert(index >= 0);
  assert(index < (int)order_.size());
  return order_[size_t(index)];
}

std::ostream &operator<<(std::ostream &out, const Route &route) {
  for (auto location : route.order_)
    out << location << " ";
  return out;
}

std::size_t Route::size() const { return order_.size(); }
