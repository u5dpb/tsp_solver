#ifndef SOLUTION_H_
#define SOLUTION_H_

#include "base_solution.h"
#include "move_list.h"
#include "move_reinsert.h"
#include "move_reverse.h"
#include "move_swap_2_adj.h"
#include "move_swap_2_any.h"
#include "random_number.h"
#include "route.h"
#include "tabu_list.h"
#include <limits.h>
#include <set>

class Solution : public BaseSolution {
public:
  Solution() = delete;
  Solution(const Solution &solution);

  Solution(const Route &route);
  ~Solution();

  void evaluate(const MapData &map_data) override;

  Solution &operator=(const Solution &rhs);

  std::unique_ptr<BaseSolution> clone() const override;

protected:
};

#endif // SOLUTION_H_