#ifndef BASE_SOLUTION_H_
#define BASE_SOLUTION_H_

#include "map_data.h"
#include "move.h"
#include "move_list.h"
#include "random_number.h"
#include "route.h"
#include "tabu_list.h"

#include <memory>
#include <string>

class BaseSolution {
public:
  virtual ~BaseSolution() = default;
  virtual void evaluate(const MapData &map_data) = 0;
  virtual double cost() const { return cost_; }
  virtual void set_route(const Route &route) { route_ = route; }
  virtual Route route() const { return route_; }

  virtual std::unique_ptr<BaseSolution> clone() const = 0;

protected:
  Route route_;
  double cost_;
};

#endif // BASE_SOLUTION_H_