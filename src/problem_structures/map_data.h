#ifndef MAP_DATA_H_
#define MAP_DATA_H_

//#include "coordinates.h"
#include <cassert>
#include <definitions.h>
#include <fstream>
#include <functional>
#include <math.h>
#include <mutex>
#include <shared_mutex>
#include <vector>

class MapData {
public:
  MapData();
  MapData(const std::vector<Coord> &coords);

  const Coord &coordinate(const unsigned int &i) const;

  const std::vector<Coord> &outline() const { return outline_; }

  const double &distance(const unsigned int &location_1,
                         const unsigned int &location_2) const;

  unsigned int no_locations() const;
  void load_map_data(std::istream &in, const std::string &filename);
  const std::string &filename() const { return filename_; }

  // sigc::signal<void()> signal_map_data_changed;

  void set_call_signal_map_data_changed(
      std::function<void()> call_signal_map_data_changed) {
    this->call_signal_map_data_changed = call_signal_map_data_changed;
  }

private:
  std::function<void()> call_signal_map_data_changed = []() {};
  mutable std::shared_mutex map_data_lock;
  void set_coordinates(const std::vector<Coord> &coords);
  std::vector<Coord> coordinates;
  std::vector<Coord> outline_;

  std::vector<std::vector<double>> distances;
  void generate_distance_matrix();
  std::string filename_;
};

#endif // MAP_DATA_H_