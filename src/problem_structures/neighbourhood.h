#ifndef NEIGHBOURHOOD_H_
#define NEIGHBOURHOOD_H_

#include "solution.h"
#include "tabu_list.h"
#include <move.h>
#include <move_list.h>
#include <move_reinsert.h>
#include <move_reverse.h>
#include <move_swap_2_adj.h>
#include <move_swap_2_any.h>

class Neighbourhood {
  struct city_pair {
    unsigned int first;
    unsigned int second;
  };

public:
  Neighbourhood() = delete;
  Neighbourhood(bool use_swap2adj, bool use_swap2any, bool use_reinsert,
                bool use_reverse)
      : use_swap2adj(use_swap2adj), use_swap2any(use_swap2any),
        use_reinsert(use_reinsert), use_reverse(use_reverse) {}
  Neighbourhood(const Neighbourhood &neighbourhood)
      : use_swap2adj(neighbourhood.use_swap2adj),
        use_swap2any(neighbourhood.use_swap2any),
        use_reinsert(neighbourhood.use_reinsert),
        use_reverse(neighbourhood.use_reverse) {}
  virtual ~Neighbourhood() = default;

  virtual void generate_move_set(const Route &route);
  virtual void generate_move_set(const Route &route, TabuList &tabu_list);

  std::unique_ptr<BaseSolution> move_set_pop(BaseSolution *solution);

  std::unique_ptr<BaseSolution> move_set_pop_random(BaseSolution *solution,
                                                    RandomNumber &rnd);

  std::size_t move_set_size() const { return move_list.size(); }

  std::unique_ptr<BaseMove> last_move_popped();

  std::string neighbourhoodStr() const;

protected:
  Route route_;

  MoveList move_list;
  bool use_swap2adj;
  bool use_swap2any;
  bool use_reinsert;
  bool use_reverse;

  std::unique_ptr<BaseMove> last_move_popped_;

private:
  void generate_move_set_swap2adj(const Route &route,
                                  const TabuList &tabu_list);
  void generate_move_set_swap2any(const Route &route,
                                  const TabuList &tabu_list);
  void generate_move_set_reverse(const Route &route, const TabuList &tabu_list);
  void generate_move_set_reinsert(const Route &route,
                                  const TabuList &tabu_list);
  std::vector<city_pair> reinsert_city_pairs;
  std::vector<city_pair> reverse_city_pairs;
};

#endif // NEIGHBOURHOOD_H_