#include "map_data.h"

MapData::MapData() {}

MapData::MapData(const std::vector<Coord> &coords) { set_coordinates(coords); }

void MapData::set_coordinates(const std::vector<Coord> &coords) {
  std::unique_lock lock(map_data_lock);
  this->coordinates = coords;
  lock.unlock();
  generate_distance_matrix();
  call_signal_map_data_changed();
}

unsigned int MapData::no_locations() const {
  std::shared_lock lock(map_data_lock);
  return (unsigned int)coordinates.size();
}
const Coord &MapData::coordinate(const unsigned int &i) const {
  std::shared_lock lock(map_data_lock);
  if (i > coordinates.size()) {
    throw "const Coord &MapData::coordinate(int i)\n i out of range\n";
  }
  return coordinates[i - 1];
}

const double &MapData::distance(const unsigned int &location_1,
                                const unsigned int &location_2) const {
  std::shared_lock lock(map_data_lock);
  assert(location_1 <= coordinates.size());
  assert(location_2 <= coordinates.size());
  return distances[location_1 - 1][location_2 - 1];
}

void MapData::generate_distance_matrix() {
  std::unique_lock lock(map_data_lock);
  distances = std::vector<std::vector<double>>(
      coordinates.size(), std::vector<double>(coordinates.size(), 0.0));
  for (unsigned int location_1 = 0; location_1 < coordinates.size();
       ++location_1) {
    for (unsigned int location_2 = 0; location_2 < coordinates.size();
         ++location_2) {
      distances[location_1][location_2] =
          sqrt(pow(coordinates[location_1].x - coordinates[location_2].x, 2) +
               pow(coordinates[location_1].y - coordinates[location_2].y, 2));
    }
  }
}

void MapData::load_map_data(std::istream &in, const std::string &filename) {
  std::unique_lock lock(map_data_lock);
  filename_ = filename;
  outline_.clear();
  unsigned int no_points;
  Coord c;
  in >> no_points;
  for (auto i = 0u; i < no_points; ++i) {
    in >> c.x >> c.y;
    outline_.push_back(c);
  }
  unsigned int no_cities;
  in >> no_cities;
  std::vector<Coord> coords;
  // Coord c;
  for (unsigned int i = 0; i < no_cities; ++i) {
    in >> c.x >> c.y;
    coords.push_back(c);
  }
  lock.unlock();
  set_coordinates(coords);
  // call_signal_map_data_changed();
}
