#include "solution.h"

// Solution::Solution() { cost_ = std::numeric_limits<double>::max(); }

Solution::Solution(const Solution &solution) { *this = solution; }

Solution::Solution(const Route &route) {
  cost_ = std::numeric_limits<double>::max();
  this->route_ = route;
}

Solution::~Solution() { // move_list.clear();
}

void Solution::evaluate(const MapData &map_data) {
  cost_ = Route::evaluate(route_, map_data);
}

Solution &Solution::operator=(const Solution &rhs) {
  if (this != &rhs) {
    this->route_ = rhs.route_;
    this->cost_ = rhs.cost_;
  }
  return *this;
}

std::unique_ptr<BaseSolution> Solution::clone() const {
  return std::make_unique<Solution>(*this);
}
