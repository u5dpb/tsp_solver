#include "move_swap_2_adj.h"

MoveSwap2Adj::MoveSwap2Adj(const Route &order, unsigned int order_1,
                           unsigned int order_2) {
  if ((order_2 != order_1 + 1) &&
      !(order_1 == order.size() - 1 && order_2 == 0))
    throw "MoveSwap2Adj::MoveSwap2Adj()\n Non-adjacent cities\n";
  this->order_1 = order_1;
  this->order_2 = order_2;
  city_1 = order[int(order_1)];
  city_2 = order[int(order_2)];
  neighbourhood_type = neighbourhood::SWAP_2_ADJ;
}

std::size_t MoveSwap2Adj::neighbourhoodSize(const Route &route) {
  return route.size();
}

Route MoveSwap2Adj::chooseMove(const Route &route) {
  Route new_route(route);
  new_route[int(order_1)] = route[int(order_2)];
  new_route[int(order_2)] = route[int(order_1)];
  return new_route;
}

double MoveSwap2Adj::getChange(const Route &route,
                               const MapData &map_data) const {
  double change = 0.0;
  // the route position before order_1
  int p_order_1 = int(order_1) - 1;
  // the route position after order_2
  int a_order_2 = int(order_2) + 1;
  // the original journey
  change -= map_data.distance(route[p_order_1], route[int(order_1)]);
  change -= map_data.distance(route[int(order_1)], route[int(order_2)]);
  change -= map_data.distance(route[int(order_2)], route[a_order_2]);
  // the new journey
  change += map_data.distance(route[p_order_1], route[int(order_2)]);
  change += map_data.distance(route[int(order_2)], route[int(order_1)]);
  change += map_data.distance(route[int(order_1)], route[a_order_2]);
  return change;
}