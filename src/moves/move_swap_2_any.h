#ifndef MOVE_SWAP_2_ANY_H_
#define MOVE_SWAP_2_ANY_H_

#include <algorithm>

#include "move.h"

class MoveSwap2Any : public BaseMove {
public:
  MoveSwap2Any() = default;
  MoveSwap2Any(const MoveSwap2Any &) = default;
  MoveSwap2Any(const Route &order, unsigned int order_1, unsigned int order_2);

  MoveSwap2Any &operator=(const MoveSwap2Any &other) = default;

  // MoveSwap2Any(MoveSwap2Any &&) = default;
  //~MoveSwap2Any() = default;
  static std::size_t neighbourhoodSize(const Route &route);
  Route chooseMove(const Route &route) override;
  double getChange(const Route &route, const MapData &map_data) const override;
  std::string name() const override { return "Swap 2 Any"; }
  // MoveSwap2Any *clone() const override { return new MoveSwap2Any(*this); }
  // std::shared_ptr<BaseMove> clone_shared_ptr() const override {
  //   return std::make_shared<MoveSwap2Any>(*this);
  // }
  // std::unique_ptr<BaseMove> clone_unique_ptr() const override {
  //   return std::make_unique<MoveSwap2Any>(*this);
  // }

private:
};

#endif // MOVE_SWAP_2_ANY_H_
