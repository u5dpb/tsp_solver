#include "move_reinsert.h"

MoveReinsert::MoveReinsert(const Route &order, unsigned int order_1,
                           unsigned int order_2) {

  if (order_2 == order_1 ||
      order_2 == (unsigned int)(((int)order_1 - 1) % (int)order.size()))
    throw "Move::makeReinsert()\n bad position for reinsertion\n";
  this->order_1 = order_1;
  this->order_2 = order_2;
  this->city_1 = order[int(order_1)];
  this->city_2 = order[int(order_1)];
  neighbourhood_type = neighbourhood::REINSERT;
}
std::size_t MoveReinsert::neighbourhoodSize(const Route &route) {
  return route.size() * (route.size() - 2u);
}

Route MoveReinsert::chooseMove(const Route &route) {

  std::vector<unsigned int> new_route;
  for (auto i = 0; i < int(route.size()); ++i) {
    if (route[i] != city_1) {
      new_route.push_back(route[i]);
    }
  }
  // {0,1,2,3,4,5} o1=0 o2=1
  // {1,0,2,3,4,5}
  // {0,1,2,3,4,5} o1=1 o2=0
  // {1,0,2,3,4,5}
  // {0,1,2,3,4,5} o1=1 o2=5
  // {0,2,3,4,1,5} o2 -1
  // {0,1,2,3,4,5}
  // {0,1,2,3,4,5}

  if (order_2 > order_1)
    new_route.insert(new_route.begin() + order_2, city_1);
  else
    new_route.insert(new_route.begin() + order_2 + 1, city_1);
  return Route(new_route);
  // Route new_route = route;
  // new_route.reverse(order_1, order_2);
  // return new_route;
}
double MoveReinsert::getChange(const Route &route,
                               const MapData &map_data) const {

  double change = 0.0;
  int p_order_1 = int(order_1) - 1;
  int a_order_1 = int(order_1) + 1;
  int p_order_2 = int(order_2);
  int a_order_2 = int(order_2) + 1;
  change -= map_data.distance(route[p_order_1], route[int(order_1)]);
  change -= map_data.distance(route[int(order_1)], route[a_order_1]);
  change -= map_data.distance(route[p_order_2], route[a_order_2]);
  change += map_data.distance(route[p_order_1], route[a_order_1]);
  change += map_data.distance(route[p_order_2], city_1);
  change += map_data.distance(city_1, route[a_order_2]);
  return change;
}