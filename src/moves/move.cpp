#include "move.h"

// Move::Move() {
//   order_1 = -1;
//   order_2 = -1;
//   city_1 = -1;
//   city_2 = -1;
//   neighbourhood_type = neighbourhood::END;
// }

// Move::Move(int order_1, int order_2, int city_1, int city_2,
//            neighbourhood::NEIGHBOURHOOD neighbourhood_type) {
//   this->order_1 = order_1;
//   this->order_2 = order_2;
//   this->city_1 = city_1;
//   this->city_2 = city_2;
//   this->neighbourhood_type = neighbourhood_type;
// }

// bool BaseMove::operator<(const BaseMove &move) const {
//   if (this->order_1 < move.order_1)
//     return true;
//   if (this->order_1 > move.order_1)
//     return false;
//   if (this->order_2 < move.order_2)
//     return true;
//   if (this->order_2 > move.order_2)
//     return false;
//   if (this->neighbourhood_type < move.neighbourhood_type)
//     return true;
//   // if (this->order_2>move.order_2)
//   //    return false;
//   return false;
// }

bool BaseMove::operator==(const BaseMove &move) const {
  return ((this->city_1 == move.city_1) && (this->city_2 == move.city_2));
}

std::ostream &operator<<(std::ostream &out, const BaseMove *move) {
  out << move->order_1 << "," << move->order_2 << "," << move->city_1 << ","
      << move->city_2 << "," << move->name() << "\n";
  return out;
}

// std::ostream &operator<<(std::ostream &out, const Move &move) {
//   out << move.order_1 << "," << move.order_2 << "," << move.city_1 << ","
//       << move.city_2 << ","
//       << neighbourhood::neighbourhood_str[move.neighbourhood_type] << "\n";
//   return out;
// }