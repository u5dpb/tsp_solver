#ifndef MOVE_SWAP_2_ADJ_H_
#define MOVE_SWAP_2_ADJ_H_

#include "move.h"

class MoveSwap2Adj : public BaseMove {
public:
  MoveSwap2Adj(const Route &order, unsigned int order_1, unsigned int order_2);
  MoveSwap2Adj(const MoveSwap2Adj &) = default;
  //~MoveSwap2Adj() = default;
  static std::size_t neighbourhoodSize(const Route &route);
  Route chooseMove(const Route &route) override;
  double getChange(const Route &route, const MapData &map_data) const override;
  std::string name() const override { return "Swap 2 Adjacent"; }
  // MoveSwap2Adj *clone() const override { return new MoveSwap2Adj(*this); }
  // std::shared_ptr<BaseMove> clone_shared_ptr() const override {
  //   return std::make_shared<MoveSwap2Adj>(*this);
  // }
  // std::unique_ptr<BaseMove> clone_unique_ptr() const override {
  //   return std::make_unique<MoveSwap2Adj>(*this);
  // }

private:
};

#endif // MOVE_SWAP_2_ADJ_H_
