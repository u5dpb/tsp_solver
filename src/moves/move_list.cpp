#include "move_list.h"

MoveList::~MoveList() { clear(); }

void MoveList::clear() { move_list.clear(); }

std::unique_ptr<BaseMove> MoveList::pop_front() {
  if (move_list.size() == 0)
    throw "MoveList::pop_front()\n Can't pop an empty MoveList\n";
  // std::unique_ptr<BaseMove> return_ptr = std::move(move_list[0]);
  // move_list.erase(move_list.begin());

  // std::swap(move_list[0], move_list.back());
  std::unique_ptr<BaseMove> return_ptr = std::move(move_list.back());
  move_list.pop_back();

  return return_ptr;
}

std::unique_ptr<BaseMove> MoveList::pop_item(unsigned int index) {
  if (index >= move_list.size())
    throw "MoveList::pop_item()\n index out of range\n";
  // std::unique_ptr<BaseMove> return_ptr = std::move(move_list[index]);
  // move_list.erase(move_list.begin() + index);

  std::swap(move_list[index], move_list.back());
  std::unique_ptr<BaseMove> return_ptr = std::move(move_list.back());
  move_list.pop_back();

  return return_ptr;
}

void MoveList::addMove(std::unique_ptr<BaseMove> move) {
  move_list.push_back(std::move(move));
}

std::size_t MoveList::size() const { return move_list.size(); }

// const BaseMove *MoveList::operator[](unsigned int index) const {
//   return move_list[index];
// }
