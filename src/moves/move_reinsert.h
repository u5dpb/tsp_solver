#ifndef MOVE_REINSERT_H_
#define MOVE_REINSERT_H_

#include "move.h"

class MoveReinsert : public BaseMove {
public:
  MoveReinsert() = default;
  MoveReinsert(const MoveReinsert &) = default;
  MoveReinsert(const Route &order, unsigned int order_1, unsigned int order_2);
  //~MoveReinsert() = default;
  static std::size_t neighbourhoodSize(const Route &route);
  Route chooseMove(const Route &route) override;
  double getChange(const Route &route, const MapData &map_data) const override;
  std::string name() const override { return "Reinsert"; }
  // MoveReinsert *clone() const override { return new MoveReinsert(*this); }
  // std::shared_ptr<BaseMove> clone_shared_ptr() const override {
  //   return std::make_shared<MoveReinsert>(*this);
  // }
  // std::unique_ptr<BaseMove> clone_unique_ptr() const override {
  //   return std::make_unique<MoveReinsert>(*this);
  // }

private:
};

#endif // MOVE_REINSERT_H_
