#ifndef MOVE_H_
#define MOVE_H_

#include <iostream>
#include <memory>
#include <vector>

#include "map_data.h"
#include "route.h"
#include "tabu_list.h"

class Move;
class TabuList;

namespace neighbourhood {
enum NEIGHBOURHOOD { SWAP_2_ADJ = 0, SWAP_2_ANY, REINSERT, REVERSE, END };

// quite a few of the tests throw a warning here

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"

static std::string neighbourhood_str(const int n) {

#pragma clang diagnostic pop

  static char const *neighbourhood_str_[] = {
      "swap 2 adjacent cities", "swap any 2 cities", "reinsert 1 city",
      "reverse a route section", "no-move"};
  return neighbourhood_str_[n];
}
} // namespace neighbourhood

class BaseMove {

  friend std::ostream &operator<<(std::ostream &stream, const BaseMove *move);

public:
  // bool operator<(const BaseMove &) const;
  bool operator==(const BaseMove &) const;

  unsigned int get_city_1() const { return city_1; }
  unsigned int get_city_2() const { return city_2; }
  unsigned int get_order_1() const { return order_1; }
  unsigned int get_order_2() const { return order_2; }

  // virtual BaseMove *clone() const = 0;
  // virtual std::shared_ptr<BaseMove> clone_shared_ptr() const = 0;
  // virtual std::unique_ptr<BaseMove> clone_unique_ptr() const = 0;

  neighbourhood::NEIGHBOURHOOD neighbourhood_type = neighbourhood::END;

  BaseMove() = default;
  BaseMove(const BaseMove &) = default;
  virtual ~BaseMove() = default;
  BaseMove &operator=(const BaseMove &other) = default;

  static std::size_t neighbourhoodSize(const Route &route);
  static std::vector<Move> generateMoveSet(const Route &route,
                                           const TabuList &tabu_list);
  virtual Route chooseMove(const Route &route) = 0;

  virtual double getChange(const Route &route,
                           const MapData &map_data) const = 0;
  virtual std::string name() const = 0;

protected:
  unsigned int city_1{};
  unsigned int city_2{};
  unsigned int order_1{};
  unsigned int order_2{};
};

// class Move : public BaseMove {

// public:
//   Move();
//   Move(int order_1, int order_2, int city_1, int city_2,
//        neighbourhood::NEIGHBOURHOOD neighbourhood_type);
//   ~Move() {}

//   Route chooseMove(const Route &route) {
//     throw "No implementation of Move::chooseMove()\n";
//   }
//   double getChange(const Route &route, const MapData &map_data) const {
//     throw "No implementation of Move::getChange()\n";
//   }

// private:
// };

#endif // MOVE_H_
