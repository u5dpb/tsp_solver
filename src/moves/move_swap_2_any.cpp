#include "move_swap_2_any.h"

MoveSwap2Any::MoveSwap2Any(const Route &order, unsigned int order_1,
                           unsigned int order_2) {
  if (order_1 == order_2)
    throw "MoveSwap2Any()\n A city cannot swap with itself\n";
  if (order_1 > order_2)
    throw "MoveSwap2Any()\n order_1 < order_2 failed\n requirement "
          "prevents symmetric moves\n";
  this->order_1 = order_1;
  this->order_2 = order_2;
  this->city_1 = order[int(order_1)];
  this->city_2 = order[int(order_2)];
  this->neighbourhood_type = neighbourhood::SWAP_2_ANY;
}
std::size_t MoveSwap2Any::neighbourhoodSize(const Route &route) {
  return route.size() * (route.size() - 1u) / 2u;
}

Route MoveSwap2Any::chooseMove(const Route &route) {
  Route new_route(route);
  std::swap(new_route[int(order_1)], new_route[int(order_2)]);
  return new_route;
}
double MoveSwap2Any::getChange(const Route &route,
                               const MapData &map_data) const {
  bool adjacent = (order_2 == order_1 + 1 ||
                   (order_1 == 0 && order_2 == route.size() - 1u));
  double change = 0.0;
  int l_order_1 = int(order_1);
  int l_order_2 = int(order_2);
  if ((order_1 == 0 && order_2 == route.size() - 1u))
    std::swap(l_order_1, l_order_2);
  // the route position before order_1
  int p_order_1 = l_order_1 - 1;
  int a_order_1 = l_order_1 + 1;
  // the route position after order_2
  int p_order_2 = l_order_2 - 1;
  int a_order_2 = l_order_2 + 1;

  // the original journey
  change -= map_data.distance(route[p_order_1], route[l_order_1]);
  if (!adjacent) {
    change -= map_data.distance(route[l_order_1], route[a_order_1]);
    change -= map_data.distance(route[p_order_2], route[l_order_2]);
  }
  change -= map_data.distance(route[l_order_2], route[a_order_2]);
  // the new journey
  change += map_data.distance(route[p_order_1], route[l_order_2]);
  if (!adjacent) {
    change += map_data.distance(route[l_order_2], route[a_order_1]);
    change += map_data.distance(route[p_order_2], route[l_order_1]);
  }
  change += map_data.distance(route[l_order_1], route[a_order_2]);
  return change;
}