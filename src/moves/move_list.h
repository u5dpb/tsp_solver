#ifndef MOVE_LIST_H_
#define MOVE_LIST_H_

#include "move.h"
#include <vector>

class MoveList {
public:
  MoveList() = default;
  MoveList(const MoveList &) = delete;
  ~MoveList();
  void clear();
  void addMove(std::unique_ptr<BaseMove>);
  std::size_t size() const;
  std::unique_ptr<BaseMove> pop_front();
  std::unique_ptr<BaseMove> pop_item(unsigned int index);
  // const BaseMove *operator[](unsigned int index) const;

private:
  std::vector<std::unique_ptr<BaseMove>> move_list;
};

#endif // MOVE_LIST_H_