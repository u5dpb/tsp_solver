#ifndef MOVE_REVERSE_H_
#define MOVE_REVERSE_H_

#include "move.h"

class MoveReverse : public BaseMove {
public:
  MoveReverse(const Route &order, unsigned int order_1, unsigned int order_2);
  MoveReverse(const MoveReverse &) = default;
  //~MoveReverse() = default;
  static std::size_t neighbourhoodSize(const Route &route);
  // static std::vector<MoveReverse> generateMoveSet(const Route &route,
  //                                                 const TabuList &tabu_list);
  Route chooseMove(const Route &route) override;
  double getChange(const Route &route, const MapData &map_data) const override;
  std::string name() const override { return "Reverse"; }
  // MoveReverse *clone() const override { return new MoveReverse(*this); }
  // std::shared_ptr<BaseMove> clone_shared_ptr() const override {
  //   return std::make_shared<MoveReverse>(*this);
  // }
  // std::unique_ptr<BaseMove> clone_unique_ptr() const override {
  //   return std::make_unique<MoveReverse>(*this);
  // }

private:
};

#endif // MOVE_REVERSE_H_
