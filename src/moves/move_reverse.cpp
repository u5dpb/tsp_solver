#include "move_reverse.h"

MoveReverse::MoveReverse(const Route &order, unsigned int order_1,
                         unsigned int order_2) {

  if (order_1 > order_2)
    throw "Move::makeReverse()\n order_1 < order_2 violated\n";
  if (order_1 == order_2)
    throw "Move::makeReverse()\n order_1 != order_2 violated\n";
  if (order_2 - order_1 >= order.size() - 2u)
    throw "Move::makeReverse()\n reversing the whole route\n";
  this->order_1 = order_1;
  this->order_2 = order_2;
  this->city_1 = order[int(order_1)];
  this->city_2 = order[int(order_2)];
  this->neighbourhood_type = neighbourhood::REVERSE;
}
std::size_t MoveReverse::neighbourhoodSize(const Route &route) {
  return (route.size() * (route.size() - 1u) / 2u) - 3u;
}
// std::vector<MoveReverse>
// MoveReverse::generateMoveSet(const Route &route, const TabuList &tabu_list) {

//   std::vector<MoveReverse> move_set;
//   for (auto i = 0u; i < route.size(); ++i) {
//     for (auto j = i + 1u; j < route.size() && j - i < route.size() - 2u; ++j)
//     {
//       MoveReverse new_move(route, i, j);
//       if (!tabu_list.contains(&new_move)) {
//         move_set.push_back(new_move);
//       }
//     }
//   }
//   return move_set;
// }
Route MoveReverse::chooseMove(const Route &route) {

  Route new_route = route;
  for (auto i = 0u; i <= order_2 - order_1; ++i) {
    new_route[int(order_1 + i)] = route[int(order_2 - i)];
  }
  return new_route;
}
double MoveReverse::getChange(const Route &route,
                              const MapData &map_data) const {

  double change = 0.0;
  // the route position before order_1
  int p_order_1 = int(order_1) - 1;
  // the route position after order_2
  int a_order_2 = int(order_2) + 1;
  // the original journey
  change -= map_data.distance(route[p_order_1], route[int(order_1)]);
  change -= map_data.distance(route[int(order_2)], route[a_order_2]);
  // the new journey
  change += map_data.distance(route[p_order_1], route[int(order_2)]);
  change += map_data.distance(route[int(order_1)], route[a_order_2]);
  return change;
}
