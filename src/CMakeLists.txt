cmake_minimum_required(VERSION 3.0.0)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
project(tsp_solver)

add_subdirectory(heuristics)
add_subdirectory(initial_route)
add_subdirectory(moves)
add_subdirectory(problem_structures)
add_subdirectory(ui)
add_subdirectory(ui/search_settings)
add_subdirectory(include)
add_subdirectory(rnd)
add_subdirectory(file_io)

# add_executable(${PROJECT_NAME} main.cpp main_window.cpp settings_tab.cpp tabu_settings.cpp
#                map_drawing_area.cpp text_output.cpp tabu.cpp route.cpp initial_order.cpp 
#                steepest_descent.cpp first_found.cpp simulated_annealing.cpp sa_settings.cpp 
#                search.cpp move.cpp map_data.cpp)
add_executable(${PROJECT_NAME} main.cpp 
ui/main_window.cpp 
ui/settings_tab.cpp 
ui/map_drawing_area.cpp 
ui/text_output.cpp 
#    ui/help.cpp
ui/search_settings/simulated_annealing_settings_frame.cpp 
ui/search_settings/tabu_settings_frame.cpp
ui/search_settings/steepest_descent_settings_frame.cpp
ui/search_settings/first_found_descent_settings_frame.cpp
initial_route/initial_route.cpp 
initial_route/initial_route_option.cpp 
initial_route/random_initial_route.cpp 
initial_route/nearest_neighbour_initial_route.cpp 
initial_route/successive_inclusion_initial_route.cpp 
problem_structures/route.cpp 
problem_structures/solution.cpp 
problem_structures/map_data.cpp 
problem_structures/neighbourhood.cpp 
heuristics/tabu_list.cpp
heuristics/search_option.cpp
heuristics/search.cpp
heuristics/tabu_search.cpp
heuristics/simulated_annealing.cpp
heuristics/first_found_descent.cpp
heuristics/steepest_descent.cpp
rnd/uniform_double_random_number.cpp
moves/move.cpp
moves/move_list.cpp
moves/move_reinsert.cpp
moves/move_reverse.cpp
moves/move_swap_2_adj.cpp
moves/move_swap_2_any.cpp
file_io/file_io.cpp ui/initial_order_settings_frame.cpp ui/initial_order_settings_frame.h)

find_package( PkgConfig REQUIRED )
pkg_check_modules( GTK REQUIRED gtk+-3.0 cairo gtkmm-3.0 sigc++-2.0)
include_directories( ${GTK_INCLUDE_DIRS} 
                    file_io/
                    include/ 
                    initial_route/ 
                    problem_structures/ 
                    heuristics/ 
                    problem_structures/ 
                    ui/ 
                    ui/search_settings/ 
                    rnd/ 
                    moves/
                    )
set(TSP_SOLVER_LIBS ${GTK_LIBRARIES} )
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries( tsp_solver  ${TSP_SOLVER_LIBS} Threads::Threads)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
#target_link_libraries(tsp_solver PRIVATE Threads::Threads)
