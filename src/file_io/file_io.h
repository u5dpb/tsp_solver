#ifndef FILE_IO_H_
#define FILE_IO_H_

#include <fstream>
#include <functional>
#include <string>

namespace file_io {
// typedef void (*FileIOFunctionCall)(std::ifstream &in,
//                                    const std::string &filename);

class FileIO {
public:
  void load_file(const std::string &filename,
                 std::function<void(std::ifstream &, const std::string &)>
                     read_function) const;
  void save_file(std::string filename);

private:
};
} // namespace file_io
#endif // FILE_IO_H_