#include "file_io.h"

namespace file_io {
void FileIO::load_file(const std::string &filename,
                       std::function<void(std::ifstream &, const std::string &)>
                           read_function) const {
  std::ifstream in;
  in.open(filename.c_str(), std::ios::in);
  if (!in) { // load file error
    throw "void FileIO::load_file()\n File not found\n";
  }

  read_function(in, filename);
  in.close();
  in.clear();
  // signal to update map data
  //   settings_tab.setFilename(filename);
  //   settings_tab.setCityLimits(1, map_data.no_locations());
  //  initial_route->setNoLocations(map_data.getNoLocations());
}

void FileIO::save_file(std::string filename) {
  // text_output.saveBuffer(filename);
}
} // namespace file_io