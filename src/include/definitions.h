// some basic definitions for the TSP Solver program

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

#include <string>

// namespace neighbourhood
// {
//   enum NEIGHBOURHOOD
//   {
//     SWAP_2_ADJ=0,
//     SWAP_2_ANY,
//     REINSERT,
//     REVERSE,
//     END
//   };
//   static char const *neighbourhood_str[]=
//       {"swap 2 adjacent cities",
//        "swap any 2 cities",
//        "reinsert 1 city",
//        "reverse a route section"
//       };
// };

// namespace initial {
// enum INITIAL_SOLUTION {
//   NEAREST_NEIGHBOUR = 0,
//   SUCCESSIVE_INCLUSION,
//   RANDOM,
//   //    DIY,
//   END
// };
// static char const *initial_solution_str[] = {
//     "Nearest Neighbour", "Successive Inclusion", "Random", "None"
//     //    "Do It Yourself"
// };
// } // namespace initial

// struct City {
//   unsigned int number;
//   unsigned int to_unsigned_int() const { return number - 1; }
// };

struct Coord {
  int x;
  int y;
};

inline bool operator==(const Coord &lhs, const Coord &rhs) {
  return (lhs.x == rhs.x && lhs.y == rhs.y);
}

// static std::string map_file = "../maps/map.txt";

// namespace scale {
// static double scale = 2.5;
// static int x_plus = 100;
// static int y_plus = 10;
// } // namespace scale

// static int sleep_time = 0;

#endif // DEFINTIONS_H_
