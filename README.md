# TSP Solver (Travelling Salesman Problem Solver)

This is a program developed in 2008. It was written for an Optimisation course at Lancaster University. The students on the course used it for an assignment. It allows students to experiment with various meta-heuristics. Students were required to use this program to generate results and demonstrate their understanding of how results were generated.

# Implementation

I have re-implemented a lot of this code as on opportunity to learn about techniques such as Unit Testing (with Google Test) and the SOLID principles.

# Overview

## TSP

The Travelling Salesman Problem (TSP) consists of finding the quickest route around all locations in a map. In this case this is minimum distance loop around the a number of cities in the USA. The direct distance between two cities is used (i.e. as the crow flies).

## Search Process

The search process has two stages:
* generate an initial (complete) route.
* use a meta-heuristic to improve the route.

## Meta-heuristics

A number of "neighbour search" meta-heuristics are provided. These search method choose potential improvements by considering "neighbours" of the current route. A neighbour will differ by, for example, swapping the position of two cities in the route.

The meta-heuristics used are not designed to be particularly efficient in terms of design. They correspond to what was taught on the course.

# Install

## CMake

The code can be copiled using the following commands:

    find . -name CMakeCache.txt -exec rm {} \;
    cmake src/
    cmake -B build/
    make tsp_solver

## Docker

A docker file can be created and run using the following linux commands:

    docker build . -t tsp
    xhost +
    docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY tsp

# Usage

Before the application can run a map file must be loaded. These can be found in `maps/`.

More details of how to use to program can be found in guide.html. Some details were also provided on the course website, but, unfortunately, these have been lost over time. I have changed a few things in my revisions to the code (e.g. the delay is now measured in seconds).

The aim of the assignment wasn't that students should find the best solution but that they should demonstrate knowledge of "how these things work".

