# run tsp_solver from a docker container
#FROM fedora
FROM alpine

# Install gtk
#RUN dnf -y install gtkmm30-devel gtk3-devel cairomm libsigc++-devel cmake g++ findutils && \
#    dnf clean all && \
#    rm -rf /var/cache/dnf
RUN apk add gtkmm3-dev cmake g++ build-base

# Copy tsp_solver to /
COPY . /tsp_solver

WORKDIR /tsp_solver

#Compile tsp_solver
RUN find . -name CMakeCache.txt -exec rm {} \; && \
    cmake src/ && \
    cmake -B build/ && \
    make tsp_solver

CMD /tsp_solver/tsp_solver

# To run:
# xhost +
# docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY tsp
