# TODO List

- [ ] Simulated Annealing - tidy up the title.
- [ ] Introductory docs for the API
- [ ] Make MapData completely const
- [ ] - create a factory function
- [ ] - const everything
- [ ] - remove the mutexes
- [ ] Make a smaller Dockerfile
- [ ] Is save output still working?
- [ ] Tidy up the CMake files
- [ ] Run profiling on the search methods
- [ ] Tidy up simulated annealing numerical output
- [ ] Inject the search methods into MainWindow.

# TODOing

- [ ] API of Improvement methods
- [ ] : Simulated Annealing
- [ ] : - example code
- [ ] : - API
- [ ] : - details desription
- [ ] : Tabu Search
- [ ] : Tabu List

# TODOne

- [x] Initial route seed and starting city. Grey out the irrelevant one.
- [x] Random starting city is 0.
- [x] Give the initial order as part of an extended title.
- [x] Give the improvement method as part of an extended title.
- [x] Successive Inclusion map update split to two lines
- [x] Nearest Neighbour map update split to two lines
- [x] "Search finished" message
- [x] ~~~Create a "city" class/struct to reflect the fact that they are numbered from 1.~~~ Modified the MapData class interface to reference cities from 1 and not 0
- [x] API of Initial Order methods
- [x] Test whether Route works worse as a linked list - yes it does
- [x] Implement a Solution clone function : https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rh-copy
- [x] Use the Solution clone in the Search methods
- [x] Only use unique_ptr in the Search methods
- [x] Seperate out the move generation for the Solution class into a move factory class
- [x] API of Improvement methods
- [x] : First Found Descent
- [x] : - example code
- [x] : - API
- [x] : - details description
- [x] : Steepest Descent
- [x] : - example code
- [x] : - API
- [x] : - details desription