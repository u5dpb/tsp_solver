
# First Found Descent

A search method that chooses neighbourhood moves at random until it finds one that improves the current solution. Once found that move is made to update/improve the solution. The process is repeated. If no improvement is found search finishes.

The search process calls a set of signal functions that signal improvements in the current solution and text updates of the search process.

# FirstFoudnDescent API

```c++
FirstFoundDescent(const std::unique_ptr<BaseSolution> &initial_solution,
                  std::unique_ptr<Neighbourhood> &&neighbourhood)
```
Constructor
* `const std::unique_ptr<BaseSolution> &initial_solution` is the **complete** initial solution that search will start from.
* `std::unique_ptr<Neighbourhood> &&neighbourhood` is the neighbourhood factory object that generates the possible neighbourhood moves.

---

```c++
void run_search(const MapData &map_data, RandomNumber &rnd)
```
Function that starts the search process.
* `const MapData &map_data` is the map of cities used.
* `RandomNumber &rnd` is a random number generator used to choose the next move from the neighbourhood.

---

```c++
void run_search(const MapData &map_data)
```
Function that starts the search process using the `seed` defined in `void set_seed(unsigned int seed)` and the `UniformDoubleRandomNumber` random number generator.
* `const MapData &map_data` is the map of cities used.

---

```c++
const std::string name() const
```
Returns the name of the search process.

---

```c++
const std::vector<std::string> title() const
```
Returns the *title* of the search process. This title is the name plus search parameters.

---

```c++
void end_search()
```
Function ends search prematurely.

---

```c++
void set_seed(unsigned int seed)
```
Setter.
* `unsigned int seed` is the seed to be used by the `UniformDoubleRandomNumber` random number generator.

---

```c++
void set_initial_solution(const std::unique_ptr<BaseSolution> &initial_solution)
```
Setter.
* `const std::unique_ptr<BaseSolution> &initial_solution` is the initial solution to be used by the search process.

---

```c++
std::unique_ptr<BaseSolution> best_solution() const
```
Returns the best solution so far.

---

```c++
void set_signal_functions(SearchSignalsBase *search_signal_functions)
```
Setter.
* `SearchSignalsBase *search_signal_functions` is a set of signals to be called by search. These signals allow a UI to update changes as search progressed. They are initialised to an empty set of signals that do nothing.

---

```c++
void setDelay(double delay)
```
Setter.
* `double delay` is the delay in seconds between each loop/iteration of the search algorithm.

---

```c++
void setTextPeriod(unsigned int text_period)
```

* `unsigned int text_period` is the frequency of periodic text updates when search is not improving the current solution. It is initialised to `0`. When `0` only text updates due to solution improvements will be signalled/produced.

---

```c++
Route current_route() const
```
Returns the current route being considered by search.

---

```c++
std::vector<std::string> search_status() const
```
Returns a number of strings detailing a snapshot of progress. Information could include: search name, iteration/loop number, current solution cost, best solution cost. This information could be used for a graphical display of search progress.

---

```c++
std::vector<std::string> summary()
```
Returns a number of strings detailing current progress. These strings form a buffer from a text description of search from start to finish. The information in the strings will be deleted from the buffer when this function is called and details of further progress added. This information could be used for a text display of search progress.

# Example

The following code generates a map of 100 randomly placed locations, generates an initial random route around the locations and then performs First Found Descent with different random seeds.

File: `example_first_found_descent.cpp`

```c++
#include <definitions.h>
#include <first_found_descent.h>
#include <map_data.h>
#include <neighbourhood.h>
#include <random_initial_route.h>
#include <solution.h>
#include <stdlib.h>
#include <uniform_double_random_number.h>

int main() {
  const unsigned int map_seed = 1;
  UniformDoubleRandomNumber rnd(map_seed);
  const unsigned int no_locations = 100;
  std::vector<Coord> coords;
  for (auto i = 0u; i < no_locations; ++i) {
    coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  }
  MapData map_data(coords);

  RandomInitialRoute random;
  random.setSeed(1);
  random.generateInitialSolution(map_data);
  Route route = random.current_route();
  double cost = Route::evaluate(route, map_data);
  std::cout << random.title() << " = "
            << " " << cost << "\n";
  for (auto search_seed = 1u; search_seed <= 100u; ++search_seed) {

    std::unique_ptr<BaseSolution> initial_solution =
        std::make_unique<Solution>(route);
    Neighbourhood neighbourhood(false, true, false, true);
    FirstFoundDescent first_found_descent(
        initial_solution, std::make_unique<Neighbourhood>(neighbourhood));
    first_found_descent.set_seed(search_seed);
    first_found_descent.run_search(map_data);
    for (auto str : first_found_descent.title()) {
      std::cout << str << "\n";
    }
    Route search_route = first_found_descent.current_route();
    cost = Route::evaluate(search_route, map_data);
    std::cout << "Route = " << search_route << "\nRoute cost ";
    std::cout << cost << "\n";
  }

  exit(0);
}
```

Output:

```
Random seed 1 =  51921.4
First Found Descent :
   seed 1
Route = 8 31 29 11 13 1 82 5 73 10 94 79 64 27 37 19 100 70 85 53 52 95 83 41 28 32 38 16 97 56 99 4 48 42 43 96 68 34 98 39 6 58 57 3 76 30 63 81 18 17 22 7 71 9 36 66 69 21 49 14 90 50 44 46 33 75 40 74 51 55 45 62 86 60 89 61 77 65 59 91 93 78 92 23 12 87 25 20 15 80 84 35 24 26 2 72 88 54 67 47 
Route cost 8280.55
First Found Descent :
   seed 2
Route = 44 33 74 51 55 45 62 86 60 89 61 77 65 59 91 93 78 92 57 58 3 76 30 63 81 18 17 22 7 71 9 6 39 4 48 42 43 96 68 34 98 87 25 12 23 20 15 80 84 35 24 26 2 72 88 54 47 67 99 56 97 8 31 29 11 13 1 82 5 73 10 94 95 83 41 16 38 28 32 36 66 69 52 53 85 70 100 19 37 79 64 27 14 50 90 49 21 40 75 46 
Route cost 8294.96
First Found Descent :
   seed 3
Route = 6 39 4 68 96 43 42 48 67 54 47 31 29 8 99 56 97 38 16 28 32 9 58 3 76 30 63 81 18 17 22 7 71 36 66 69 52 41 83 11 13 1 82 5 73 10 94 95 53 85 70 100 19 37 79 64 27 14 49 21 90 50 44 46 33 75 40 74 51 55 45 62 86 60 89 61 77 65 59 91 93 78 92 57 87 25 12 23 20 15 80 84 35 24 26 2 72 88 34 98 
Route cost 8567.96
...
```

# Implementation Details

An example implementation could be:

```c++
void FirstFoundDescent::run_search(const MapData &map_data, RandomNumber &rnd) {
  bool improvement_found = true;
  best_solution = initial_solution;
  unsigned int iteration = 0;
  while (improvement_found) {
    iteration++;
    neighbourhood.generate_move_set(best_solution);
    improvement_found = false;
    while (!improvement_found && neighbourhood.move_set_size() > 0) {
      new_solution = neighbourhood.move_set_pop_random(rnd);

      if (new_solution.cost() < best_solution.cost()) {
        improvement_found = true;
        best_solution = new_solution;
      }
    }
  }
}

```