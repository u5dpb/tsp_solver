#include <definitions.h>
#include <first_found_descent.h>
#include <map_data.h>
#include <neighbourhood.h>
#include <random_initial_route.h>
#include <solution.h>
#include <stdlib.h>
#include <uniform_double_random_number.h>

int main() {
  const unsigned int map_seed = 1;
  UniformDoubleRandomNumber rnd(map_seed);
  const unsigned int no_locations = 100;
  std::vector<Coord> coords;
  for (auto i = 0u; i < no_locations; ++i) {
    coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  }
  MapData map_data(coords);

  RandomInitialRoute random;
  random.setSeed(1);
  random.generateInitialSolution(map_data);
  Route route = random.current_route();
  double cost = Route::evaluate(route, map_data);
  std::cout << random.title() << " = "
            << " " << cost << "\n";
  for (auto search_seed = 1u; search_seed <= 100u; ++search_seed) {

    std::unique_ptr<BaseSolution> initial_solution =
        std::make_unique<Solution>(route);
    Neighbourhood neighbourhood(false, true, false, true);
    FirstFoundDescent first_found_descent(
        initial_solution, std::make_unique<Neighbourhood>(neighbourhood));
    first_found_descent.set_seed(search_seed);
    first_found_descent.run_search(map_data);
    for (auto str : first_found_descent.title()) {
      std::cout << str << "\n";
    }
    Route search_route = first_found_descent.current_route();
    cost = Route::evaluate(search_route, map_data);
    std::cout << "Route = " << search_route << "\nRoute cost ";
    std::cout << cost << "\n";
  }

  exit(0);
}