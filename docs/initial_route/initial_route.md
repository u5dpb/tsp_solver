# Initial Route

This is a base class for the various methods that create a simple route around cities stored in the `MapData` class.

# Initial Route API

```c++
InitialRoute();
```
Constructor

---

```c++
void set_signal_functions(SearchSignalsBase *search_signal_functions)
```
Setter
* `SearchSignalsBase *search_signal_functions` is a class containing various signals that are called during search so that a UI can update.

---

```c++
void setStartingCity(unsigned int starting_city);
```
Setter
* `unsigned int starting_city` is the starting city (where relevant) of the route generation method.


---

```c++
void setSeed(unsigned int seed);
```
Setter
* `unsigned int seed` is the seed (where relevent) of the random number generator used by the route generation method.

---

```c++
void setDelay(double delay);
```
Setter
* `double delay` is the delay in seconds after each iteration of the route generation method.

---

```c++
void end_search()
```
Calling this function will terminate the route generation method early.

---

```c++
bool searching();
```

Returns whether the route generation method is currently running.

---

```c++
void search(const MapData &map_data);
```
Function starts the route generation method **with** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.
  
---

```c++
virtual void generateInitialSolution(const MapData &map_data) {}
```

Function starts the route generation method **without** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.


---

```c++
virtual const std::string name() const
```
Getter. Returns the name of the route generation method.

---

```c++
virtual const std::string title() const
```

Getter. Returns a title for the route generation method. The title expands on the name giving further relevent details of the method (e.g. starting parameters).

---

```c++
Route current_route() const;
```
Getter. Returns the current (possibly incomeplete) route of the route generation method.

---

```c++
std::string summary()
```
Getter. Returns a summary of the current (possibly incomeplete) route of the route generation method. This summary is stored in a buffer and will be deleted once retrieved by this function call.

---

```c++
std::vector<std::string> search_text_strings() const
```
Getter. Returns a number of strings to summarise progress suitable for display on screen.
