#include <definitions.h>
#include <map_data.h>
#include <random_initial_route.h>
#include <stdlib.h>
#include <uniform_double_random_number.h>

int main() {
  const unsigned int seed = 1;
  UniformDoubleRandomNumber rnd(seed);
  const unsigned int no_locations = 100;
  std::vector<Coord> coords;
  coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  for (auto i = 1u; i < no_locations; ++i) {
    coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  }
  MapData map_data(coords);

  for (auto search_seed = 1u; search_seed <= 50u; ++search_seed) {

    RandomInitialRoute random;
    random.setSeed(search_seed);
    random.generateInitialSolution(map_data);
    Route route = random.current_route();
    double cost = Route::evaluate(route, map_data);
    std::cout << random.title() << " = "
              << " " << cost << "\n";
  }

  exit(0);
}