# Random Initial Route

This method creates a route that visits all cities in random order.

# RandomInitialRoute API


```c++
void generateInitialSolution(const MapData &map_data) override
```
Function starts the route generation method **without** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.

---

```c++
void generateInitialSolution(const MapData &map_data, RandomNumber &rnd_dist)
```
Function starts the route generation method **without** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.
* `RandomNumber &rnd_dist` is the random number generator used.

```c++
const std::string name() const override 
```
Getter. Returns the name of the route generation method.

---

```c++
const std::string title() const override 
```
Getter. Returns a title for the route generation method. The title expands on the name giving further relevent details of the method (e.g. starting parameters).


---

```c++
void set_signal_functions(SearchSignalsBase *search_signal_functions) 
```
Setter
* `SearchSignalsBase *search_signal_functions` is a class containing various signals that are called during search so that a UI can update.

---

```c++
void setStartingCity(unsigned int starting_city)
```
Unused setter

---

```c++
void setSeed(unsigned int seed)
```
* `unsigned int seed` is the seed used in random number generation.

---

```c++
void setDelay(double delay)
```
Setter
* `double delay` is the delay in seconds after each iteration of the route generation method.


---

```c++
bool searching()
```
Returns whether the route generation method is currently running.

---

```c++
void search(const MapData &map_data)
```
Function starts the route generation method **with** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.

---

```c++
void end_search()
```
Calling this function will terminate the route generation method early.

---

```c++
Route current_route() const
```
Getter. Returns the current (possibly incomeplete) route of the route generation method.

---

```c++
std::string summary()
```
Getter. Returns a summary of the current (possibly incomeplete) route of the route generation method. This summary is stored in a buffer and will be deleted once retrieved by this function call.

---

```c++
std::vector<std::string> search_text_strings() const
```
Getter. Returns a number of strings to summarise progress suitable for display on screen.

---

# Example usage

The following program generates a map with 100 randomly generated locations. It then considers random number generation seeds and generates a route route around all fo the cities for that seed. It evaluates the route.

File: `example_random.cpp`

```c++
#include <definitions.h>
#include <map_data.h>
#include <random_initial_route.h>
#include <stdlib.h>
#include <uniform_double_random_number.h>

int main() {
  const unsigned int seed = 1;
  UniformDoubleRandomNumber rnd(seed);
  const unsigned int no_locations = 100;
  std::vector<Coord> coords;
  coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  for (auto i = 1u; i < no_locations; ++i) {
    coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  }
  MapData map_data(coords);

  for (auto search_seed = 1u; search_seed <= 50u; ++search_seed) {

    RandomInitialRoute random;
    random.setSeed(search_seed);
    random.generateInitialSolution(map_data);
    Route route = random.current_route();
    double cost = Route::evaluate(route, map_data);
    std::cout << random.title() << " = "
              << " " << cost << "\n";
  }

  exit(0);
}
```

The output is:

```
Random seed 1 =  51921.4
Random seed 2 =  47555
Random seed 3 =  51693.9
Random seed 4 =  52914.7
Random seed 5 =  50398.5
Random seed 6 =  56481.8
Random seed 7 =  48949.8
Random seed 8 =  49589.7
Random seed 9 =  51616.4
Random seed 10 =  48136.3
Random seed 11 =  51761.4
Random seed 12 =  52201.5
...
```