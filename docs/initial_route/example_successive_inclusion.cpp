#include <definitions.h>
#include <map_data.h>
#include <stdlib.h>
#include <successive_inclusion_initial_route.h>
#include <uniform_double_random_number.h>

int main() {
  const unsigned int seed = 1;
  UniformDoubleRandomNumber rnd(seed);
  const unsigned int no_locations = 100;
  std::vector<Coord> coords;
  coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  for (auto i = 1u; i < no_locations; ++i) {
    coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  }
  MapData map_data(coords);

  for (auto starting_city = 1u; starting_city <= no_locations;
       ++starting_city) {

    SuccessiveInclusionInitialRoute si;
    si.setStartingCity(starting_city);
    si.generateInitialSolution(map_data);
    Route route = si.current_route();
    double cost = Route::evaluate(route, map_data);
    std::cout << si.title() << " = "
              << " " << cost << "\n";
  }

  exit(0);
}