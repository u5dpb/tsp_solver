# Nearest Neighbour Initial Route

Starting from a given starting city this method will visit the nearest city to the curretn city in turn.

# NearestNeightbourInitialRoute API


```c++
void generateInitialSolution(const MapData &map_data) override
```
Function starts the route generation method **without** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.

---

```c++
const std::string name() const override 
```
Getter. Returns the name of the route generation method.

---

```c++
const std::string title() const override 
```
Getter. Returns a title for the route generation method. The title expands on the name giving further relevent details of the method (e.g. starting parameters).


---

```c++
void set_signal_functions(SearchSignalsBase *search_signal_functions) 
```
Setter
* `SearchSignalsBase *search_signal_functions` is a class containing various signals that are called during search so that a UI can update.

---

```c++
void setStartingCity(unsigned int starting_city)
```
Setter
* `unsigned int starting_city` is the starting city (where relevant) of the route generation method.

---

```c++
void setSeed(unsigned int seed)
```
Unused Setter

---

```c++
void setDelay(double delay)
```
Setter
* `double delay` is the delay in seconds after each iteration of the route generation method.


---

```c++
bool searching()
```
Returns whether the route generation method is currently running.

---

```c++
void search(const MapData &map_data)
```
Function starts the route generation method **with** support for the `searching()` function.
* `const MapData &map_data` is the map of cities used for this search.

---

```c++
void end_search()
```
Calling this function will terminate the route generation method early.

---

```c++
Route current_route() const
```
Getter. Returns the current (possibly incomeplete) route of the route generation method.

---

```c++
std::string summary()
```
Getter. Returns a summary of the current (possibly incomeplete) route of the route generation method. This summary is stored in a buffer and will be deleted once retrieved by this function call.

---

```c++
std::vector<std::string> search_text_strings() const
```
Getter. Returns a number of strings to summarise progress suitable for display on screen.

---

# Example usage

The following program generates a map with 100 randomly generated locations. It then considers each starting city and generates a nearest neighbour route around all fo the cities from that starting point. It evaluates the route.

File: `example_nearest_neighbour.cpp`

```c++
#include <definitions.h>
#include <map_data.h>
#include <nearest_neighbour_initial_route.h>
#include <stdlib.h>
#include <uniform_double_random_number.h>

int main() {
  const unsigned int seed = 1;
  UniformDoubleRandomNumber rnd(seed);
  const unsigned int no_locations = 100;
  std::vector<Coord> coords;
  coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  for (auto i = 1u; i < no_locations; ++i) {
    coords.push_back({(int)rnd.generate(1000u), (int)rnd.generate(1000u)});
  }
  MapData map_data(coords);

  for (auto starting_city = 1u; starting_city <= no_locations;
       ++starting_city) {

    NearestNeighbourInitialRoute nn;
    nn.setStartingCity(starting_city);
    nn.generateInitialSolution(map_data);
    Route route = nn.current_route();
    double cost = Route::evaluate(route, map_data);
    std::cout << nn.title() << " = "
              << " " << cost << "\n";  }

  exit(0);
}
```

The output is:

```
Nearest Neighbour start 1 =  9444.98
Nearest Neighbour start 2 =  9716.38
Nearest Neighbour start 3 =  9631.64
Nearest Neighbour start 4 =  9429.31
Nearest Neighbour start 5 =  9477.5
Nearest Neighbour start 6 =  9386.78
Nearest Neighbour start 7 =  9539.75
Nearest Neighbour start 8 =  9650.46
Nearest Neighbour start 9 =  9527.92
Nearest Neighbour start 10 =  9509.22
Nearest Neighbour start 11 =  10069.1
Nearest Neighbour start 12 =  9483.99
...
```