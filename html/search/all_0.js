var searchData=
[
  ['action_5fgroup_0',['action_group',['../classMainWindow.html#acd24d9e851227f5aa395a7a5e63baff3',1,'MainWindow']]],
  ['add_1',['add',['../classTabuList.html#affe8cb3c71f803aecdd32557749aff63',1,'TabuList']]],
  ['add_5fsearch_5fstatus_2',['add_search_status',['../classSearch.html#acc30f84a2a69ab01392671be9789b55a',1,'Search::add_search_status(const std::string &amp;text)'],['../classSearch.html#a2e5a23a7344a224ed0e82fbd86e780d6',1,'Search::add_search_status(const std::vector&lt; std::string &gt; &amp;text_vector)']]],
  ['addmove_3',['addMove',['../classMoveList.html#a81b1ae401996d594bfc853256ab437b1',1,'MoveList']]],
  ['adj_5fdelay_5ffactor_4',['adj_delay_factor',['../classSettingsTab.html#ada72fbfeedc0d4d6abefac04268165ee',1,'SettingsTab']]],
  ['adj_5fstarting_5fcity_5',['adj_starting_city',['../classSettingsTab.html#a1047c417f84741ce350dfad57d4c0879',1,'SettingsTab']]],
  ['adj_5ftext_5fperiod_6',['adj_text_period',['../classSettingsTab.html#a01042caf65013fdadf36bf5e289d374b',1,'SettingsTab']]]
];
