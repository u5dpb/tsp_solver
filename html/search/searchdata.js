var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwxy~",
  1: "bcfhimnrstu",
  2: "fimns",
  3: "bcdfhimnrstu",
  4: "abcdefghilmnoprstuv~",
  5: "abcdefgilmnorstuwxy",
  6: "ins",
  7: "efnrst",
  8: "o",
  9: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros"
};

