var searchData=
[
  ['neighbourhood_5ftype_811',['neighbourhood_type',['../classBaseMove.html#a4a110e68757c797fb70919ec55913a3f',1,'BaseMove']]],
  ['neighbourhood_5fvbox_812',['neighbourhood_vbox',['../classSettingsTab.html#af61065dc5b132cca25a62b6170b1745c',1,'SettingsTab']]],
  ['no_5fcost_5fcomparisons_813',['no_cost_comparisons',['../classSimulatedAnnealingSettingsFrame.html#afdee16b7fbdfbe92f0dc1edb22346754',1,'SimulatedAnnealingSettingsFrame']]],
  ['no_5fcost_5fcomparisons_5fadj_814',['no_cost_comparisons_adj',['../classSimulatedAnnealingSettingsFrame.html#afd33f9cd59eb8322bfcbc039177b2b50',1,'SimulatedAnnealingSettingsFrame']]],
  ['no_5fcost_5fcomparisons_5flabel_815',['no_cost_comparisons_label',['../classSimulatedAnnealingSettingsFrame.html#af88c0b62501e6f392f4828bc9b290c37',1,'SimulatedAnnealingSettingsFrame']]],
  ['no_5fcost_5fcomparisons_5fspin_5fbutton_816',['no_cost_comparisons_spin_button',['../classSimulatedAnnealingSettingsFrame.html#a09396b1e56ea63dfd291446ddc72b515',1,'SimulatedAnnealingSettingsFrame']]],
  ['no_5flocations_817',['no_locations',['../classSettingsTab.html#a5a2af521d10b162934d38b5c698623ee',1,'SettingsTab']]],
  ['no_5fmoves_818',['no_moves',['../structTabuSettings.html#a185ce8f17318e1e76e8b697ff8c1ddc8',1,'TabuSettings::no_moves()'],['../classTabuSettingsFrame.html#a38de5dd3b43b1d3073222c093b8fbc74',1,'TabuSettingsFrame::no_moves()']]],
  ['no_5fmoves_5fadj_819',['no_moves_adj',['../classTabuSettingsFrame.html#a03ba35f8ac7b9f3118cf35d3fddacf3f',1,'TabuSettingsFrame']]],
  ['no_5fmoves_5flabel_820',['no_moves_label',['../classTabuSettingsFrame.html#a5389c17a01b90a807a7990e651ae5258',1,'TabuSettingsFrame']]],
  ['no_5fmoves_5fspin_5fbutton_821',['no_moves_spin_button',['../classTabuSettingsFrame.html#a88a204d2ee3310e0b498c071f4fd2e00',1,'TabuSettingsFrame']]],
  ['notebook_822',['notebook',['../classMainWindow.html#a15e1bc28c7a215334a6d7e6ba636b56e',1,'MainWindow']]]
];
