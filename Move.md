# What is a Move?

Defining a move, it's variables and functions.

## Who uses a move?

A move is used by neighbourhood search methods. Neighbourhood search methods can be simple heuristics (e.g. Steepest Descent) or more complicated (e.g. Tabu Search and Simulated Annealing).

## What are the variables of a move?

The variables are:

 * city_1
 * city_2
 * order_1
 * order_2

These store slightly different things depending on the move. But usually refer to the order in which the cities appear on the route and the corresponding cities to the order poisitions.

## What moves are defined in this program?

0 -> 4 -> 2 -> 1 -> 3 -> 5 ->

This is a complete route around 6 cities. The "->" at the end signifies that it is a loop. Moves can act across the start and end of this route (because it is a loop).

### Swap 2 Adjacent Cities

0 -> 4 -> 2 -> 1 -> 3 -> 5 ->

city_1 = 4 ; city_2 = 2 ; order_1 = 1 ; order_2 = 2 

0 -> 2 -> 4 -> 1 -> 3 -> 5 ->

It is possible to swap the first and end cities (0 and 5 in this example).

### Swap any 2 Cities

0 -> 4 -> 2 -> 1 -> 3 -> 5 ->

city_1 = 4 ; city_2 = 5 ; order_1 = 1 ; order_2 = 5 

0 -> 5 -> 2 -> 1 -> 3 -> 4 ->

A city cannot swap with itself.

order_1 < order_2 to prevent symmetric moves being included within the neighbourhood.

### Reinsert 1 City

0 -> 4 -> 2 -> 1 -> 3 -> 5 ->

  0    1    2    3    4    5   

city_1 = 4 ; city_2 = 4 ; order_1 = 1 ; order_2 = 4

0 -> 2 -> 1 -> 3 -> 4 -> 5 -> 

city_2 = order_2 always.

The potential values for order_2 = the potential values for order 1 - 2.

### Reverse a Route Section

0 -> 4 -> 2 -> 1 -> 3 -> 5 ->

city_1 = 4 ; city_2 = 3 ; order_1 = 1 ; order_2 = 4

0 -> 3 -> 1 -> 2 -> 4 -> 5 ->

order_1 < order_2 to prevent symmatrical moves being included in the neighbourhood.

WRONG: but this should allow looping (so reversing from city 5 to city 2 is okay).
BECAUSE: The loop is covered by reversing 2 to 5 (i.e. tje opposite)

A city cannot reverse with itself.

## What are the functions of a move?

### int neighbourhood_size(Route, neighbourhood_type)

Returns the number of neighbours of a given type for the given route.

### int neighbourhood_size(Route, set<neighbourhood_type>)

Returns the number of neighboiurs of the given types for the given route.

Note: not implemented yet!

### vector<Move> generateMoveSet(Route, set<neighbourhood_type>, set<Move> tabu_list)

Generates a complete list of moves for a given route that:

* come from the different neighbour types
* do not appear on the tabu list
* are NOT randomised

Note, that such a move set might include some copies as different neighbourhoods can produce the same move.

### Route chooseMove(Route, Move)

Applies the given move to the given route.

### double getChange(Route, Move, Distance Matrix)

Gives to alpha (increase/decrease in cost) when implementing the given move.