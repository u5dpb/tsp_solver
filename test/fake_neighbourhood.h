#include "neighbourhood.h"

class FakeNeighbourhood : public Neighbourhood {
public:
  FakeNeighbourhood(bool use_swap2adj, bool use_swap2any, bool use_reinsert,
                    bool use_reverse)
      : Neighbourhood(use_swap2adj, use_swap2any, use_reinsert, use_reverse) {}
  FakeNeighbourhood(const FakeNeighbourhood &neighbourhood)
      : Neighbourhood(neighbourhood) {
    this->generate_move_set_override = neighbourhood.generate_move_set_override;
  }
  //~FakeNeighbourhood() { Neighbourhood::~Neighbourhood(); }

  void generate_move_set(const Route &route) override {
    route_ = route;
    generate_move_set_override(this->move_list, route);
  }

  std::function<void(MoveList &, const Route &)> generate_move_set_override;

// TabuList &tabu_list is required because of the override but throws a warning
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
  void generate_move_set(const Route &route, TabuList &tabu_list) override {
#pragma clang diagnostic pop
    route_ = route;
    generate_move_set_override(this->move_list, route);
  }
};