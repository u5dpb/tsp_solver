#include <gtest/gtest.h>

#include "problem_structures/map_data.h"
#include "problem_structures/solution.h"
#include <deque>
#include <set>
#include <vector>

class FakeRandomNumber_0 : public RandomNumber {
  void set_seed(const uint seed) override { this->seed = seed; }
  double generate() override { return 0.0 * seed; }
  uint generate(uint value) override { return 0 * value; }
  uint seed;
};

class FakeRandomNumber_1 : public RandomNumber {
  void set_seed(const uint seed) override {
    this->seed = seed;
    this->seed = 1u;
  }
  double generate() override { return 0.999; }
  uint generate(uint value) override { return value - 1; }
  uint seed;
};

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};
MapData map_data(coords);

TEST(SolutionClass, SolutionConstructorRoute) {
  Route route({1, 2, 3, 4});
  EXPECT_NO_THROW(Solution s = Solution(route));
}

TEST(SolutionClass, GetRoute) {
  Route route({1, 2, 3, 4});
  Solution s = Solution(route);
  ASSERT_NO_THROW(s.route(););
  std::vector<uint> expected = {1, 2, 3, 4};
  EXPECT_EQ(s.route().order(), expected);
}

TEST(SolutionClass, evaluate) {
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  s1.evaluate(map_data);
  EXPECT_DOUBLE_EQ(s1.cost(), 6.0);
}

TEST(SolutionClass, operatorEquals) {
  MoveList move_list_1, move_list_2;
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  s1.evaluate(map_data);
  Solution s2 = s1;
  EXPECT_EQ(s1.route().order(), s2.route().order());
  EXPECT_EQ(s1.cost(), s2.cost());
}

TEST(SolutionClass, Clone) {
  MoveList move_list_1, move_list_2;
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  s1.evaluate(map_data);
  std::unique_ptr<BaseSolution> s2 = s1.clone();
  EXPECT_EQ(s1.route().order(), s2->route().order());
  EXPECT_EQ(s1.cost(), s2->cost());
}
