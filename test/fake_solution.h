#include "solution.h"

class FakeSolution : public Solution {
public:
  FakeSolution() = delete;
  FakeSolution(const FakeSolution &sol) : Solution(sol) { *this = sol; }
  FakeSolution(const Route &route) : Solution(route) {}
  void evaluate(const MapData &map_data) override {
    cost_ = Route::evaluate(route_, map_data);
  }
  double cost() const override { return cost_; }
  Route route() const override { return route_; }

  FakeSolution &operator=(const FakeSolution &rhs) {
    if (this != &rhs) {
      this->route_ = rhs.route_;
      this->cost_ = rhs.cost_;
    }
    return *this;
  }

  std::unique_ptr<BaseSolution> clone() const override {
    return std::make_unique<FakeSolution>(*this);
  }

private:
};