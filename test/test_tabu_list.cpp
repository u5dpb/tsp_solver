#include "heuristics/tabu_list.h"
#include "moves/move_swap_2_adj.h"
#include "moves/move_swap_2_any.h"
#include <gtest/gtest.h>

TEST(TabuList, Constructor) {
  EXPECT_NO_THROW(TabuList tabu_list = TabuList(10););
}

TEST(TabuList, addmove) {
  TabuList tabu_list = TabuList(10);
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move = MoveSwap2Any(route, 0, 5);
  EXPECT_NO_THROW(tabu_list.add(std::make_unique<MoveSwap2Any>(move)));
}

TEST(TabuList, containsTrue) {
  TabuList tabu_list = TabuList(10);
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move = MoveSwap2Any(route, 0, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  move = MoveSwap2Any(route, 1, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  move = MoveSwap2Any(route, 2, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  MoveSwap2Any test_move = MoveSwap2Any(route, 0, 5);
  EXPECT_EQ(tabu_list.contains(&test_move), true);
  test_move = MoveSwap2Any(route, 1, 5);
  EXPECT_EQ(tabu_list.contains(&test_move), true);
  test_move = MoveSwap2Any(route, 2, 5);
  EXPECT_EQ(tabu_list.contains(&test_move), true);
}
TEST(TabuList, containsFalse) {
  TabuList tabu_list = TabuList(10);
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move = MoveSwap2Any(route, 0, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  move = MoveSwap2Any(route, 1, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  move = MoveSwap2Any(route, 2, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  MoveSwap2Any test_move = MoveSwap2Any(route, 0, 4);
  EXPECT_EQ(tabu_list.contains(&test_move), false);
}

TEST(TabuList, fixedLength) {
  TabuList tabu_list = TabuList(3);
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move = MoveSwap2Any(route, 0, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  move = MoveSwap2Any(route, 1, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  move = MoveSwap2Any(route, 2, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  MoveSwap2Any test_move = MoveSwap2Any(route, 0, 5);
  EXPECT_EQ(tabu_list.contains(&test_move), true);
  move = MoveSwap2Any(route, 3, 5);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  EXPECT_EQ(tabu_list.contains(&test_move), false);
}