#include <functional>
#include <gtest/gtest.h>
#include <vector>

#include "heuristics/first_found_descent.h"
#include "moves/move.h"
#include "moves/move_list.h"
#include "moves/move_swap_2_any.h"
//#include "problem_structures/base_solution.h"
#include "problem_structures/neighbourhood.h"
#include "problem_structures/route.h"
#include "problem_structures/solution.h"
#include "random_number.h"

#include "fake_neighbourhood.h"
#include "fake_solution.h"

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};
MapData map_data(coords);

class FakeRandomNumber : public RandomNumber {
  void set_seed(const uint seed) override { this->seed = seed; }
  double generate() override { return 0.0; }
  uint generate(uint value) override { return 0 * value * seed; }
  uint seed;
};

class SearchSignalsFake : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() {}
  void signal_function_search_status_update() {}
  void signal_function_summary_update() {}
  void signal_function_initial_route_changed() {}
};

TEST(FirstFoundDescent, InitialRoute) {
  Route initial_route = Route({1, 2, 3, 4, 5, 6});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  EXPECT_NO_THROW(FirstFoundDescent ffd(
                      p_initial_solution,
                      std::make_unique<FakeNeighbourhood>(neighbourhood)););
}

// is it making the first improvement
TEST(FirstFoundDescent, FirstImprovement) {
  FakeRandomNumber fake_random_number;
  Route initial_route = Route({1, 6, 3, 4, 5, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() == std::vector<uint>({1, 6, 3, 4, 5, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
    }
  };
  FirstFoundDescent ffd(p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(ffd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(ffd.run_search(map_data, fake_random_number));
  EXPECT_EQ(ffd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6}));
}

// is it skipping if no improvement
TEST(FirstFoundDescent, FirstSkip) {
  FakeRandomNumber fake_random_number;

  Route initial_route = Route({1, 2, 3, 4, 5, 6});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() == std::vector<uint>({1, 2, 3, 4, 5, 6})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
    }
  };
  FirstFoundDescent ffd(p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(ffd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(ffd.run_search(map_data, fake_random_number));
  EXPECT_EQ(ffd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6}));
}

// is it making successive improvements
TEST(FirstFoundDescent, SuccesiveImprovement) {
  FakeRandomNumber fake_random_number;

  Route initial_route = Route({1, 6, 4, 3, 5, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() == std::vector<uint>({1, 6, 4, 3, 5, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
    } else if (route.order() == std::vector<uint>({1, 2, 4, 3, 5, 6})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    }
  };
  FirstFoundDescent ffd(p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(ffd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(ffd.run_search(map_data, fake_random_number));
  EXPECT_EQ(ffd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6}));
}
// is it finishing when there are no improvements

TEST(FirstFoundDescent, NoImprovementFinish) {
  FakeRandomNumber fake_random_number;

  Route initial_route = Route({1, 6, 4, 3, 5, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() == std::vector<uint>({1, 6, 4, 3, 5, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
    } else if (route.order() == std::vector<uint>({1, 2, 4, 3, 5, 6})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 3));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 5));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    }
  };
  FirstFoundDescent ffd(p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(ffd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(ffd.run_search(map_data, fake_random_number));
  EXPECT_EQ(ffd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6}));
}