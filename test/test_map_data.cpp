
#include <gtest/gtest.h>
#include <iostream>
#include <math.h>
#include <vector>

#include "definitions.h"
#include "problem_structures/map_data.h"

TEST(MapDataClass, ConstructorEmpty) {
  EXPECT_NO_THROW(MapData mp = MapData(););
}

TEST(MapDataClass, ConstructorCoords) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}};
  EXPECT_NO_THROW(MapData mp = MapData(coords));
}

TEST(MapDataClass, NoLocations) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
  MapData mp = MapData(coords);
  EXPECT_EQ(mp.no_locations(), 4);
}

TEST(MapDataClass, coordinateFirst) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
  MapData mp = MapData(coords);
  Coord expected = {0, 0};
  EXPECT_EQ(mp.coordinate(1), expected);
}

TEST(MapDataClass, coordinateSecond) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
  MapData mp = MapData(coords);
  Coord expected = {1, 1};
  EXPECT_EQ(mp.coordinate(2), expected);
}

TEST(MapDataClass, coordinateLast) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
  MapData mp = MapData(coords);
  Coord expected = {3, 3};
  EXPECT_EQ(mp.coordinate(4), expected);
}

TEST(MapDataClass, coordinateOutOfRange) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
  MapData mp = MapData(coords);
  EXPECT_ANY_THROW(mp.coordinate(5));
}

// TEST(MapDataClass, set_coordinates) {
//   std::vector<Coord> coords = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
//   MapData mp = MapData();
//   EXPECT_NO_THROW(mp.set_coordinates(coords));
// }

TEST(MapDataClass, generate_distance_matrixValues) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {1, 2}, {3, 1}};
  MapData mp = MapData(coords);
  EXPECT_DOUBLE_EQ(mp.distance(1, 1), 0.0);
  EXPECT_DOUBLE_EQ(mp.distance(1, 2), sqrt(2));
  EXPECT_DOUBLE_EQ(mp.distance(1, 3), sqrt(5));
  EXPECT_DOUBLE_EQ(mp.distance(1, 4), sqrt(10));
}

TEST(MapDataClass, distanceOutOfRange1) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {1, 2}, {3, 1}};
  MapData mp = MapData(coords);
  EXPECT_DEATH(mp.distance(1, 7), "Assertion.*failed");
}

TEST(MapDataClass, distanceOutOfRange2) {
  std::vector<Coord> coords = {{0, 0}, {1, 1}, {1, 2}, {3, 1}};
  MapData mp = MapData(coords);
  EXPECT_DEATH(mp.distance(7, 2), "Assertion.*failed");
}

const std::string map_file = R""""(
46
12  11    23  10    91  16   159  17   181  20   195  14
210  20   214  29   238  31   236  43   242  47   268  31
282  18   288   5   296   4   309  23   298  36   299  50
283  67   277  96   270 114   257 134   261 157   270 176
263 180   251 169   238 154   212 152   204 163   181 160
162 168   157 180   155 190   144 187   131 167   120 163
112 171   101 158    88 146    77 150    53 142    33 140
 19 124     8 112     2  74     6  56 
 25
   225 112   202 119   248  68   208  40    56 125    70  32   211 137
    16  29    25  77    38  25   249 102   172  24   267  81   257  63
    62 106   241 118   170 132   275  88    20  16   208  53   185  28
   129  69   114 101    99  56    65  84
)"""";

TEST(MapDataClass, loadMapDataNoThrow) {
  MapData mp;
  std::istringstream in;
  in.str(map_file);
  EXPECT_NO_THROW(
      mp.load_map_data(in, "/home/danb/code/tsp_solver/maps/CITY25.TXT"));
}

// TEST(MapDataClass, loadMapDataThrowNoFile) {
//   MapData mp;
//   EXPECT_ANY_THROW(mp.load_map_data("../maps/CITYXXX.TXT"));
// }

TEST(MapDataClass, loadMapDataNoLocations) {
  MapData mp;
  std::istringstream in;
  in.str(map_file);
  mp.load_map_data(in, "/home/danb/code/tsp_solver/maps/CITY25.TXT");
  EXPECT_EQ(mp.no_locations(), 25);
}