#include "file_io/file_io.h"
#include <gtest/gtest.h>

TEST(FileIOTest, ConstructorEmpty) {
  EXPECT_NO_THROW(file_io::FileIO file_handler;);
}

std::string test_string;
void read_function(std::ifstream &in, const std::string &filename) {

  test_string = filename;
}

TEST(FileIOTest, ReadFileNoThrow) {
  file_io::FileIO file_handler;
  test_string = "";
  EXPECT_NO_THROW(file_handler.load_file("/proc/cpuinfo", &read_function););
}

TEST(FileIOTest, ReadFileFilename) {
  file_io::FileIO file_handler;
  test_string = "";
  file_handler.load_file("/proc/cpuinfo", &read_function);
  EXPECT_EQ(test_string, "/proc/cpuinfo");
}

class TestClass {
public:
  std::string test_filename;
  void read_function(std::ifstream &in, const std::string &filename) {
    test_filename = filename;
  }
};

TEST(FileIOTest, ReadFileNoThrowWithBind) {
  file_io::FileIO file_handler;
  TestClass test_class;
  test_class.test_filename = "";
  EXPECT_NO_THROW(file_handler.load_file(
      "/proc/cpuinfo",
      std::bind(&TestClass::read_function, &test_class, std::placeholders::_1,
                std::placeholders::_2)););
}

TEST(FileIOTest, ReadFileFilenameWithBind) {
  file_io::FileIO file_handler;
  TestClass test_class;
  test_class.test_filename = "";
  file_handler.load_file(
      "/proc/cpuinfo", std::bind(&TestClass::read_function, &test_class,
                                 std::placeholders::_1, std::placeholders::_2));
  EXPECT_EQ(test_class.test_filename, "/proc/cpuinfo");
}