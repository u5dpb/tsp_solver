#include <functional>
#include <gtest/gtest.h>
#include <vector>

#include "moves/move.h"
#include "moves/move_list.h"
#include "moves/move_swap_2_any.h"
//#include "problem_structures/base_solution.h"
#include "problem_structures/route.h"
#include "problem_structures/solution.h"
// #include "random_number.h"

#include "fake_solution.h"

// TEST(FakeSolution, EmptyMoveSet) {
//   Route initial_route = Route({1, 6, 3, 4, 5, 2});
//   FakeSolution initial_solution(initial_route);
//   MoveList move_list;
//   initial_solution.generate_move_set_override = [](MoveList &move_list,
//                                                    const Route &route) {
//     // if (route.order() == std::vector<uint>({1, 6, 3, 4, 5, 2})) {
//     //   move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
//     // }
//   };
//   std::unique_ptr<BaseSolution> p_initial_solution =
//       std::make_unique<FakeSolution>(initial_solution);
//   initial_solution.generate_move_set(move_list);
//   EXPECT_EQ(initial_solution.move_set_size(move_list), 0);
//   p_initial_solution->generate_move_set(move_list);
//   EXPECT_EQ(p_initial_solution->move_set_size(move_list), 0);
// }

// TEST(FakeSolution, MoveSetOneTime) {
//   Route initial_route = Route({1, 6, 3, 4, 5, 2});
//   FakeSolution initial_solution(initial_route);
//   MoveList move_list;
//   initial_solution.generate_move_set_override = [](MoveList &move_list,
//                                                    const Route &route) {
//     if (route.order() == std::vector<uint>({1, 6, 3, 4, 5, 2})) {
//       move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
//     }
//   };
//   initial_solution.generate_move_set(move_list);
//   EXPECT_EQ(initial_solution.move_set_size(move_list), 1);
// }

// TEST(FakeSolution, MoveSetOneTimePtr) {
//   Route initial_route = Route({1, 6, 3, 4, 5, 2});
//   FakeSolution initial_solution(initial_route);
//   MoveList move_list;
//   initial_solution.generate_move_set_override = [](MoveList &move_list,
//                                                    const Route &route) {
//     if (route.order() == std::vector<uint>({1, 6, 3, 4, 5, 2})) {
//       move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
//     }
//   };
//   std::unique_ptr<BaseSolution> p_initial_solution =
//       std::make_unique<FakeSolution>(initial_solution);
//   p_initial_solution->generate_move_set(move_list);
//   EXPECT_EQ(p_initial_solution->move_set_size(move_list), 1);
// }

// TEST(FakeSolution, MoveSetEmptyAfterMove) {
//   Route initial_route = Route({1, 6, 3, 4, 5, 2});
//   FakeSolution initial_solution(initial_route);
//   MoveList move_list;
//   initial_solution.generate_move_set_override = [](MoveList &move_list,
//                                                    const Route &route) {
//     if (route.order() == std::vector<uint>({1, 6, 3, 4, 5, 2})) {
//       move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
//     }
//   };
//   std::unique_ptr<BaseSolution> p_initial_solution =
//       std::make_unique<FakeSolution>(initial_solution);

//   p_initial_solution->generate_move_set(move_list);
//   std::unique_ptr<BaseSolution> p_new_solution =
//       p_initial_solution->move_set_pop(move_list);
//   p_new_solution->generate_move_set(move_list);
//   EXPECT_EQ(p_new_solution->move_set_size(move_list), 0);
// }

TEST(FakeSolution, Clone) {
  Route initial_route = Route({1, 6, 3, 4, 5, 2});
  FakeSolution initial_solution(initial_route);
  MoveList move_list;
  // initial_solution.generate_move_set_override = [](MoveList &move_list,
  //                                                  const Route &route) {
  //   if (route.order() == std::vector<uint>({1, 6, 3, 4, 5, 2})) {
  //     move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 5));
  //   }
  // };
  auto p_initial_solution = initial_solution.clone();
  EXPECT_EQ(initial_route.order(), p_initial_solution->route().order());
  // p_initial_solution->generate_move_set(move_list);
  // EXPECT_EQ(p_initial_solution->move_set_size(move_list), 1);
}