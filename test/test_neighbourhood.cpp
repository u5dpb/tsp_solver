#include <gtest/gtest.h>

#include "problem_structures/map_data.h"
#include "problem_structures/neighbourhood.h"
#include "problem_structures/solution.h"
#include <deque>
#include <set>
#include <vector>

class FakeRandomNumber_0 : public RandomNumber {
  void set_seed(const uint seed) override { this->seed = seed; }
  double generate() override { return 0.0 * seed; }
  uint generate(uint value) override { return 0 * value; }
  uint seed;
};

class FakeRandomNumber_1 : public RandomNumber {
  void set_seed(const uint seed) override {
    this->seed = seed;
    this->seed = 1u;
  }
  double generate() override { return 0.999; }
  uint generate(uint value) override { return value - 1; }
  uint seed;
};

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};
MapData map_data(coords);

TEST(NeighbourhoodClass, Test) {
  Route route({1, 2, 3, 4});
  Solution s = Solution(route);
  Neighbourhood neighbourhood(true, true, true, true);
}

TEST(NeighbourhoodClass, GenerateMoveSetNoThrow) {
  MoveList move_list;
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(true, true, true, true);
  ASSERT_NO_THROW(neighbourhood_1.generate_move_set(route));
  EXPECT_GT(neighbourhood_1.move_set_size(), 0);
}

TEST(NeighbourhoodClass, TabuListWorks) {
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Solution s2 = Solution(route);
  Neighbourhood neighbourhood_1(true, true, true, true);
  neighbourhood_1.generate_move_set(route);
  Neighbourhood neighbourhood_2(true, true, true, true);
  TabuList tabu_list(1);
  MoveSwap2Any move = MoveSwap2Any(route, 1, 3);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  neighbourhood_2.generate_move_set(route, tabu_list);
  EXPECT_EQ(neighbourhood_1.move_set_size(),
            neighbourhood_2.move_set_size() + 1);
}

TEST(NeighbourhoodClass, popNoThrow) {
  MoveList move_list;
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set(route);
  // Solution s2;
  EXPECT_NO_THROW(std::unique_ptr<BaseSolution> s2 =
                      neighbourhood.move_set_pop(&s1));
}

TEST(NeighbourhoodClass, popSampleSolutions) {
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(true, true, true, true);
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> route_set = {
      {1, 2, 4, 3}, {1, 3, 2, 4}, {2, 1, 3, 4}, {4, 2, 3, 1}, {3, 2, 1, 4},
      {1, 4, 3, 2}, {2, 3, 1, 4}, {1, 3, 4, 2}, {1, 4, 2, 3}};
  std::set<std::vector<uint>> solution_route_set;
  while (neighbourhood_1.move_set_size() > 0) {
    auto s2 = neighbourhood_1.move_set_pop(&s1);
    solution_route_set.insert((*s2).route().order());
  }
  for (std::vector<uint> route_to_find : route_set) {

    EXPECT_NE(solution_route_set.find(route_to_find), solution_route_set.end());
  }
}
TEST(NeighbourhoodClass, popSampleSwapAdjOnly) {
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(true, false, false, false);
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> good_route_set = {
      {1, 2, 4, 3}, {1, 3, 2, 4}, {2, 1, 3, 4}, {4, 2, 3, 1}};
  std::set<std::vector<uint>> bad_route_set = {
      {3, 2, 1, 4}, {1, 4, 3, 2}, {2, 3, 1, 4}, {1, 3, 4, 2}, {1, 4, 2, 3}};
  std::set<std::vector<uint>> solution_route_set;
  while (neighbourhood_1.move_set_size() > 0) {

    solution_route_set.insert(
        (neighbourhood_1.move_set_pop(&s1))->route().order());
  }
  for (std::vector<uint> route_to_find : good_route_set) {

    EXPECT_NE(solution_route_set.find(route_to_find), solution_route_set.end());
  }
  for (std::vector<uint> route_to_find : bad_route_set) {

    EXPECT_EQ(solution_route_set.find(route_to_find), solution_route_set.end());
  }
}

TEST(NeighbourhoodClass, popSampleSwapAnyOnly) {
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(false, true, false, false);
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> good_route_set = {{1, 2, 4, 3}, {1, 3, 2, 4},
                                                {2, 1, 3, 4}, {4, 2, 3, 1},
                                                {3, 2, 1, 4}, {1, 4, 3, 2}};
  std::set<std::vector<uint>> bad_route_set = {
      {2, 3, 1, 4}, {1, 3, 4, 2}, {1, 4, 2, 3}};
  std::set<std::vector<uint>> solution_route_set;
  while (neighbourhood_1.move_set_size() > 0) {

    solution_route_set.insert(
        neighbourhood_1.move_set_pop(&s1)->route().order());
  }
  for (std::vector<uint> route_to_find : good_route_set) {

    EXPECT_NE(solution_route_set.find(route_to_find), solution_route_set.end());
  }
  for (std::vector<uint> route_to_find : bad_route_set) {

    EXPECT_EQ(solution_route_set.find(route_to_find), solution_route_set.end());
  }
}

TEST(NeighbourhoodClass, popSampleReinsertOnly) {
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(false, false, true, false);
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> good_route_set = {{1, 2, 4, 3}, {1, 3, 2, 4},
                                                {2, 1, 3, 4}, {2, 3, 1, 4},
                                                {1, 3, 4, 2}, {1, 4, 2, 3}};
  std::set<std::vector<uint>> bad_route_set = {
      {4, 2, 3, 1}, {3, 2, 1, 4}, {1, 4, 3, 2}};
  std::set<std::vector<uint>> solution_route_set;
  while (neighbourhood_1.move_set_size() > 0) {

    solution_route_set.insert(
        neighbourhood_1.move_set_pop(&s1)->route().order());
  }
  for (std::vector<uint> route_to_find : good_route_set) {

    EXPECT_NE(solution_route_set.find(route_to_find), solution_route_set.end());
  }
  for (std::vector<uint> route_to_find : bad_route_set) {

    EXPECT_EQ(solution_route_set.find(route_to_find), solution_route_set.end());
  }
}

TEST(SolutionClass, popSampleReverseOnly) {
  MoveList move_list;
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(false, false, false, true);
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> good_route_set = {
      {1, 2, 4, 3}, {1, 3, 2, 4}, {2, 1, 3, 4}};
  std::set<std::vector<uint>> bad_route_set = {{2, 3, 1, 4}, {1, 3, 4, 2},
                                               {1, 4, 2, 3}, {3, 2, 1, 4},
                                               {1, 4, 3, 2}, {4, 2, 3, 1}};
  std::set<std::vector<uint>> solution_route_set;
  while (neighbourhood_1.move_set_size() > 0) {
    auto move = neighbourhood_1.move_set_pop(&s1);
    // std::cout << move.route() << "\n";
    solution_route_set.insert(move->route().order());
  }
  for (std::vector<uint> route_to_find : good_route_set) {

    EXPECT_NE(solution_route_set.find(route_to_find), solution_route_set.end());
  }
  for (std::vector<uint> route_to_find : bad_route_set) {

    EXPECT_EQ(solution_route_set.find(route_to_find), solution_route_set.end());
  }
}

TEST(SolutionClass, popNoRandomThrow) {
  MoveList move_list;
  FakeRandomNumber_0 rnd_0;
  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(true, true, true, true);
  neighbourhood_1.generate_move_set(route);
  // Solution s2;
  EXPECT_NO_THROW(std::unique_ptr<BaseSolution> s2 =
                      neighbourhood_1.move_set_pop_random(&s1, rnd_0));
}

TEST(SolutionClass, popNoRandomReverseOrder) {
  MoveList move_list;
  FakeRandomNumber_0 rnd_0;
  FakeRandomNumber_1 rnd_1;

  Route route({1, 2, 3, 4});
  Solution s1 = Solution(route);
  Neighbourhood neighbourhood_1(true, true, true, true);
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> forward_order;
  while (neighbourhood_1.move_set_size() > 0) {
    forward_order.insert(
        neighbourhood_1.move_set_pop_random(&s1, rnd_0)->route().order());
  }
  neighbourhood_1.generate_move_set(route);
  std::set<std::vector<uint>> backward_order;
  while (neighbourhood_1.move_set_size() > 0) {
    backward_order.insert(
        neighbourhood_1.move_set_pop_random(&s1, rnd_1)->route().order());
  }
  EXPECT_EQ(forward_order, backward_order);
}

TEST(Neighbourhood, Swap2AdjMoveSetCorrectSize) {
  Route route({1, 4, 2, 3, 6, 5});
  Neighbourhood neighbourhood_1(true, false, false, false);
  neighbourhood_1.generate_move_set(route);
  TabuList empty_tabu_list(10);

  EXPECT_EQ(MoveSwap2Adj::neighbourhoodSize(route),
            neighbourhood_1.move_set_size());
}

TEST(Neighbourhood, Swap2AnyMoveSetCorrectSize) {
  Route route({1, 4, 2, 3, 6, 5});
  Neighbourhood neighbourhood_1(false, true, false, false);
  neighbourhood_1.generate_move_set(route);
  TabuList empty_tabu_list(10);

  EXPECT_EQ(MoveSwap2Any::neighbourhoodSize(route),
            neighbourhood_1.move_set_size());
}

TEST(Neighbourhood, ReinsertMoveSetCorrectSize) {
  Route route({1, 4, 2, 3, 6, 5});
  Neighbourhood neighbourhood_1(false, false, true, false);
  neighbourhood_1.generate_move_set(route);
  TabuList empty_tabu_list(10);

  EXPECT_EQ(MoveReinsert::neighbourhoodSize(route),
            neighbourhood_1.move_set_size());
}

TEST(Neighbourhood, ReverseMoveSetCorrectSize) {
  Route route({1, 4, 2, 3, 6, 5});
  Neighbourhood neighbourhood_1(false, false, false, true);
  neighbourhood_1.generate_move_set(route);
  TabuList empty_tabu_list(10);

  EXPECT_EQ(MoveReverse::neighbourhoodSize(route),
            neighbourhood_1.move_set_size());
}

TEST(neighbourhood, Swap2AdjMoveSetTabuListWorks) {
  Route route({1, 4, 2, 3, 6, 5});
  TabuList tabu_list(10);
  MoveSwap2Adj move(route, 0, 1);
  tabu_list.add(std::make_unique<MoveSwap2Adj>(move));
  Neighbourhood neighbourhood_1(true, false, false, false);
  neighbourhood_1.generate_move_set(route, tabu_list);

  EXPECT_EQ(MoveSwap2Adj::neighbourhoodSize(route) - 1,
            neighbourhood_1.move_set_size());
}

TEST(Neighbourhood, Swap2AnyMoveSetTabuListWorks) {
  Route route({1, 4, 2, 3, 6, 5});
  TabuList tabu_list(10);
  MoveSwap2Any move(route, 0, 1);
  tabu_list.add(std::make_unique<MoveSwap2Any>(move));
  Neighbourhood neighbourhood_1(false, true, false, false);
  neighbourhood_1.generate_move_set(route, tabu_list);

  EXPECT_EQ(MoveSwap2Any::neighbourhoodSize(route) - 1,
            neighbourhood_1.move_set_size());
}

TEST(Neighbourhood, ReinsertMoveSetTabuListWorks) {
  Route route({1, 4, 2, 3, 6, 5});
  TabuList tabu_list(10);
  MoveReinsert move(route, 1, 3);
  tabu_list.add(std::make_unique<MoveReinsert>(move));
  Neighbourhood neighbourhood_1(false, false, true, false);
  neighbourhood_1.generate_move_set(route, tabu_list);

  EXPECT_EQ(MoveReinsert::neighbourhoodSize(route) - (route.size() - 2),
            neighbourhood_1.move_set_size());
}

TEST(Neighbourhood, ReverseMoveSetTabuListWorks) {
  Route route({1, 4, 2, 3, 6, 5});
  TabuList tabu_list(10);
  MoveReverse move(route, 0, 3);
  tabu_list.add(std::make_unique<MoveReverse>(move));
  Neighbourhood neighbourhood_1(false, false, false, true);
  neighbourhood_1.generate_move_set(route, tabu_list);

  EXPECT_EQ(MoveReverse::neighbourhoodSize(route) - 1,
            neighbourhood_1.move_set_size());
}
