
#include <gtest/gtest.h>

#include "definitions.h"
#include "heuristics/search_signals_base.h"
#include "initial_route/initial_route.h"
#include "initial_route/nearest_neighbour_initial_route.h"
#include "initial_route/random_initial_route.h"
#include "initial_route/successive_inclusion_initial_route.h"
#include "problem_structures/map_data.h"
#include "random_number.h"

class FakeRandomNumber : public RandomNumber {
public:
  FakeRandomNumber(std::vector<double> vals) {
    if (vals.size() == 0)
      throw "Bad FakeRandomNumber\n";
    values = vals;
    c_values = 0;
  }
  double generate() {
    double result = values[c_values++];
    if (c_values >= values.size())
      c_values = 0 * seed;
    return result;
  }
  uint generate(uint value) { return uint(floor(double(value) * generate())); }
  void set_seed(uint seed) { this->seed = seed; }

private:
  uint c_values;
  std::vector<double> values;
  uint seed;
};

class SearchSignalsFake : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() {}
  void signal_function_initial_route_changed() {}
  void signal_function_search_status_update() {}
  void signal_function_summary_update() {}
};

TEST(InitialRoute, ConstructorEmpty) { EXPECT_NO_THROW(InitialRoute io;); }

// TEST(RandomInitialRoute, setSeed) {
//   RandomInitialRoute io();
//   EXPECT_NO_THROW(io.setSeed(10););
// }

TEST(RandomInitialRoute, generateInitialSolutionRandom) {

  RandomInitialRoute rnd_io;
  // rnd_io.setInitialSolution(initial::RANDOM);
  std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};

  MapData map_data = MapData(coords);
  ASSERT_NO_THROW(rnd_io.setStartingCity(0u));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(rnd_io.set_signal_functions(&search_signals));
  FakeRandomNumber rnd_dist({0.0});
  ASSERT_NO_THROW(rnd_io.generateInitialSolution(map_data, rnd_dist));
  std::vector<std::string> expected_output = {"Random", "Current cost: 10"};
  EXPECT_EQ(rnd_io.search_text_strings(), expected_output);
  EXPECT_EQ(rnd_io.current_route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6}));
}

TEST(RandomInitialRoute, generateInitialSolutionRandomReverse) {
  RandomInitialRoute rnd_io;
  // rnd_io.setInitialSolution(initial::RANDOM);
  std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(rnd_io.set_signal_functions(&search_signals));
  MapData map_data = MapData(coords);
  // ASSERT_NO_THROW(rnd_io.setStartingCity(5));
  FakeRandomNumber rnd_dist({0.99, 0.7, 0.3, 0.0, 0.0});
  ASSERT_NO_THROW(rnd_io.generateInitialSolution(map_data, rnd_dist));
  std::vector<std::string> expected_output = {"Random", "Current cost: 10"};
  EXPECT_EQ(rnd_io.search_text_strings(), expected_output);
  EXPECT_EQ(rnd_io.current_route().order(),
            std::vector<uint>({6, 5, 4, 3, 2, 1}));
}

TEST(NearestNeighbourInitialRoute, generateInitialSolution) {

  NearestNeighbourInitialRoute io;
  std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};

  MapData map_data = MapData(coords);
  ASSERT_NO_THROW(io.setStartingCity(1));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(io.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(io.generateInitialSolution(map_data));
  std::vector<std::string> expected_output = {"Nearest Neighbour",
                                              "Current cost: 10"};
  EXPECT_EQ(io.search_text_strings(), expected_output);
  EXPECT_EQ(io.current_route().order(), std::vector<uint>({1, 2, 3, 4, 5, 6}));
}
TEST(NearestNeighbourInitialRoute, generateInitialSolutionReverse) {

  NearestNeighbourInitialRoute io;
  std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(io.set_signal_functions(&search_signals));
  MapData map_data = MapData(coords);
  ASSERT_NO_THROW(io.setStartingCity(6));

  ASSERT_NO_THROW(io.generateInitialSolution(map_data));
  std::vector<std::string> expected_output = {"Nearest Neighbour",
                                              "Current cost: 10"};
  EXPECT_EQ(io.search_text_strings(), expected_output);
  EXPECT_EQ(io.current_route().order(), std::vector<uint>({6, 5, 4, 3, 2, 1}));
}

TEST(NearestNeighbourInitialRoute, generateInitialSolutionCenter) {

  NearestNeighbourInitialRoute io;
  std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 4}, {0, 5}, {0, 6}};
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(io.set_signal_functions(&search_signals));
  MapData map_data = MapData(coords);
  ASSERT_NO_THROW(io.setStartingCity(4));

  ASSERT_NO_THROW(io.generateInitialSolution(map_data));
  std::vector<std::string> expected_output = {"Nearest Neighbour",
                                              "Current cost: 12"};
  EXPECT_EQ(io.search_text_strings(), expected_output);
  EXPECT_EQ(io.current_route().order(), std::vector<uint>({4, 5, 6, 3, 2, 1}));
}

TEST(SuccessiveInclusionInitialRoute, generateInitialSolution) {

  SuccessiveInclusionInitialRoute io;
  std::vector<Coord> coords = {{0, 0}, {2, 4}, {1, 3}, {3, 4}, {4, 3}, {2, 1}};
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(io.set_signal_functions(&search_signals));
  MapData map_data = MapData(coords);
  ASSERT_NO_THROW(io.setStartingCity(1));

  ASSERT_NO_THROW(io.generateInitialSolution(map_data));
  std::vector<std::string> expected_output = {"Successive Inclusion",
                                              "Current cost: 12.0552"};
  EXPECT_EQ(io.search_text_strings(), expected_output);
  EXPECT_EQ(io.current_route().order(), std::vector<uint>({1, 3, 2, 4, 5, 6}));
}

TEST(SuccessiveInclusionInitialRoute, generateInitialSolutionReverse) {

  SuccessiveInclusionInitialRoute io;
  std::vector<Coord> coords = {{0, 0}, {2, 4}, {1, 3}, {3, 4}, {4, 3}, {2, 1}};
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(io.set_signal_functions(&search_signals));
  MapData map_data = MapData(coords);
  ASSERT_NO_THROW(io.setStartingCity(6));

  ASSERT_NO_THROW(io.generateInitialSolution(map_data));
  std::vector<std::string> expected_output = {"Successive Inclusion",
                                              "Current cost: 12.0552"};
  EXPECT_EQ(io.search_text_strings(), expected_output);
  EXPECT_EQ(io.current_route().order(), std::vector<uint>({6, 5, 4, 2, 3, 1}));
}
