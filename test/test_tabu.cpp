#include <functional>
#include <gtest/gtest.h>
#include <vector>

#include "heuristics/tabu_list.h"
#include "heuristics/tabu_search.h"
#include "moves/move.h"
#include "moves/move_list.h"
#include "moves/move_swap_2_any.h"
#include "problem_structures/neighbourhood.h"
#include "problem_structures/route.h"
#include "problem_structures/solution.h"

#include "fake_neighbourhood.h"
#include "fake_solution.h"

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5},
                             {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10}};
MapData map_data(coords);

class SearchSignalsFake : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() {}
  void signal_function_initial_route_changed() {}
  void signal_function_search_status_update() {}
  void signal_function_summary_update() {}
};

TEST(TabuSearch, ConstructorEmpty) {
  Route initial_route = Route({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  EXPECT_NO_THROW(
      TabuSearch tabu(10, 1, p_initial_solution,
                      std::make_unique<FakeNeighbourhood>(neighbourhood)););
}

TEST(TabuSearch, InitialRoute) {
  Route initial_route = Route({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  TabuSearch tabu(10, 1, p_initial_solution,
                  std::make_unique<FakeNeighbourhood>(neighbourhood));
  // initial_solution.set_route(initial_route);
  EXPECT_NO_THROW(tabu.set_initial_solution(p_initial_solution););
}

// // is it making the first improvement
TEST(TabuSearch, FirstImprovement) {
  Route initial_route = Route({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    }
  };
  TabuSearch tabu(10, 1, p_initial_solution,
                  std::make_unique<FakeNeighbourhood>(neighbourhood));
  // initial_solution.set_route(initial_route);
  // tabu.set_initial_solution(initial_solution);
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(tabu.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(tabu.run_search(map_data));
  EXPECT_EQ(tabu.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
}

TEST(TabuSearch, FirstImprovementSearch) {
  Route initial_route = Route({1, 11, 3, 5, 4, 7, 6, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 5, 4, 7, 6, 8, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 6));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    }
  };
  TabuSearch tabu(10, 1, p_initial_solution,
                  std::make_unique<FakeNeighbourhood>(neighbourhood));
  // initial_solution.set_route(initial_route);
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(tabu.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(tabu.run_search(map_data));
  EXPECT_EQ(tabu.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 5, 4, 7, 6, 8, 9, 10, 11}));
}

//  is it making successive improvements

TEST(TabuSearch, SuccessiveImprovementSearch) {
  Route initial_route = Route({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
    } else // if (route.order() ==
    {      //    std::vector<uint>({0, 1, 2, 4, 3, 7, 6, 5, 8, 9, 10})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
    }
  };
  TabuSearch tabu(10, 2, p_initial_solution,
                  std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(tabu.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(tabu.run_search(map_data));
  EXPECT_EQ(tabu.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 5, 4, 6, 7, 8, 9, 10, 11}));
}

// // // is it finishing when there are no improvements

TEST(TabuSearch, FinishingSearch) {
  Route initial_route = Route({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
    } else // if (route.order() ==
    {      //    std::vector<uint>({0, 1, 2, 4, 3, 7, 6, 5, 8, 9, 10})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
    }
  };
  TabuSearch tabu(10, 4, p_initial_solution,
                  std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(tabu.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(tabu.run_search(map_data));
  EXPECT_EQ(tabu.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
}
