#include "uniform_double_random_number.h"
#include <gtest/gtest.h>

TEST(UniformDoubleRandomNumberClass, ConstructorEmpty) {
  EXPECT_NO_THROW(UniformDoubleRandomNumber rand =
                      UniformDoubleRandomNumber(););
}

TEST(UniformDoubleRandomNumberClass, set_seed) {
  UniformDoubleRandomNumber rand = UniformDoubleRandomNumber();
  EXPECT_NO_THROW(rand.set_seed(1););
}

TEST(UniformDoubleRandomNumberClass, generate) {
  UniformDoubleRandomNumber rand = UniformDoubleRandomNumber();
  rand.set_seed(1);
  double result;
  ASSERT_NO_THROW(result = rand.generate(););
  EXPECT_GE(result, 0.0);
  EXPECT_LT(result, 1.0);
}

TEST(UniformDoubleRandomNumberClass, generateLimit) {
  UniformDoubleRandomNumber rand = UniformDoubleRandomNumber();
  rand.set_seed(1);
  unsigned int result;
  for (auto i = 0; i < 100; ++i) {
    EXPECT_LT(result = rand.generate(5u), 5u) << result;
  }
}

TEST(UniformDoubleRandomNumberClass, differentSeedDifferentResults) {
  UniformDoubleRandomNumber rand = UniformDoubleRandomNumber();
  rand.set_seed(1);
  std::vector<unsigned int> results_1;
  for (auto i = 0; i < 100; ++i) {
    results_1.push_back(rand.generate(5u));
  }
  rand.set_seed(2);
  std::vector<unsigned int> results_2;
  for (auto i = 0; i < 100; ++i) {
    results_2.push_back(rand.generate(5u));
  }
  EXPECT_NE(results_1, results_2);
}