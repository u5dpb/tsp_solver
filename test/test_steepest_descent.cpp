#include <functional>
#include <gtest/gtest.h>
#include <vector>

#include "heuristics/search_signals_base.h"
#include "heuristics/steepest_descent.h"
#include "moves/move.h"
#include "moves/move_list.h"
#include "moves/move_swap_2_any.h"
#include "problem_structures/route.h"
#include "problem_structures/solution.h"

#include "fake_neighbourhood.h"

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5},
                             {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10}};
MapData map_data = MapData(coords);

class SearchSignalsFake : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() {}
  void signal_function_initial_route_changed() {}
  void signal_function_search_status_update() {}
  void signal_function_summary_update() {}
};

TEST(SteepestDescent, InitialRoute) {
  Route initial_route = Route({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  EXPECT_NO_THROW(
      SteepestDescent sd(p_initial_solution,
                         std::make_unique<FakeNeighbourhood>(neighbourhood)););
}

// is it making the first improvement
TEST(SteepestDescent, FirstImprovement) {
  Route initial_route = Route({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &m_l,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2})) {
      m_l.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    }
  };
  SteepestDescent sd(p_initial_solution,
                     std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(sd.run_search(map_data));
  EXPECT_EQ(sd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
}

TEST(SteepestDescent, FirstImprovementSearch) {
  Route initial_route = Route({1, 11, 3, 5, 4, 7, 6, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &m_l,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 5, 4, 7, 6, 8, 9, 10, 2})) {
      m_l.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
      m_l.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      m_l.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      m_l.addMove(std::make_unique<MoveSwap2Any>(route, 5, 6));
      m_l.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    }
  };
  SteepestDescent sd(p_initial_solution,
                     std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(sd.run_search(map_data));
  EXPECT_EQ(sd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 5, 4, 7, 6, 8, 9, 10, 11}));
}

// // // is it making successive improvements

TEST(SteepestDescent, SuccessiveImprovementSearch) {
  Route initial_route = Route({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 5, 4, 8, 7, 6, 9, 10, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
    }
  };
  SteepestDescent sd(p_initial_solution,
                     std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(sd.run_search(map_data));
  EXPECT_EQ(sd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 5, 4, 6, 7, 8, 9, 10, 11}));
}

// // // is it finishing when there are no improvements

TEST(SteepestDescent, FinishingSearch) {
  Route initial_route = Route({1, 11, 3, 5, 4, 8, 7, 6, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    move_list.addMove(std::make_unique<MoveSwap2Any>(route, 3, 4));
    move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 7));
    move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 2));
  };
  SteepestDescent sd(p_initial_solution,
                     std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sd.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(sd.run_search(map_data));
  EXPECT_EQ(sd.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
}
