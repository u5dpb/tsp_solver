#include <functional>
#include <gtest/gtest.h>
#include <vector>

#include "fake_neighbourhood.h"
#include "fake_solution.h"
#include "heuristics/simulated_annealing.h"
#include "heuristics/tabu_list.h"
#include "moves/move.h"
#include "moves/move_list.h"
#include "moves/move_swap_2_any.h"
#include "problem_structures/neighbourhood.h"
#include "problem_structures/route.h"
#include "problem_structures/solution.h"

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5},
                             {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10}};
MapData map_data = MapData(coords);

class SearchSignalsFake : public SearchSignalsBase {
public:
  void signal_function_current_route_changed() {}
  void signal_function_initial_route_changed() {}
  void signal_function_search_status_update() {}
  void signal_function_summary_update() {}
};

class FakeRandomNumber : public RandomNumber {
public:
  FakeRandomNumber(std::vector<double> vals) {
    if (vals.size() == 0)
      throw "Bad FakeRandomNumber\n";
    values = vals;
    c_values = 0;
  }
  double generate() {
    double result = values[c_values++];
    if (c_values >= values.size())
      c_values = 0;
    return result;
  }
  uint generate(uint value) { return uint(floor(double(value) * generate())); }
  void set_seed(uint seed) { this->seed = seed; }

private:
  uint c_values;
  std::vector<double> values;
  uint seed;
};

TEST(SimulatedAnnealing, InitialRoute) {
  Route initial_route = Route({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  EXPECT_NO_THROW(SimulatedAnnealing sa(
                      5.0, 0.25, 0.95, 1000, p_initial_solution,
                      std::make_unique<FakeNeighbourhood>(neighbourhood)););
}

// makes an first improvement
TEST(SimulatedAnnealing, FirstImprovement) {
  Route initial_route = Route({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    }
  };
  SimulatedAnnealing sa(5.0, 0.25, 0.0, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  FakeRandomNumber fake_rnd({0.0});
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
}
// accepts a non-optimal improvement
TEST(SimulatedAnnealing, AcceptsNonOptimal) {
  Route initial_route = Route({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 6));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 3, 4, 5, 7, 6, 8, 9, 10, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    }
  };
  SimulatedAnnealing sa(5.0, 0.25, 0.0, 2, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 7, 6, 8, 9, 10, 11}));
}
// rejects a non-optimal improvement
TEST(SimulatedAnnealing, RejectsNonOptimal) {
  Route initial_route = Route({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    move_list.addMove(std::make_unique<MoveSwap2Any>(route, 5, 6));

    // }
  };
  SimulatedAnnealing sa(5.0, 0.25, 0.0, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.999999999999999});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 11, 3, 4, 5, 6, 7, 8, 9, 10, 2}));
}
// temperature decrements correctly
TEST(SimulatedAnnealing, Temperature_0) {
  Route initial_route = Route({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 6, 7));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 8, 9));
    }
  };
  SimulatedAnnealing sa(4.0, 4.25, 0.5, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2}));
}

TEST(SimulatedAnnealing, Temperature_1) {
  Route initial_route = Route({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 6, 7));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 8, 9));
    }
  };
  SimulatedAnnealing sa(4.0, 4.0, 0.5, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2}));
}

TEST(SimulatedAnnealing, Temperature_2) {
  Route initial_route = Route({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 6, 7));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 8, 9));
    }
  };
  SimulatedAnnealing sa(4.0, 2.0, 0.5, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11}));
}

TEST(SimulatedAnnealing, Temperature_3) {
  Route initial_route = Route({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 6, 7));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 8, 9));
    }
  };
  SimulatedAnnealing sa(4.0, 1.0, 0.5, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11}));
}

TEST(SimulatedAnnealing, Temperature_4) {
  Route initial_route = Route({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 6, 7));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 8, 9));
    }
  };
  SimulatedAnnealing sa(4.0, 0.5, 0.5, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11}));
}

TEST(SimulatedAnnealing, Temperature_5) {
  Route initial_route = Route({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2});
  Solution initial_solution(initial_route);
  std::unique_ptr<BaseSolution> p_initial_solution =
      std::make_unique<Solution>(initial_solution);
  FakeNeighbourhood neighbourhood(true, true, true, true);
  neighbourhood.generate_move_set_override = [](MoveList &move_list,
                                                const Route &route) {
    if (route.order() ==
        std::vector<uint>({1, 11, 4, 3, 6, 5, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 4, 5));
    } else if (route.order() ==
               std::vector<uint>({1, 11, 4, 3, 5, 6, 8, 7, 10, 9, 2})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 1, 10));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 4, 3, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 2, 3));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 8, 7, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 6, 7));
    } else if (route.order() ==
               std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11})) {
      move_list.addMove(std::make_unique<MoveSwap2Any>(route, 8, 9));
    }
  };
  SimulatedAnnealing sa(4.0, 0.25, 0.5, 1, p_initial_solution,
                        std::make_unique<FakeNeighbourhood>(neighbourhood));
  SearchSignalsFake search_signals;
  ASSERT_NO_THROW(sa.set_signal_functions(&search_signals));
  FakeRandomNumber fake_rnd({0.0000001});
  ASSERT_NO_THROW(sa.run_search(map_data, fake_rnd));
  EXPECT_EQ(sa.best_solution()->route().order(),
            std::vector<uint>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
}