#include "problem_structures/map_data.h"
#include "problem_structures/route.h"
#include <gtest/gtest.h>
#include <math.h>
#include <set>
#include <sstream>

std::vector<Coord> coords = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}};
MapData map_data(coords);

// double evaluate(std::vector<uint> order,
//                 std::vector<std::vector<double> > distances)
// {
//   double distance=0.0;
//   for (int i=0; i<order.size()-1; ++i)
//   {
//     distance+=distances[order[i]-1][order[i+1]-1];
//   }
//   distance+=distances[order[order.size()-1]-1][order[0]-1];
//   return distance;
// }

// Test Route::Route()

TEST(RouteClass, RouteConstructor) {
  ASSERT_NO_THROW(Route route);
  Route route;
  EXPECT_EQ(route.size(), 0);
}
TEST(RouteClass, Size) {
  ASSERT_NO_THROW(Route route);
  Route route;
  ASSERT_NO_THROW(route.setOrder({1, 2, 3, 4, 5, 6}));
  route.setOrder({1, 2, 3, 4, 5, 6});
  EXPECT_EQ(route.size(), 6);
}
TEST(RouteClass, ElementReferenceNormal) {
  ASSERT_NO_THROW(Route route);
  Route route;
  ASSERT_NO_THROW(route.setOrder({1, 2, 3, 4, 5, 6}));
  EXPECT_EQ(route[0], 1);
  EXPECT_EQ(route[1], 2);
  EXPECT_EQ(route[5], 6);
}
TEST(RouteClass, ElementReferenceLow) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  EXPECT_EQ(route[-1], 6);
  EXPECT_EQ(route[-6], 1);
  EXPECT_DEATH(route[-12], "Assertion.*failed");
}
TEST(RouteClass, ElementReferenceHigh) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  EXPECT_EQ(route[6], 1);
  EXPECT_EQ(route[11], 6);
  EXPECT_DEATH(route[12], "Assertion.*failed");
}
TEST(RouteClass, ElementWriteNormal) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  route[0] = 7;
  EXPECT_EQ(route.order(), std::vector<uint>({7, 2, 3, 4, 5, 6}));
  route[1] = 8;
  EXPECT_EQ(route.order(), std::vector<uint>({7, 8, 3, 4, 5, 6}));
  route[5] = 9;
  EXPECT_EQ(route.order(), std::vector<uint>({7, 8, 3, 4, 5, 9}));
}
TEST(RouteClass, ElementWriteLow) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  route[-1] = 7;
  EXPECT_EQ(route.order(), std::vector<uint>({1, 2, 3, 4, 5, 7}));
  ASSERT_NO_THROW(route[-2] = 8;);
  EXPECT_EQ(route.order(), std::vector<uint>({1, 2, 3, 4, 8, 7}));
  EXPECT_DEATH(route[-9] = 9, "Assertion.*failed");
  // EXPECT_EQ(route.getOrder(), std::vector<uint>({1, 2, 3, 9, 8, 7}));
}
TEST(RouteClass, ElementWriteHigh) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  route[6] = 7;
  EXPECT_EQ(route.order(), std::vector<uint>({7, 2, 3, 4, 5, 6}));
  ASSERT_NO_THROW(route[7] = 8;);
  EXPECT_EQ(route.order(), std::vector<uint>({7, 8, 3, 4, 5, 6}));
  EXPECT_DEATH(route[14] = 9, "Assertion.*failed");
  // EXPECT_EQ(route.getOrder(), std::vector<uint>({7, 8, 9, 4, 5, 6}));
}

TEST(RouteClass, OstreamOverloadWorks) {
  std::ostringstream output_string_stream;
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  EXPECT_NO_THROW(output_string_stream << route);
  EXPECT_EQ(output_string_stream.str(), "1 2 3 4 5 6 ");
}

TEST(RouteClass, EvaluateSimple) {
  Route r({1, 2, 3, 4, 5, 6});
  ASSERT_NO_THROW(Route::evaluate(r, map_data););
  EXPECT_DOUBLE_EQ(Route::evaluate(r, map_data), 10.0);
}

TEST(RouteClass, InsertAfterFirst) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  route.insert_after(0, 7);
  EXPECT_EQ(route.order(), std::vector<uint>({1, 7, 2, 3, 4, 5, 6}));
}

TEST(RouteClass, InsertAfterSecond) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  route.insert_after(1, 7);
  EXPECT_EQ(route.order(), std::vector<uint>({1, 2, 7, 3, 4, 5, 6}));
}

TEST(RouteClass, InsertAfterLast) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});
  route.insert_after(5, 7);
  EXPECT_EQ(route.order(), std::vector<uint>({1, 2, 3, 4, 5, 6, 7}));
}

TEST(RouteClass, InsertAfterEmpty) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({});
  route.insert_after(0, 7);
  EXPECT_EQ(route.order(), std::vector<uint>({7}));
}
TEST(RouteClass, InsertOutOfRange) {
  ASSERT_NO_THROW(Route route);
  Route route;
  route.setOrder({1, 2, 3, 4, 5, 6});

  EXPECT_DEATH(route.insert_after(6, 8), "Assertion.*failed");
}
