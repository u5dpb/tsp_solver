#include <gtest/gtest.h>
#include <moves/move_list.h>
#include <moves/move_swap_2_adj.h>
#include <moves/move_swap_2_any.h>
#include <problem_structures/route.h>

TEST(MoveListClass, ConstructorEmpty) {
  EXPECT_NO_THROW(MoveList m = MoveList(););
}

TEST(MoveListClass, StartsZeroSize) {
  MoveList move_list = MoveList();

  EXPECT_EQ(move_list.size(), 0);
}

TEST(MoveListClass, AddAMoveNoThrow) {
  MoveList move_list = MoveList();
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move(route, 0, 5);
  EXPECT_NO_THROW(move_list.addMove(std::make_unique<MoveSwap2Any>(move)));
}

TEST(MoveListClass, AddAMoveIncreaseSize) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move));
  EXPECT_EQ(move_list.size(), 1);
}

TEST(MoveListClass, AddTwoDifferentMovesNoThrow) {

  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  EXPECT_NO_THROW(move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2)));
}

TEST(MoveListClass, AddTwoDifferentMovesSize) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  EXPECT_EQ(move_list.size(), 2);
}

// TEST(MoveListClass, operatorEquals) {
//   MoveList move_list;
//   Route route = Route({0, 1, 2, 3, 4, 5});
//   MoveSwap2Any move_1(route, 0, 5);
//   move_list.addMove(std::make_unique<BaseMove>(move_1));
//   MoveSwap2Adj move_2(route, 0, 1);
//   move_list.addMove(std::make_unique<BaseMove>(move_2));
//   EXPECT_EQ(move_list[0]->get_city_1(), 0);
//   EXPECT_EQ(move_list[0]->get_city_2(), 5);
//   EXPECT_EQ(move_list[1]->get_city_1(), 0);
//   EXPECT_EQ(move_list[1]->get_city_2(), 1);
// }

TEST(MoveListClass, PopFrontNoThrow) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  std::unique_ptr<BaseMove> base_move;
  EXPECT_NO_THROW(base_move = move_list.pop_front());
}

TEST(MoveListClass, PopFrontMatch) {
  MoveList move_list_1, move_list_2;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list_1.addMove(std::make_unique<MoveSwap2Any>(move_1));
  move_list_2.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list_1.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  move_list_2.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  EXPECT_EQ(*(move_list_1.pop_front()), *(move_list_2.pop_front()));
}

TEST(MoveListClass, PopFrontSize) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  std::unique_ptr<BaseMove> base_move;
  base_move = move_list.pop_front();
  EXPECT_EQ(move_list.size(), 1);
}

TEST(MoveListClass, PopSecondMatch) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  std::unique_ptr<BaseMove> base_move;
  base_move = move_list.pop_item(1);
  EXPECT_EQ(*base_move, MoveSwap2Adj(route, 0, 1));
}

TEST(MoveListClass, PopSecondSize) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  std::unique_ptr<BaseMove> base_move;
  base_move = move_list.pop_item(1);
  EXPECT_EQ(move_list.size(), 1);
}

TEST(MoveListClass, PopThirdThrows) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Any move_1(route, 0, 5);
  move_list.addMove(std::make_unique<MoveSwap2Any>(move_1));
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  std::unique_ptr<BaseMove> base_move;
  EXPECT_ANY_THROW(base_move = move_list.pop_item(2));
}

TEST(MoveListClass, PopEmptyThrows) {
  MoveList move_list;
  std::unique_ptr<BaseMove> base_move;
  EXPECT_ANY_THROW(base_move = move_list.pop_front());
}

TEST(MoveListClass, PopMadeEmptyThrows) {
  MoveList move_list;
  Route route = Route({0, 1, 2, 3, 4, 5});
  MoveSwap2Adj move_2(route, 0, 1);
  move_list.addMove(std::make_unique<MoveSwap2Adj>(move_2));
  std::unique_ptr<BaseMove> base_move;
  base_move = move_list.pop_front();
  EXPECT_ANY_THROW(base_move = move_list.pop_front());
}