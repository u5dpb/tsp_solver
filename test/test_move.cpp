// a series of test functions to test that the move class is working correctly

#include <gtest/gtest.h>
#include <iostream>

#include "moves/move.h"
#include "moves/move_reinsert.h"
#include "moves/move_reverse.h"
#include "moves/move_swap_2_adj.h"
#include "moves/move_swap_2_any.h"
#include "problem_structures/map_data.h"
#include "problem_structures/route.h"

std::ostream &operator<<(std::ostream &out, const std::vector<uint> &v) {
  for (auto i : v)
    out << i << " ";
  return out;
}

std::vector<Coord> coords = {{0, 0}, {1, 3}, {2, 4}, {3, 4}, {4, 3}, {2, 1}};
MapData map_data = MapData(coords);

// Route route({1, 4, 2, 3, 6, 5});
// std::vector<std::vector<double>> distance_matrix = {
//     {0.0, 0.5, 1.1, 1.3, 2.9, 4.1}, {0.5, 0.0, 2.0, 1.3, 0.1, 9.9},
//     {1.1, 2.0, 0.0, 4.1, 3.7, 2.9}, {1.3, 1.3, 4.1, 0.0, 0.7, 5.1},
//     {2.9, 0.1, 3.7, 0.7, 0.0, 0.3}, {4.1, 9.9, 2.9, 5.1, 0.3, 0.0}};

double evaluate(std::vector<uint> test_route) {
  double result = 0.0;
  for (auto i = 0u; i < test_route.size(); ++i) {
    result += map_data.distance(test_route[i],
                                test_route[((i + 1) % (test_route.size()))]);
  }
  return result;
}

// swap 2 adjectent cities
TEST(Swap2Adj, GoodMoveNoThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_NO_THROW(MoveSwap2Adj m = MoveSwap2Adj(route, 0, 1););
}
TEST(Swap2Adj, GoodMoveCheck) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveSwap2Adj m = MoveSwap2Adj(route, 0, 1);
  EXPECT_EQ(m.get_city_1(), 1);
  EXPECT_EQ(m.get_city_2(), 4);
}
TEST(Swap2Adj, GoodMoveLoopNoThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_NO_THROW(MoveSwap2Adj m = MoveSwap2Adj(route, 5, 0););
}
TEST(Swap2Adj, GoodMoveLoopCheck) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveSwap2Adj m = MoveSwap2Adj(route, 5, 0);
  EXPECT_EQ(m.get_city_1(), 5);
  EXPECT_EQ(m.get_city_2(), 1);
  // std::cout << m << Move(0,1,5,0,neighbourhood::SWAP_2_ADJ);
}
TEST(Swap2Adj, BadAdjMoveWrongOrderThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveSwap2Adj m = MoveSwap2Adj(route, 1, 0););
}
TEST(Swap2Adj, BadNonAdjMoveNoThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveSwap2Adj m = MoveSwap2Adj(route, 0, 2););
}
TEST(Swap2Adj, BadSameCityMoveThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveSwap2Adj m = MoveSwap2Adj(route, 0, 0););
}
// TEST(Swap2Adj, Clone) { // route = {1, 4, 2, 3, 6, 5};
//   Route route({1, 4, 2, 3, 6, 5});
//   MoveSwap2Adj m = MoveSwap2Adj(route, 0, 1);
//   MoveSwap2Adj *m2;
//   EXPECT_NO_THROW(m2 = m.clone(););
//   if (m2 != NULL) {
//     EXPECT_EQ(m, *m2);
//     delete m2;
//   }
// }

TEST(Swap2Adj, NeighbourhoodSize) {
  Route route({1, 4, 2, 3, 6, 5});
  // EXPECT_EQ(neighbourhood::neighbourhoodSize(route,
  // neighbourhood::SWAP_2_ADJ),
  //           6);
  EXPECT_EQ(MoveSwap2Adj::neighbourhoodSize(route), 6);
}

// swap adj : test generate move set

// swap adj : test choose move
TEST(Swap2Adj, ChooseMove) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveSwap2Adj m = MoveSwap2Adj(route, 0, 1);
  Route new_route = m.chooseMove(route);
  std::vector<uint> expected_route = {4, 1, 2, 3, 6, 5};
  EXPECT_EQ(new_route.order(), expected_route);
}

// swap adj : test get change
TEST(Swap2Adj, TestGetChange) {
  Route route({1, 4, 2, 3, 6, 5});
  TabuList empty_tabu_list(10);
  std::vector<double> expected_difference;
  std::vector<double> calculated_difference;
  std::vector<MoveSwap2Adj> move_set;
  for (auto i = 0u; i < route.size(); ++i) {
    MoveSwap2Adj new_move(route, i, i + 1);
    move_set.push_back(new_move);
  }

  for (auto move : move_set) {
    ASSERT_NO_THROW(move.chooseMove(route)) << "\n"
                                            << route << "\n"
                                            << &move << "\n";
    Route new_route = move.chooseMove(route);
    double cost = evaluate(new_route.order()) - evaluate(route.order());
    ASSERT_NO_THROW(move.getChange(route, map_data));
    // << "\n"
    // << route << "\n"
    // << move << "\n";
    EXPECT_NEAR(cost, move.getChange(route, map_data), 0.01);
    // << "cost =" << cost << " vs "
    // << neighbourhood::getChange(route, move, distance_matrix) << "\nroute "
    // << route << "\n"
    // << move << "\n";
  }
}

// swap any two cities

TEST(Swap2Any, GoodMoveNoThrow) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_NO_THROW(MoveSwap2Any m = MoveSwap2Any(route, 0, 4););
}
TEST(Swap2Any, GoodMoveCheck) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveSwap2Any m = MoveSwap2Any(route, 0, 4);
  EXPECT_EQ(m.get_city_1(), 1);
  EXPECT_EQ(m.get_city_2(), 6);
}

TEST(Swap2Any, BadSameCityMoveThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveSwap2Any m = MoveSwap2Any(route, 0, 0););
}

TEST(Swap2Any, BadWrongOrderMoveThrow) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveSwap2Any m = MoveSwap2Any(route, 5, 0););
}
// TEST(Swap2Any, Clone) { // route = {1, 4, 2, 3, 6, 5};
//   Route route({1, 4, 2, 3, 6, 5});
//   MoveSwap2Any m = MoveSwap2Any(route, 0, 1);
//   MoveSwap2Any *m2;
//   EXPECT_NO_THROW(m2 = m.clone(););
//   if (m2 != NULL) {
//     EXPECT_EQ(m, *m2);
//     delete m2;
//   }
// }

TEST(Swap2Any, NeighbourhoodSize) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_EQ(MoveSwap2Any::neighbourhoodSize(route), 15);
}

TEST(Swap2Any, ChooseMove) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveSwap2Any m = MoveSwap2Any(route, 0, 4);
  Route new_route = m.chooseMove(route);
  std::vector<uint> expected_route = {6, 4, 2, 3, 1, 5};
  EXPECT_EQ(new_route.order(), expected_route);
}

TEST(Swap2Any, TestGetChange) {
  Route route({1, 4, 2, 3, 6, 5});
  TabuList empty_tabu_list(10);
  std::vector<double> expected_difference;
  std::vector<double> calculated_difference;
  std::vector<MoveSwap2Any> move_set;
  for (auto i = 0u; i < route.size() - 1; ++i) {
    for (auto j = i + 1u; j < route.size(); ++j) {
      MoveSwap2Any new_move(route, i, j);
      move_set.push_back(new_move);
    }
  }
  for (auto move : move_set) {
    Route new_route = move.chooseMove(route);
    double cost = evaluate(new_route.order()) - evaluate(route.order());
    EXPECT_NEAR(cost, move.getChange(route, map_data), 0.01);
    // << route << "\n"
    // << move << "\n";
  }
}

// reinsert one city

TEST(Reinsert, GoodMoveNoThrow) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_NO_THROW(MoveReinsert m = MoveReinsert(route, 1, 4););
}
TEST(Reinsert, GoodMoveNoThrow2) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_NO_THROW(MoveReinsert m = MoveReinsert(route, 0, 3););
}
TEST(Reinsert, GoodMoveCheckBoundary) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveReinsert m = MoveReinsert(route, 1, 4);
  EXPECT_EQ(m.get_city_1(), 4);
  EXPECT_EQ(m.get_city_2(), 4);
}
TEST(Reinsert, GoodMoveCheck) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveReinsert m = MoveReinsert(route, 1, 3);
  EXPECT_EQ(m.get_city_1(), 4);
  EXPECT_EQ(m.get_city_2(), 4);
}
// {0,1,2,3,4,5} o1=0 o2=1
// {1,0,2,3,4,5}
// {0,1,2,3,4,5} o1=1 o2=0
// {1,0,2,3,4,5}
// {0,1,2,3,4,5} o1=1 o2=5
// {0,2,3,4,1,5} o2 -1
TEST(Reinsert, BadInvalidPlaceMoveThrowFirst) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveReinsert m = MoveReinsert(route, 1, 0););
}
TEST(Reinsert, BadInvalidPlaceMoveThrowSecond) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveReinsert m = MoveReinsert(route, 1, 1););
}

TEST(Reinsert, NeighbourhoodSize) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_EQ(MoveReinsert::neighbourhoodSize(route), 24);
}

TEST(Reinsert, ChooseMove) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveReinsert m = MoveReinsert(route, 0, 3);
  Route new_route = m.chooseMove(route);
  std::vector<uint> expected_route = {4, 2, 3, 1, 6, 5};
  //{1, 4, 2, 3, 6, 5};
  EXPECT_EQ(new_route.order(), expected_route);
}

TEST(Reinsert, TestGetChange) {
  Route route({1, 4, 2, 3, 6, 5});
  std::vector<double> expected_difference;
  std::vector<double> calculated_difference;
  std::vector<MoveReinsert> move_set;
  for (auto i = 0u; i < route.size(); ++i) {
    for (auto j = 0u; j < route.size(); ++j) {
      auto k = (j + 1u) % route.size(); //(j - 1 + route.size()) % route.size();
      if (j != i && k != i) {
        // std::cout << "Try " << i << " and " << j << "\n";
        MoveReinsert new_move(route, i, j);
        move_set.push_back(new_move);
      }
    }
  }
  for (auto move : move_set) {
    Route new_route = move.chooseMove(route);
    double cost = evaluate(new_route.order()) - evaluate(route.order());
    EXPECT_NEAR(cost, move.getChange(route, map_data), 0.01);
    // << route << "\n"
    // << move << "\n";
  }
}

// reverse

TEST(Reverse, GoodMoveNoThrow) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_NO_THROW(MoveReverse m = MoveReverse(route, 1, 4););
}
TEST(Reverse, GoodMoveCheck) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveReverse m = MoveReverse(route, 1, 4);
  EXPECT_EQ(m.get_city_1(), 4);
  EXPECT_EQ(m.get_city_2(), 6);
}

TEST(Reverse, BadInvalidSameCity) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveReverse m = MoveReverse(route, 1, 1););
}
TEST(Reverse, BadInvalidNeighbouringCityLoop) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveReverse m = MoveReverse(route, 0, 5););
}
TEST(Reverse, BadInvalidReverseAll) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveReverse m = MoveReverse(route, 1, 5););
}
TEST(Reverse, BadWrongOrder) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_ANY_THROW(MoveReverse m = MoveReverse(route, 5, 1););
}

TEST(Reverse, NeighbourhoodSize) {
  Route route({1, 4, 2, 3, 6, 5});
  EXPECT_EQ(MoveReverse::neighbourhoodSize(route), 12);
}

TEST(Reverse, ChooseMove) { // route = {1, 4, 2, 3, 6, 5};
  Route route({1, 4, 2, 3, 6, 5});
  MoveReverse m = MoveReverse(route, 0, 3);
  Route new_route = m.chooseMove(route);
  std::vector<uint> expected_route = {3, 2, 4, 1, 6, 5};
  ; //{1, 4, 2, 3, 6, 5};
  EXPECT_EQ(new_route.order(), expected_route);
}

TEST(Reverse, TestGetChange) {
  Route route({1, 4, 2, 3, 6, 5});
  std::vector<double> expected_difference;
  std::vector<double> calculated_difference;
  std::vector<MoveReverse> move_set;
  for (auto i = 0u; i < route.size(); ++i) {
    for (auto j = i + 1u; j < route.size() && j - i < route.size() - 2u; ++j) {
      MoveReverse new_move(route, i, j);
      move_set.push_back(new_move);
    }
  }
  for (auto move : move_set) {
    Route new_route = move.chooseMove(route);
    double cost = evaluate(new_route.order()) - evaluate(route.order());
    EXPECT_NEAR(cost, move.getChange(route, map_data), 0.01);
    // << route << "\n"
    // << move << "\n";
  }
}

// TEST(ManyNeighourhoods, MoveSetCorrectSizeOneHood) {
//   Route route({1, 4, 2, 3, 6, 5});
//   std::set<Move> empty_set;
//   std::set<neighbourhood::NEIGHBOURHOOD> neighbourhood_types;
//   neighbourhood_types.insert(neighbourhood::SWAP_2_ADJ);
//   std::vector<Move> move_set = neighbourhood::generateMoveSet(
//       route, neighbourhood::SWAP_2_ADJ, empty_set);

//   EXPECT_EQ(neighbourhood::neighbourhoodSize(route, neighbourhood_types),
//             move_set.size());
// }

// TEST(ManyNeighourhoods, MoveSetCorrectSizeTwoHoods) {
//   Route route({1, 4, 2, 3, 6, 5});
//   std::set<Move> empty_set;
//   std::set<neighbourhood::NEIGHBOURHOOD> neighbourhood_types;
//   neighbourhood_types.insert(neighbourhood::SWAP_2_ADJ);
//   neighbourhood_types.insert(neighbourhood::REVERSE);

//   std::vector<Move> move_set_swap_2_adj = neighbourhood::generateMoveSet(
//       route, neighbourhood::SWAP_2_ADJ, empty_set);
//   std::vector<Move> move_set_reverse =
//       neighbourhood::generateMoveSet(route, neighbourhood::REVERSE,
//       empty_set);
//   EXPECT_EQ(neighbourhood::neighbourhoodSize(route, neighbourhood_types),
//             move_set_swap_2_adj.size() + move_set_reverse.size());
// }
